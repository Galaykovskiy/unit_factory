/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/15 16:44:56 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/15 16:44:57 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H
# define WIN_S 1000
# define TEX_S 64
# define SPR_NUM 24
# define TXR_NUM 29
# define DST 100
# include "../libft/libft.h"
# include <mlx.h>
# include <math.h>
# include <errno.h>
# include <time.h>
# include <pthread.h>

typedef struct	s_spr
{
	double		x;
	double		y;
	int			t;
	int			f;
}				t_spr;

typedef struct	s_wolf
{
	void		*mlx;
	void		*win;
	char		*img;
	void		*img_ptr;
	void		*z[12];
	t_spr		spr[SPR_NUM];
	int			bpp;
	int			size_l;
	int			endian;
	int			width;
	int			height;
	int			x;
	int			y;
	int			texs;
	int			start;
	int			end;
	int			open;
	int			pt;
	int			m_x;
	int			m_y;
	int			t_x;
	int			t_y;
	int			t_num;
	int			hit;
	int			side;
	int			color;
	int			line_h;
	int			d;
	int			s_x;
	int			s_y;
	int			f_x;
	int			f_y;
	int			i;
	int			spr_ord[SPR_NUM];
	int			str;
	int			dr_s_x;
	int			dr_s_y;
	int			dr_e_x;
	int			dr_e_y;
	int			spr_scr_x;
	int			mv_scr;
	int			spr_w;
	int			spr_h;
	int			vdiv;
	int			udiv;
	int			*texture[TXR_NUM];
	int			**map;
	int			compas;
	double		ms;
	double		rs;
	double		p_x;
	double		p_y;
	double		d_x;
	double		d_y;
	double		pl_x;
	double		pl_y;
	double		dstwall;
	double		fps;
	double		buf[WIN_S + DST];
	double		cam_x;
	double		rd_x;
	double		rd_y;
	double		ddst_x;
	double		ddst_y;
	double		sdst_x;
	double		sdst_y;
	double		w_x;
	double		dst_cur;
	double		dst_pl;
	double		dst_w;
	double		cf_x;
	double		cf_y;
	double		weight;
	double		f_x_w;
	double		f_y_w;
	double		spr_x;
	double		spr_y;
	double		tr_x;
	double		tr_y;
	double		invdet;
	double		mv_v;
	double		spr_dst[SPR_NUM];
}				t_wolf;

void			first_read(char *file, t_wolf *wolf);
void			read_file(char *file, t_wolf *wolf);
void			spr_init(t_wolf *wolf);
void			set_z(t_wolf *wolf);
void			set_txr(t_wolf *wolf);
int				key_hook1(int keycode, t_wolf *wolf);
int				key_hook2(int keycode, t_wolf *wolf);
int				key_hook3(int keycode, t_wolf *wolf);
int				key_hook4(int keycode, t_wolf *wolf);
int				key_hook5(int keycode, t_wolf *wolf);
void			scene(t_wolf *wolf);
void			spr_casting(t_wolf *wolf);
void			count_points(t_wolf *wolf);
void			count_wall(t_wolf *wolf);
void			count_height(t_wolf *wolf);
void			count(t_wolf *wolf);
void			count_step(t_wolf *wolf);
void			clear_image(t_wolf *wolf);
void			put_pxl_to_img(t_wolf *wolf, int x, int y, int color);
void			check_side(t_wolf *wolf);
void			sorting(int *order, double *dst, int amount);
void			addition_hook5(t_wolf *wolf);
void			error(t_wolf *wolf, char *message);
int				check_digit(char *str);
void			check_sides(t_wolf *wolf);
int				quit(void *param);
#endif
