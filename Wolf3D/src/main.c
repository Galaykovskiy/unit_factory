/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/15 17:04:02 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/15 17:04:02 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/wolf3d.h"

int			key_hook(int keycode, t_wolf *wolf)
{
	key_hook1(keycode, wolf);
	key_hook2(keycode, wolf);
	key_hook3(keycode, wolf);
	key_hook4(keycode, wolf);
	key_hook5(keycode, wolf);
	return (1);
}

static void	start_game(t_wolf *wolf)
{
	set_txr(wolf);
	set_z(wolf);
	mlx_put_image_to_window(wolf->mlx, wolf->win, wolf->z[11], 0, 0);
	system("afplay ./game_music.mp3 &");
}

static void	init(char *file, t_wolf *wolf)
{
	void	*mlx;

	first_read(file, wolf);
	read_file(file, wolf);
	check_sides(wolf);
	wolf->mlx = mlx_init();
	mlx = wolf->mlx;
	wolf->win = mlx_new_window(wolf->mlx, WIN_S, WIN_S, "Wolf3D");
	wolf->img_ptr = mlx_new_image(mlx, WIN_S, WIN_S);
	wolf->img = mlx_get_data_addr(wolf->img_ptr, &wolf->bpp,
									&wolf->size_l, &wolf->endian);
	start_game(wolf);
	mlx_hook(wolf->win, 2, 1L << 0, key_hook, wolf);
	mlx_hook(wolf->win, 17, 1L << 17, quit, wolf);
	mlx_loop(wolf->mlx);
}

static void	initial_data(t_wolf *wolf)
{
	spr_init(wolf);
	wolf->udiv = 1;
	wolf->vdiv = 1;
	wolf->mv_v = 0;
	wolf->bpp = 32;
	wolf->endian = 0;
	wolf->texs = 128;
	wolf->size_l = WIN_S;
	wolf->p_x = 2;
	wolf->p_y = 2;
	wolf->d_x = -1;
	wolf->d_y = 0;
	wolf->pt = 0;
	wolf->pl_x = 0;
	wolf->pl_y = 0.66;
	wolf->pt = 0;
	wolf->color = 265;
	wolf->compas = 0;
}

int			main(int argc, char **argv)
{
	t_wolf	*wolf;

	if (argc != 2)
	{
		if (argc > 2)
			ft_putendl("Please, input only one map!");
		else
			ft_putendl("Please, input the map!");
		return (0);
	}
	if (!(wolf = (t_wolf*)malloc(sizeof(t_wolf))))
		return (0);
	initial_data(wolf);
	init(argv[1], wolf);
	return (0);
}
