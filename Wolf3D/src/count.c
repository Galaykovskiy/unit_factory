/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/01 20:28:21 by agalayko          #+#    #+#             */
/*   Updated: 2018/11/01 20:28:23 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	count_points(t_wolf *wolf)
{
	int tmp;

	tmp = wolf->pt;
	while (tmp > 9)
	{
		mlx_put_image_to_window(wolf->mlx, wolf->win,
			wolf->z[tmp % 10], 260, 920);
		tmp = tmp / 10;
	}
	mlx_put_image_to_window(wolf->mlx, wolf->win,
		wolf->z[tmp], 230, 920);
}

void	count_wall(t_wolf *wolf)
{
	wolf->cam_x = 2 * wolf->x / (double)WIN_S - 1;
	wolf->rd_x = wolf->d_x + wolf->pl_x * wolf->cam_x;
	wolf->rd_y = wolf->d_y + wolf->pl_y * wolf->cam_x;
	wolf->m_x = wolf->p_x;
	wolf->m_y = wolf->p_y;
	wolf->ddst_x = fabs(1 / wolf->rd_x);
	wolf->ddst_y = fabs(1 / wolf->rd_y);
	wolf->hit = 0;
}

void	count_height(t_wolf *wolf)
{
	wolf->line_h = (WIN_S / wolf->dstwall);
	wolf->start = -wolf->line_h / 2 + WIN_S / 2;
	if (wolf->start < 0)
		wolf->start = 0;
	wolf->end = wolf->line_h / 2 + WIN_S / 2;
	if (wolf->end >= WIN_S)
		wolf->end = WIN_S - 1;
	wolf->t_num = wolf->map[wolf->m_x][wolf->m_y] - 1;
	if (wolf->side == 0)
		wolf->w_x = wolf->p_y + wolf->dstwall * wolf->rd_y;
	else
		wolf->w_x = wolf->p_x + wolf->dstwall * wolf->rd_x;
	wolf->w_x -= floor((wolf->w_x));
	wolf->t_x = (int)(wolf->w_x * (double)(TEX_S));
	if (wolf->side == 0 && wolf->rd_x > 0)
		wolf->t_x = TEX_S - wolf->t_x - 1;
	if (wolf->side == 1 && wolf->rd_y < 0)
		wolf->t_x = TEX_S - wolf->t_x - 1;
}

void	count(t_wolf *wolf)
{
	wolf->d = wolf->y * 256 - WIN_S * 128 + wolf->line_h * 128;
	wolf->t_y = ((wolf->d * TEX_S) / wolf->line_h) / 256;
	wolf->color = wolf->texture[wolf->t_num][TEX_S * wolf->t_y + wolf->t_x];
	if (wolf->compas)
	{
		if (wolf->s_x < 0 && wolf->t_num != 4 && wolf->t_num != 8)
			wolf->color = wolf->texture[0][TEX_S * wolf->t_y + wolf->t_x];
		else if (wolf->t_num != 4 && wolf->t_num != 8)
			wolf->color = wolf->texture[1][TEX_S * wolf->t_y + wolf->t_x];
		if (wolf->side == 1 && wolf->t_num != 4 && wolf->t_num != 8)
		{
			if (wolf->s_y < 0)
				wolf->color = wolf->texture[2][TEX_S * wolf->t_y + wolf->t_x];
			else
				wolf->color = wolf->texture[3][TEX_S * wolf->t_y + wolf->t_x];
		}
	}
	if (wolf->side == 1)
		wolf->color = (wolf->color >> 1) & 8355711;
	put_pxl_to_img(wolf, wolf->x, wolf->y, wolf->color);
	wolf->y++;
}

void	count_step(t_wolf *wolf)
{
	if (wolf->rd_x < 0)
	{
		wolf->s_x = -1;
		wolf->sdst_x = (wolf->p_x - wolf->m_x) * wolf->ddst_x;
	}
	else
	{
		wolf->s_x = 1;
		wolf->sdst_x = (wolf->m_x + 1.0 - wolf->p_x) * wolf->ddst_x;
	}
	if (wolf->rd_y < 0)
	{
		wolf->s_y = -1;
		wolf->sdst_y = (wolf->p_y - wolf->m_y) * wolf->ddst_y;
	}
	else
	{
		wolf->s_y = 1;
		wolf->sdst_y = (wolf->m_y + 1.0 - wolf->p_y) * wolf->ddst_y;
	}
}
