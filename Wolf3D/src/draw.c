/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/01 22:10:02 by agalayko          #+#    #+#             */
/*   Updated: 2018/11/01 22:10:03 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void	set_spr(t_wolf *wolf)
{
	wolf->spr_x = wolf->spr[wolf->spr_ord[wolf->i]].x - wolf->p_x;
	wolf->spr_y = wolf->spr[wolf->spr_ord[wolf->i]].y - wolf->p_y;
	wolf->invdet = 1.0 / (wolf->pl_x * wolf->d_y - wolf->d_x * wolf->pl_y);
	wolf->tr_x = wolf->invdet *
	(wolf->d_y * wolf->spr_x - wolf->d_x * wolf->spr_y);
	wolf->tr_y = wolf->invdet *
	(-wolf->pl_y * wolf->spr_x + wolf->pl_x * wolf->spr_y);
	wolf->mv_scr = (int)(wolf->mv_v / wolf->tr_y);
	wolf->spr_scr_x = (int)((WIN_S / 2) * (1 + wolf->tr_x
	/ wolf->tr_y));
	wolf->spr_h = abs((int)(WIN_S / (wolf->tr_y))) / wolf->vdiv;
	wolf->dr_s_y = -wolf->spr_h / 2 + WIN_S / 2 + wolf->mv_scr;
	if (wolf->dr_s_y < 0)
		wolf->dr_s_y = 0;
	wolf->dr_e_y = wolf->spr_h / 2 + WIN_S / 2 + wolf->mv_scr;
	if (wolf->dr_e_y >= WIN_S)
		wolf->dr_e_y = WIN_S - 1;
	wolf->spr_w = abs((int)(WIN_S / (wolf->tr_y))) / wolf->udiv;
	wolf->dr_s_x = -wolf->spr_w / 2 + wolf->spr_scr_x;
	if (wolf->dr_s_x < 0)
		wolf->dr_s_x = 0;
	wolf->dr_e_x = wolf->spr_w / 2 + wolf->spr_scr_x;
	if (wolf->dr_e_x >= WIN_S)
		wolf->dr_e_x = WIN_S - 1;
}

static void	draw_local(t_wolf *wolf)
{
	wolf->d = (wolf->y - wolf->mv_scr) * 256 - WIN_S *
	128 + wolf->spr_h * 128;
	wolf->t_y = ((wolf->d * TEX_S) / wolf->spr_h) / 256;
	wolf->color = wolf->texture[wolf->spr[wolf->spr_ord[wolf->i]].t]
		[TEX_S * wolf->t_y + wolf->t_x];
	if ((wolf->color & 0x00FFFFFF) != 0 && wolf->color != 0x980088)
		put_pxl_to_img(wolf, wolf->str, wolf->y, wolf->color);
}

static void	draw_spr(t_wolf *wolf)
{
	if (fabs(wolf->spr_x) < 1 && fabs(wolf->spr_y) < 1
	&& (wolf->spr[wolf->spr_ord[wolf->i]].t == 24
	|| wolf->spr[wolf->spr_ord[wolf->i]].t == 25
	|| wolf->spr[wolf->spr_ord[wolf->i]].t == 26
	|| wolf->spr[wolf->spr_ord[wolf->i]].t == 27)
	&& wolf->spr[wolf->spr_ord[wolf->i]].f == 0)
	{
		wolf->spr[wolf->spr_ord[wolf->i]].f = 1;
		wolf->pt++;
	}
	wolf->t_x = (int)(256 * (wolf->str - (-wolf->spr_w
	/ 2 + wolf->spr_scr_x)) * TEX_S / wolf->spr_w) / 256;
	if (wolf->tr_y > 0 && wolf->str > 0 &&
	wolf->str < WIN_S && wolf->tr_y
	< wolf->buf[wolf->str] && wolf->spr[wolf->spr_ord[wolf->i]].f == 0)
	{
		wolf->y = wolf->dr_s_y;
		while (wolf->y < wolf->dr_e_y)
		{
			draw_local(wolf);
			wolf->y++;
		}
	}
	wolf->str++;
}

void		spr_casting(t_wolf *wolf)
{
	wolf->i = 0;
	while (wolf->i < SPR_NUM)
	{
		wolf->spr_ord[wolf->i] = wolf->i;
		wolf->spr_dst[wolf->i] = ((wolf->p_x - wolf->spr[wolf->i].x) *
		(wolf->p_x - wolf->spr[wolf->i].x)
		+ (wolf->p_y - wolf->spr[wolf->i].y)
		* (wolf->p_y - wolf->spr[wolf->i].y));
		wolf->i++;
	}
	sorting(wolf->spr_ord, wolf->spr_dst, SPR_NUM);
	wolf->i = 0;
	while (wolf->i < SPR_NUM)
	{
		set_spr(wolf);
		wolf->str = wolf->dr_s_x;
		while (wolf->str < wolf->dr_e_x)
			draw_spr(wolf);
		wolf->i++;
	}
}
