/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/01 15:16:04 by agalayko          #+#    #+#             */
/*   Updated: 2018/11/01 15:16:05 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void	floorcasting(t_wolf *wolf)
{
	check_side(wolf);
	wolf->dst_w = wolf->dstwall;
	wolf->dst_pl = 0.0;
	if (wolf->end < 0)
		wolf->end = WIN_S;
	wolf->y = wolf->end + 1;
	while (wolf->y < WIN_S)
	{
		wolf->dst_cur = WIN_S / (2.0 * wolf->y - WIN_S);
		wolf->weight = (wolf->dst_cur - wolf->dst_pl)
		/ (wolf->dst_w - wolf->dst_pl);
		wolf->cf_x = wolf->weight * wolf->f_x_w
		+ (1.0 - wolf->weight) * wolf->p_x;
		wolf->cf_y = wolf->weight * wolf->f_y_w
		+ (1.0 - wolf->weight) * wolf->p_y;
		wolf->f_x = (int)(wolf->cf_x * TEX_S) % TEX_S;
		wolf->f_y = (int)(wolf->cf_y * TEX_S) % TEX_S;
		wolf->color = (wolf->texture[5][TEX_S * wolf->f_y
			+ wolf->f_x] >> 1) & 8355711;
		put_pxl_to_img(wolf, wolf->x, wolf->y, wolf->color);
		wolf->color = wolf->texture[6][TEX_S * wolf->f_y + wolf->f_x];
		put_pxl_to_img(wolf, wolf->x, WIN_S - wolf->y, wolf->color);
		wolf->y++;
	}
}

static void	calculate(t_wolf *wolf)
{
	while (wolf->hit == 0)
	{
		if (wolf->sdst_x < wolf->sdst_y)
		{
			wolf->sdst_x += wolf->ddst_x;
			wolf->m_x += wolf->s_x;
			wolf->side = 0;
		}
		else
		{
			wolf->sdst_y += wolf->ddst_y;
			wolf->m_y += wolf->s_y;
			wolf->side = 1;
		}
		if (wolf->map[wolf->m_x][wolf->m_y] > 0)
			wolf->hit = 1;
	}
	if (wolf->side == 0)
		wolf->dstwall = ((double)wolf->m_x - wolf->p_x +
		(1.0 - (double)wolf->s_x) / 2.0) / wolf->rd_x;
	else
		wolf->dstwall = ((double)wolf->m_y - wolf->p_y +
		(1.0 - (double)wolf->s_y) / 2.0) / wolf->rd_y;
}

static void	draw(t_wolf *wolf)
{
	char	*str;

	str = ft_itoa((int)(1.0 / wolf->fps));
	mlx_put_image_to_window(wolf->mlx, wolf->win, wolf->img_ptr, 0, 0);
	mlx_put_image_to_window(wolf->mlx, wolf->win, wolf->z[10], 0, 875);
	mlx_string_put(wolf->mlx, wolf->win, 0, 2,
		0xFFFFFF, str);
	free(str);
}

static void	raycasting(t_wolf *wolf)
{
	count_wall(wolf);
	count_step(wolf);
	calculate(wolf);
	count_height(wolf);
	wolf->y = wolf->start;
	wolf->open = 0;
	while (wolf->y < wolf->end)
	{
		count(wolf);
	}
	wolf->buf[wolf->x] = wolf->dstwall;
	floorcasting(wolf);
	wolf->x++;
}

void		scene(t_wolf *wolf)
{
	clock_t	start;
	clock_t	end;

	wolf->x = 0;
	start = clock();
	while (wolf->x < WIN_S)
		raycasting(wolf);
	spr_casting(wolf);
	end = clock();
	wolf->fps = (double)(end - start) / CLOCKS_PER_SEC;
	wolf->ms = wolf->fps * 9;
	wolf->rs = wolf->fps * 5;
	draw(wolf);
	count_points(wolf);
}
