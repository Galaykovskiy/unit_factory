/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/01 22:46:39 by agalayko          #+#    #+#             */
/*   Updated: 2018/11/01 22:46:40 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void	ft_swap_integer(int *a, int *b)
{
	int c;

	c = *a;
	*a = *b;
	*b = c;
}

static void	ft_swap_double(double *a, double *b)
{
	double c;

	c = *a;
	*a = *b;
	*b = c;
}

static void	check(int *space)
{
	*space = (*space * 10) / 13;
	if (*space == 9 || *space == 10)
		*space = 11;
	if (*space < 1)
		*space = 1;
}

void		sorting(int *order, double *dst, int amount)
{
	int space;
	int flag;
	int j;
	int i;

	space = amount;
	flag = 0;
	while (space > 1 || flag)
	{
		check(&space);
		flag = 0;
		i = 0;
		while (i < amount - space)
		{
			j = i + space;
			if (dst[i] < dst[j])
			{
				ft_swap_double(&dst[i], &dst[j]);
				ft_swap_integer(&order[i], &order[j]);
				flag = 1;
			}
			i++;
		}
	}
}

void		addition_hook5(t_wolf *wolf)
{
	if (wolf->map[(int)(wolf->p_x)][(int)(wolf->p_y
		- wolf->dstwall + wolf->d_y * wolf->ms)] == 5)
	{
		wolf->map[(int)(wolf->p_x)][(int)(wolf->p_y
			- wolf->dstwall + wolf->d_y * wolf->ms)] = 0;
		wolf->pt++;
	}
	if (wolf->map[(int)(wolf->p_x + wolf->dstwall
		+ wolf->d_x * wolf->ms)][(int)wolf->p_y] == 5)
	{
		wolf->map[(int)(wolf->p_x + wolf->dstwall
			+ wolf->d_x * wolf->ms)][(int)wolf->p_y] = 0;
		wolf->pt++;
	}
}
