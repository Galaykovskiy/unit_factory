/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spr.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 21:25:42 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/31 21:25:43 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void	spr1(t_wolf *wolf)
{
	wolf->spr[0].x = 3;
	wolf->spr[0].y = 8;
	wolf->spr[0].t = 7;
	wolf->spr[1].x = 2;
	wolf->spr[1].y = 18;
	wolf->spr[1].t = 7;
	wolf->spr[2].x = 9;
	wolf->spr[2].y = 17;
	wolf->spr[2].t = 7;
	wolf->spr[3].x = 10;
	wolf->spr[3].y = 2.5;
	wolf->spr[3].t = 17;
	wolf->spr[4].x = 10;
	wolf->spr[4].y = 4.5;
	wolf->spr[4].t = 17;
	wolf->spr[5].x = 12;
	wolf->spr[5].y = 8;
	wolf->spr[5].t = 22;
	wolf->spr[6].x = 5;
	wolf->spr[6].y = 5;
	wolf->spr[6].t = 26;
	wolf->spr[7].x = 2;
	wolf->spr[7].y = 7;
	wolf->spr[7].t = 26;
}

static void	spr2(t_wolf *wolf)
{
	wolf->spr[8].x = 3;
	wolf->spr[8].y = 6;
	wolf->spr[8].t = 27;
	wolf->spr[9].x = 12;
	wolf->spr[9].y = 2;
	wolf->spr[9].t = 26;
	wolf->spr[10].x = 13;
	wolf->spr[10].y = 2;
	wolf->spr[10].t = 26;
	wolf->spr[11].x = 1.7;
	wolf->spr[11].y = 20.5;
	wolf->spr[11].t = 12;
	wolf->spr[12].x = 3.2;
	wolf->spr[12].y = 20.5;
	wolf->spr[12].t = 12;
	wolf->spr[13].x = 1.5;
	wolf->spr[13].y = 15;
	wolf->spr[13].t = 15;
	wolf->spr[14].x = 3.5;
	wolf->spr[14].y = 15;
	wolf->spr[14].t = 16;
	wolf->spr[15].x = 2;
	wolf->spr[15].y = 16;
	wolf->spr[15].t = 26;
}

static void	spr3(t_wolf *wolf)
{
	wolf->spr[16].x = 3;
	wolf->spr[16].y = 18;
	wolf->spr[16].t = 26;
	wolf->spr[17].x = 4.5;
	wolf->spr[17].y = 8;
	wolf->spr[17].t = 23;
	wolf->spr[18].x = 10.5;
	wolf->spr[18].y = 18;
	wolf->spr[18].t = 19;
	wolf->spr[19].x = 10.5;
	wolf->spr[19].y = 19;
	wolf->spr[19].t = 21;
	wolf->spr[20].x = 10.5;
	wolf->spr[20].y = 17;
	wolf->spr[20].t = 21;
	wolf->spr[21].x = 15;
	wolf->spr[21].y = 15;
	wolf->spr[21].t = 25;
	wolf->spr[22].x = 14;
	wolf->spr[22].y = 24;
	wolf->spr[22].t = 24;
	wolf->spr[23].x = 13;
	wolf->spr[23].y = 28;
	wolf->spr[23].t = 11;
}

void		spr_init(t_wolf *wolf)
{
	int	i;

	i = 0;
	while (i < 24)
		wolf->spr[i++].f = 0;
	spr1(wolf);
	spr2(wolf);
	spr3(wolf);
}
