/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 20:09:01 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/29 20:09:02 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	check_side(t_wolf *wolf)
{
	if (wolf->side == 0 && wolf->rd_x > 0)
	{
		wolf->f_x_w = wolf->m_x;
		wolf->f_y_w = wolf->m_y + wolf->w_x;
	}
	else if (wolf->side == 0 && wolf->rd_x < 0)
	{
		wolf->f_x_w = wolf->m_x + 1.0;
		wolf->f_y_w = wolf->m_y + wolf->w_x;
	}
	else if (wolf->side == 1 && wolf->rd_y > 0)
	{
		wolf->f_x_w = wolf->m_x + wolf->w_x;
		wolf->f_y_w = wolf->m_y;
	}
	else
	{
		wolf->f_x_w = wolf->m_x + wolf->w_x;
		wolf->f_y_w = wolf->m_y + 1.0;
	}
}

void	put_pxl_to_img(t_wolf *wolf, int x, int y, int color)
{
	if (wolf->x < WIN_S && wolf->y < WIN_S)
	{
		color = mlx_get_color_value(wolf->mlx, color);
		ft_memcpy(wolf->img + 4 * WIN_S * y + x * 4,
				&color, sizeof(int));
	}
}

void	error(t_wolf *wolf, char *message)
{
	int		i;

	ft_putstr("ERROR! ");
	ft_putendl(message);
	if (wolf->map)
	{
		i = 0;
		if (wolf->map[0])
			while (wolf->map[i] && i < wolf->height)
				free(wolf->map[i++]);
		free(wolf->map);
	}
	free(wolf);
	exit(1);
}

int		quit(void *param)
{
	param = NULL;
	system("killall afplay");
	exit(1);
	return (0);
}

int		check_digit(char *str)
{
	int		i;

	i = 0;
	while (str[i])
		if (!ft_isdigit(str[i++]))
			return (0);
	return (1);
}
