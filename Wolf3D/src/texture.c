/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/01 15:16:13 by agalayko          #+#    #+#             */
/*   Updated: 2018/11/01 15:16:13 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	check_sides(t_wolf *wolf)
{
	int	i;
	int j;

	i = 0;
	while (i < wolf->height)
	{
		j = 0;
		while (j < wolf->width)
		{
			if (wolf->map[0][j] == 0 || wolf->map[wolf->height - 1][j] == 0 ||
				wolf->map[i][0] == 0 || wolf->map[i][wolf->width - 1] == 0 ||
				wolf->map[0][j] == 9 || wolf->map[wolf->height - 1][j] == 9 ||
				wolf->map[i][0] == 9 || wolf->map[i][wolf->width - 1] == 9 ||
				wolf->map[0][j] == 5 || wolf->map[wolf->height - 1][j] == 5 ||
				wolf->map[i][0] == 5 || wolf->map[i][wolf->width - 1] == 5)
				error(wolf, "Bad sides!");
			if (wolf->map[1][1] != 0 || wolf->map[1][2] != 0 || wolf->map[1][3]
				!= 0 || wolf->map[2][1] != 0 || wolf->map[2][2] != 0 ||
				wolf->map[2][3] != 0 || wolf->map[3][1] != 0 || wolf->map[3][2]
				!= 0 || wolf->map[3][3] != 0)
				error(wolf, "Bad Start area!");
			j++;
		}
		i++;
	}
}

void	set_z(t_wolf *wolf)
{
	int		i;
	char	*num;
	char	*txt;

	i = 0;
	while (i < 12)
	{
		txt = ft_itoa(i);
		num = ft_strjoin(txt, ".xpm");
		free(txt);
		txt = ft_strjoin("./spr/z/", num);
		free(num);
		if (!(wolf->z[i++] = mlx_xpm_file_to_image(wolf->mlx, txt, &wolf->texs,
				&wolf->texs)))
		{
			free(txt);
			error(wolf, "Texture initialization!");
		}
		free(txt);
	}
}

void	set_txr(t_wolf *wolf)
{
	void	*tmp;
	char	*num;
	char	*txt;
	int		i;

	i = 0;
	while (i < TXR_NUM)
	{
		txt = ft_itoa(i);
		num = ft_strjoin(txt, ".xpm");
		free(txt);
		txt = ft_strjoin("./spr/", num);
		free(num);
		if (!(tmp = mlx_xpm_file_to_image(wolf->mlx, txt, &wolf->texs,
				&wolf->texs)))
		{
			free(txt);
			error(wolf, "Texture initialization!");
		}
		wolf->texture[i++] = (int*)mlx_get_data_addr(tmp, &wolf->bpp,
								&wolf->size_l, &wolf->endian);
		free(txt);
	}
}

void	clear_image(t_wolf *wolf)
{
	int i;
	int j;

	i = 0;
	while (i < WIN_S)
	{
		j = 0;
		while (j < WIN_S)
			put_pxl_to_img(wolf, j++, i, 0x000000);
		i++;
	}
}
