/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_input.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 19:54:02 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/31 19:54:03 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		key_hook1(int keycode, t_wolf *wolf)
{
	if (keycode == 53)
		quit(wolf);
	else if (keycode == 126 || keycode == 13)
	{
		clear_image(wolf);
		if (wolf->map[(int)(wolf->p_x + wolf->d_x
			* wolf->ms * 1.5)][(int)wolf->p_y] == 0
		|| wolf->map[(int)(wolf->p_x + wolf->d_x
			* wolf->ms * 1.5)][(int)wolf->p_y] == 9)
			wolf->p_x += wolf->d_x * (wolf->ms);
		if (wolf->map[(int)(wolf->p_x)][(int)(wolf->p_y
			+ wolf->d_y * wolf->ms * 1.5)] == 0
		|| wolf->map[(int)(wolf->p_x)][(int)(wolf->p_y
			+ wolf->d_y * wolf->ms * 1.5)] == 9)
			wolf->p_y += wolf->d_y * wolf->ms;
		scene(wolf);
	}
	return (0);
}

int		key_hook2(int keycode, t_wolf *wolf)
{
	if (keycode == 8)
	{
		if (wolf->compas)
			wolf->compas = 0;
		else
			wolf->compas = 1;
		clear_image(wolf);
		scene(wolf);
	}
	else if (keycode == 125 || keycode == 1)
	{
		clear_image(wolf);
		if (wolf->map[(int)(wolf->p_x - wolf->d_x
			* wolf->ms * 1.5)][(int)wolf->p_y] == 0
		|| wolf->map[(int)(wolf->p_x - wolf->d_x
			* wolf->ms * 1.5)][(int)wolf->p_y] == 9)
			wolf->p_x -= wolf->d_x * (wolf->ms);
		if (wolf->map[(int)(wolf->p_x)][(int)(wolf->p_y
			- wolf->d_y * wolf->ms * 1.5)] == 0
		|| wolf->map[(int)(wolf->p_x)][(int)(wolf->p_y
			- wolf->d_y * wolf->ms * 1.5)] == 9)
			wolf->p_y -= wolf->d_y * wolf->ms;
		scene(wolf);
	}
	return (0);
}

int		key_hook3(int keycode, t_wolf *wolf)
{
	double	ox;
	double	opx;

	if (keycode == 124 || keycode == 2)
	{
		clear_image(wolf);
		ox = wolf->d_x;
		wolf->d_x = wolf->d_x * cos(-wolf->rs) - wolf->d_y * sin(-wolf->rs);
		wolf->d_y = ox * sin(-wolf->rs) + wolf->d_y * cos(-wolf->rs);
		opx = wolf->pl_x;
		wolf->pl_x = wolf->pl_x * cos(-wolf->rs) - wolf->pl_y * sin(-wolf->rs);
		wolf->pl_y = opx * sin(-wolf->rs) + wolf->pl_y * cos(-wolf->rs);
		scene(wolf);
	}
	return (0);
}

int		key_hook4(int keycode, t_wolf *wolf)
{
	double	ox;
	double	opx;

	if (keycode == 123 || keycode == 0)
	{
		clear_image(wolf);
		ox = wolf->d_x;
		wolf->d_x = wolf->d_x * cos(wolf->rs) - wolf->d_y * sin(wolf->rs);
		wolf->d_y = ox * sin(wolf->rs) + wolf->d_y * cos(wolf->rs);
		opx = wolf->pl_x;
		wolf->pl_x = wolf->pl_x * cos(wolf->rs) - wolf->pl_y * sin(wolf->rs);
		wolf->pl_y = opx * sin(wolf->rs) + wolf->pl_y * cos(wolf->rs);
		scene(wolf);
	}
	return (0);
}

int		key_hook5(int keycode, t_wolf *wolf)
{
	if (keycode == 49)
	{
		if (wolf->dstwall < 2)
		{
			if (wolf->map[(int)(wolf->p_x - wolf->dstwall
				+ wolf->d_x * wolf->ms)][(int)wolf->p_y] == 5)
			{
				wolf->map[(int)(wolf->p_x - wolf->dstwall
					+ wolf->d_x * wolf->ms)][(int)wolf->p_y] = 0;
				wolf->pt++;
			}
			if (wolf->map[(int)(wolf->p_x)][(int)(wolf->p_y
				+ wolf->dstwall + wolf->d_y * wolf->ms)] == 5)
			{
				wolf->map[(int)(wolf->p_x)][(int)(wolf->p_y
					+ wolf->dstwall + wolf->d_y * wolf->ms)] = 0;
				wolf->pt++;
			}
			addition_hook5(wolf);
		}
		scene(wolf);
	}
	return (0);
}
