/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 16:55:45 by agalayko          #+#    #+#             */
/*   Updated: 2018/06/22 16:55:46 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static int	*parse_line(char *line, t_wolf *wolf)
{
	int		i;
	int		*ret;
	char	**mas;

	if (!(ret = (int*)malloc(sizeof(int) * wolf->width)))
		return (0);
	i = 0;
	mas = ft_strsplit(line, ' ');
	while (mas[i])
	{
		ret[i] = atoi(mas[i]);
		if (ret[i] < 0 || ret[i] > 9 || ret[i] == 8 || !check_digit(mas[i]))
			error(wolf, "Wrong map!");
		i++;
	}
	free(line);
	ft_free_mas_char(mas);
	return (ret);
}

void		read_file(char *file, t_wolf *wolf)
{
	int		fd;
	int		i;
	char	*line;

	i = 0;
	wolf->map = (int**)malloc(sizeof(int*) * wolf->height);
	while (i < wolf->height)
		wolf->map[i++] = NULL;
	i = 0;
	fd = open(file, O_RDONLY);
	while (get_next_line(fd, &line) == 1)
	{
		if (line[0] == '\n')
			continue ;
		wolf->map[i] = parse_line(line, wolf);
		i++;
	}
	close(fd);
}

static int	get_width(char *line, int check)
{
	int		i;
	int		width;

	i = 0;
	width = 0;
	while (line[i])
	{
		if (check)
		{
			width++;
			while (line[i] != ' ' && line[i] != '\0')
				i++;
			check = 0;
		}
		else
		{
			check = 1;
			while (line[i] == ' ' && line[i] != '\0')
				i++;
		}
		if (width > 1000)
			break ;
	}
	return (width);
}

static int	ft_read(int fd, t_wolf *wolf, int ret)
{
	int		width;
	int		height;
	char	*line;

	width = 0;
	height = 0;
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		width = get_width(line, 0);
		if (!(wolf->width))
			wolf->width = width;
		if (wolf->width != width || wolf->width > 1000 || height > 1000
			|| line[0] == '\0')
		{
			free(line);
			if (line[0] == '\0')
				error(wolf, "Empty lines!");
			error(wolf, "Wrong size || size > 1000");
		}
		free(line);
		height++;
	}
	wolf->height = height;
	return (ret);
}

void		first_read(char *file, t_wolf *wolf)
{
	int		fd;
	int		ret;

	wolf->width = 0;
	wolf->height = 0;
	fd = open(file, O_RDONLY);
	ret = ft_read(fd, wolf, 0);
	close(fd);
	if (ret == -1)
		error(wolf, "File is closed or wrong name!");
	else if (wolf->height < 5 || wolf->width < 5)
		error(wolf, "Wrong size");
}
