/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:31:46 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:31:49 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(char *src)
{
	char	*a;
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (src[i] != '\0')
		i++;
	a = (char*)malloc(sizeof(*a) * (i + 1));
	if (!a)
		return (NULL);
	while (j <= i)
		a[j++] = *src++;
	a[j] = '\0';
	return (a);
}
