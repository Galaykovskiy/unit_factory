/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/30 19:22:17 by agalayko          #+#    #+#             */
/*   Updated: 2018/06/30 19:22:18 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s1, size_t n)
{
	char	*s2;
	size_t	j;

	if (!(s2 = (char*)malloc(n * sizeof(char) + 1)))
		return (0);
	j = 0;
	while (*s1 && j < n)
	{
		*(s2 + j) = *(s1 + j);
		j++;
	}
	while (j <= n)
	{
		*(s2 + j) = '\0';
		j++;
	}
	return (s2);
}
