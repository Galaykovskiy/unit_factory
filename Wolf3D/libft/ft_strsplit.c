/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 18:13:31 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 18:13:41 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	void	copy(char *dest, const char *src, int n)
{
	int i;

	i = 0;
	while (i < n)
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
}

static	int		cut(const char *str, int words, char **r, char ch)
{
	int w;
	int start;
	int end;

	w = 0;
	start = 0;
	while (w < words && str[start])
	{
		while (str[start] == ch)
			start++;
		end = start;
		while (str[end] && str[end] != ch)
			end++;
		if (!(r[w] = malloc(end - start + 1)))
		{
			ft_free_mas_char(r);
			return (0);
		}
		copy(r[w], str + start, end - start);
		start = end + 1;
		w++;
	}
	return (1);
}

char			**ft_strsplit(const char *s, char c)
{
	int		words;
	char	**r;

	if (!s)
		return (NULL);
	words = ft_count_words(s, c);
	if (!words)
	{
		if (!(r = malloc(sizeof(*r))))
			return (NULL);
		r[0] = NULL;
		return (r);
	}
	if (!(r = malloc((words + 1) * sizeof(*r))))
		return (NULL);
	if (!cut(s, words, r, c))
		return (NULL);
	r[words] = 0;
	return (r);
}
