/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 18:51:37 by agalayko          #+#    #+#             */
/*   Updated: 2018/04/06 18:51:40 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int	ft_gnl_external(t_list *lst, char **line, char *buf, int i)
{
	char	*tmp;
	char	*temp;

	temp = lst->content;
	lst->content = *line;
	tmp = ft_strsub(buf, 0, i++);
	if (!lst->content)
		*line = ft_strjoin("", tmp);
	else
		*line = ft_strjoin(lst->content, tmp);
	ft_strdel(&tmp);
	tmp = lst->content;
	lst->content = ft_strsub(buf, i, ft_strlen(buf));
	free(tmp);
	free(temp);
	return (1);
}

static int	ft_gnl(t_list *lst, char **line, char *buf)
{
	char	*tmp;
	char	*temp;
	int		i;

	i = 0;
	temp = lst->content;
	while (buf[i] && buf[i] != '\n')
		i++;
	if (buf[i] == '\n')
		return (ft_gnl_external(lst, line, buf, i));
	else
	{
		tmp = *line;
		if (buf[0] == '\0')
			*line = NULL;
		else if (tmp)
			*line = ft_strjoin(tmp, buf);
		else
			*line = ft_strjoin("", buf);
		ft_strdel(&tmp);
		lst->content = NULL;
		free(temp);
		return (0);
	}
}

static int	ft_read(t_list *lst, char **line)
{
	int		ret;
	char	buf[BUFF_SIZE + 1];

	while ((ret = read(lst->content_size, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = '\0';
		if (ft_gnl(lst, line, buf))
			break ;
	}
	if (ret == -1)
		return (-1);
	else if (*line)
		return (1);
	else
		return (0);
}

int			get_next_line(const int fd, char **line)
{
	static t_list	*lst;
	t_list			*begin;

	if (fd < 0 || BUFF_SIZE <= 0 || !line)
		return (-1);
	*line = NULL;
	begin = lst;
	while (begin && begin->content_size != (size_t)(fd))
		begin = begin->next;
	if (!begin)
	{
		ft_lstadd(&lst, ft_lstnew(NULL, fd));
		lst->content_size = (size_t)(fd);
		return (ft_read(lst, line));
	}
	else
	{
		if (begin->content && ft_gnl(begin, line, begin->content))
			return (1);
		return (ft_read(begin, line));
	}
}
