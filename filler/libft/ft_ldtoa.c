/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dtoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 13:21:33 by agalayko          #+#    #+#             */
/*   Updated: 2018/12/13 13:21:34 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_cpy(char *dst, long double nbr)
{
	int		i;
	char	*tmp;

	i = 0;
	tmp = ft_itoa((int)nbr);
	while (tmp[i])
	{
		dst[i] = tmp[i];
		i++;
	}
	ft_strdel(&tmp);
	return (i);
}

static int	ft_len(int nbr)
{
	int		i;

	i = 1;
	if (nbr < 0)
		i++;
	while (nbr < -9 || nbr > 9)
	{
		nbr /= 10;
		i++;
	}
	return (i);
}

static int	nbr2(long double nbr)
{
	int		j;

	j = (int)nbr;
	nbr -= (int)nbr;
	while (nbr-- > 0.999999)
		j++;
	return (j);
}

static void	temp(int afterpoint, long double nbr, char *ret, int start)
{
	int		i;
	int		j;
	char	*tmp;
	char	*str;

	i = 0;
	tmp = (char*)malloc(sizeof(char) * afterpoint + 1);
	while (i < afterpoint)
		tmp[i++] = '0';
	tmp[i] = '\0';
	i = 0;
	j = 0;
	str = ft_itoa(nbr2(nbr));
	while (tmp[i] && str[j])
		tmp[i++] = str[j++];
	ft_strdel(&str);
	j = 0;
	while (tmp[j])
		ret[start++] = tmp[j++];
	ft_strdel(&tmp);
}

char		*ft_ldtoa(long double nbr, int afterpoint)
{
	char	*ret;
	int		i;
	int		j;

	i = ft_len((int)nbr);
	if (afterpoint > 0)
		i += 1 + afterpoint;
	ret = (char *)malloc(sizeof(char) * i + 1);
	ret[i] = '\0';
	i = ft_cpy(ret, nbr);
	if (afterpoint > 0)
		ret[i++] = '.';
	if (nbr < 0)
		nbr *= -1;
	nbr -= (long double)((int)nbr);
	j = afterpoint;
	while (j--)
		nbr *= 10;
	temp(afterpoint, nbr, ret, i);
	return (ret);
}
