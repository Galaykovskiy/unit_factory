/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_countseg.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 15:14:48 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 15:14:49 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_countseg(char const *s, char c)
{
	size_t	size;

	size = 0;
	while (*s && *s != c)
	{
		++size;
		++s;
	}
	return (size);
}
