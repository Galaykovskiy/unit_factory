/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hextoint.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/10 19:44:54 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/10 19:44:55 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_hextoint(char *hex)
{
	int		ret;
	char	byte;

	ret = 0;
	while (*hex)
	{
		byte = *hex++;
		if (byte >= '0' && byte <= '9')
			byte -= '0';
		else if (byte >= 'a' && byte <= 'f')
			byte -= 'a' + 10;
		else if (byte >= 'A' && byte <= 'F')
			byte -= 'A' + 10;
		ret = (ret << 4) | (byte & 0xF);
	}
	return (ret);
}
