/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visual.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 20:53:52 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 20:53:53 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VISUALISATEUR_H
# define VISUALISATEUR_H

# include "../../includes/filler.h"
# include "../../libft/libft.h"
# include <mlx.h>
# define WIDTH 1200
# define HEIGHT 600
# define WINDOW_NAME "Filler by agalayko"
# define TITLE_IMAGE "sprites/fond.xpm"

typedef struct		s_vis
{
	char			*p1;
	char			*p2;
	float			score_p1;
	float			score_p2;
	char			**map;
	int				h;
	int				l;
	int				r;
	int				g;
	int				b;
	int				map_size_x;
	int				map_size_y;
	char			*ret;
	void			*ret2;
	void			*win;
	void			*mlx;
	void			*img;
	void			*img2;
	int				bpp;
	int				size_l;
	int				endian;
	int				pause;
}					t_vis;

void				modif_color(int r, int v, int b, t_vis *vis);
void				draw_score(t_vis *vis);
void				calc_score(t_vis *vis);
void				draw_rectangle(int start_x, int start_y, t_vis *vis);
void				draw(t_vis *vis);
int					is_number(char c);
int					is_aly_adj(t_vis *vis, int i, int j);
void				print_final(t_vis *vis);
void				draw_title(t_vis *vis);
void				draw_score(t_vis *vis);
int					is_number(char c);
//void				get_map_size(char *line, t_vis *p);
void				read_output(t_vis *vis);
void				modif_color(int r, int v, int b, t_vis *vis);
void				draw_square(int start_x, int start_y,
								int size, t_vis *vis);
void				draw_background(t_vis *vis);
void				draw_map(t_vis *vis);
void				draw_menu(t_vis *vis);

#endif
