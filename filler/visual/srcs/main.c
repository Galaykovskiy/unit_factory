/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 20:53:12 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 20:53:12 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/visual.h"

static int	loop_key_hook(t_vis *vis)
{
	char	*str;

	if (vis->pause == 0)
	{
		vis->ret = mlx_get_data_addr(vis->img, &(vis->bpp),
		&(vis->size_l), &(vis->endian));
		read_output(vis);
		draw(vis);
		mlx_put_image_to_window(vis->mlx, vis->win, vis->img, 0, 0);
		mlx_put_image_to_window(vis->mlx, vis->win, vis->img2, 0, 0);
		mlx_destroy_image(vis->mlx, vis->img);
		vis->img = mlx_new_image(vis->mlx, WIDTH, HEIGHT);
		print_final(vis);
	}
	if (vis->pause == 1)
	{
		str = "PAUSE";
		mlx_string_put(vis->mlx, vis->win, (WIDTH / 4) - 10, (HEIGHT / 2) - 10,
			0x0FFFFFF, str);
	}
	return (0);
}

int			key_hook(int keycode, t_vis *vis)
{
	if (keycode == 53)
		exit(1);
	loop_key_hook(vis);
	return (0);
}

int			mouse_hook(int button, t_vis *vis)
{
	if (button == 1)
	{
		if (vis->pause == 0)
			vis->pause = 1;
		else if (vis->pause == 1)
			vis->pause = 0;
	}
	loop_key_hook(vis);
	return (0);
}

void		init_env(t_vis *vis)
{
	vis->pause = 0;
	vis->r = 0;
	vis->g = 0;
	vis->b = 0;
	vis->map = NULL;
	vis->score_p1 = 0;
	vis->score_p2 = 0;
	vis->map_size_x = 0;
	vis->map_size_y = 0;
}

int			main(void)
{
	t_vis	*vis;

	vis = (t_vis *)malloc(sizeof(t_vis));
	init_env(vis);
	vis->mlx = mlx_init();
	vis->win = mlx_new_window(vis->mlx, WIDTH, HEIGHT, WINDOW_NAME);
	vis->img = mlx_new_image(vis->mlx, WIDTH, HEIGHT);
	draw_title(vis);
	mlx_hook(vis->win, 2, 2, key_hook, vis);
	mlx_mouse_hook(vis->win, mouse_hook, vis);
	loop_key_hook(vis);
	mlx_loop_hook(vis->mlx, loop_key_hook, vis);
	free(vis->map);
	mlx_loop(vis->mlx);
	free(vis);
	return (0);
}
