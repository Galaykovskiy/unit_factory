/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parcing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 20:53:31 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 20:53:32 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/visual.h"

void		get_player_name(char *line, t_vis *vis)
{
	int i;

	i = 22;
	if (line[9] == 'p' && line[10] == '1')
	{
		while (line[i])
		{
			if (line[i] == '.')
				break ;
			i++;
		}
		vis->p1 = ft_strsub((const char *)line, 23, i - 23);
	}
	if (line[9] == 'p' && line[10] == '2')
	{
		while (line[i])
		{
			if (line[i] == '.')
				break ;
			i++;
		}
		vis->p2 = ft_strsub((const char *)line, 23, i - 23);
	}
	ft_strdel(&line);
}

void		get_map2(t_vis *vis)
{
	int		i;
	char	*line;

	i = -1;
	get_next_line(0, &line);
	ft_strdel(&line);
	vis->map = (char **)malloc(sizeof(char *) * (vis->map_size_y + 1));
	while (++i <= (vis->map_size_y - 1))
	{
		get_next_line(0, &line);
		vis->map[i] = ft_strdup((const char *)(&line[4]));
		ft_strdel(&line);
	}
}

void		get_map_size2(char *line, t_vis *vis)
{
	int i;
	int tmp;

	i = 0;
	vis->map_size_x = 0;
	vis->map_size_y = 0;
	while (line[i])
	{
		tmp = 0;
		while (is_number(line[i]) == 0 && line[i])
		{
			tmp += line[i] - 48;
			if (is_number(line[i + 1]) == 0)
				tmp *= 10;
			i++;
		}
		if (vis->map_size_y == 0)
			vis->map_size_y = tmp;
		else if (vis->map_size_x == 0)
			vis->map_size_x = tmp;
		i++;
	}
	ft_strdel(&line);
	get_map2(vis);
}

void		read_output(t_vis *vis)
{
	char	*line;

	while (get_next_line(0, &line) == 1)
	{
		if (ft_strncmp(line, "$$$", 2) == 0)
			get_player_name(line, vis);
		if (ft_strncmp(line, "Plateau", 6) == 0)
			get_map_size2(line, vis);
		if (ft_strncmp(line, "Piece", 4) == 0)
		{
			ft_strdel(&line);
			return ;
		}
	}
}
