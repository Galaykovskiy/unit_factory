/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 20:53:37 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 20:53:37 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/visual.h"

void		draw_rectangle(int start_x, int start_y, t_vis *vis)
{
	int		i;
	int		j;
	int		k;

	j = 0;
	k = 0;
	while (j < (vis->h * 4))
	{
		i = 0;
		while (i < (vis->l * 4))
		{
			vis->ret[i + k + ((start_y * WIDTH * 4) + (start_x * 4))] = vis->r;
			vis->ret[i + k + 1 + ((start_y * WIDTH * 4)
						+ (start_x * 4))] = vis->g;
			vis->ret[i + k + 2 + ((start_y * WIDTH * 4)
						+ (start_x * 4))] = vis->b;
			vis->ret[i + k + 3 + ((start_y * WIDTH * 4) + (start_x * 4))] = 0;
			i += 4;
		}
		j += 4;
		k += (WIDTH * 4);
	}
}

int			is_aly_adj(t_vis *vis, int i, int j)
{
	if (j + 1 >= vis->map_size_x || j - 1 < 0 || i + 1 >= vis->map_size_y
		|| i - 1 < 0)
		return (0);
	if (vis->map[i][j + 1] == 'X' || vis->map[i][j - 1] == 'X' ||
		vis->map[i][j + 1] == 'O' || vis->map[i][j - 1] == 'O')
		return (1);
	if (vis->map[i + 1][j] == 'X' || vis->map[i - 1][j] == 'X' ||
		vis->map[i + 1][j] == 'O' || vis->map[i - 1][j] == 'O')
		return (1);
	if (j + 2 >= vis->map_size_x || j - 2 < 0 || i + 2 >= vis->map_size_y
		|| i - 2 < 0)
		return (0);
	if (vis->map[i][j + 2] == 'X' || vis->map[i][j - 2] == 'X' ||
		vis->map[i][j + 2] == 'O' || vis->map[i][j - 2] == 'O')
		return (2);
	if (vis->map[i + 2][j] == 'X' || vis->map[i - 2][j] == 'X' ||
		vis->map[i + 2][j] == 'O' || vis->map[i - 2][j] == 'O')
		return (2);
	return (0);
}

void		draw(t_vis *vis)
{
	draw_background(vis);
	draw_menu(vis);
	draw_score(vis);
	draw_map(vis);
}

int			is_number(char c)
{
	if (c >= 48 && c <= 57)
		return (0);
	return (1);
}

void		print_final(t_vis *vis)
{
	char	*str;

	str = ft_itoa(vis->score_p1);
	mlx_string_put(vis->mlx, vis->win, (WIDTH / 2) + 120, (HEIGHT / 2) - 10,
		0x0e74c3c, str);
	str = ft_itoa(vis->score_p2);
	mlx_string_put(vis->mlx, vis->win, (WIDTH / 2) + 120, (HEIGHT / 2) + 110,
		0x03498db, str);
	str = vis->p2;
	mlx_string_put(vis->mlx, vis->win, (WIDTH / 2) + 120, (HEIGHT / 2) - 100,
		0x0e74c3c, str);
	str = vis->p1;
	mlx_string_put(vis->mlx, vis->win, (WIDTH / 2) + 120, (HEIGHT / 2) + 210,
		0x03498db, str);
}
