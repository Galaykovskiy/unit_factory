/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 20:53:44 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 20:53:44 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/visual.h"

void		calc_score(t_vis *vis)
{
	int		i;
	int		i2;

	i = 0;
	vis->score_p1 = 0;
	vis->score_p2 = 0;
	while (i < vis->map_size_y)
	{
		i2 = 0;
		while (i2 < vis->map_size_x)
		{
			if (vis->map[i][i2] == 'X' || vis->map[i][i2] == 'x')
				vis->score_p1 += 1;
			if (vis->map[i][i2] == 'O' || vis->map[i][i2] == 'o')
				vis->score_p2 += 1;
			i2++;
		}
		i++;
	}
}

void		draw_score(t_vis *vis)
{
	calc_score(vis);
	modif_color(56, 60, 150, vis);
	vis->l = WIDTH / 3;
	vis->h = 20;
	draw_rectangle((WIDTH / 2) + 120, (HEIGHT / 3) + 40, vis);
	modif_color(60, 76, 232, vis);
	vis->l = ((WIDTH / 3) * (vis->score_p1 / (vis->map_size_x
				* vis->map_size_y)));
	vis->h = 20;
	draw_rectangle((WIDTH / 2) + 120, (HEIGHT / 3) + 40, vis);
	modif_color(180, 132, 52, vis);
	vis->l = WIDTH / 3;
	vis->h = 20;
	draw_rectangle((WIDTH / 2) + 120, (HEIGHT / 2) + 170, vis);
	modif_color(211, 151, 53, vis);
	vis->l = ((WIDTH / 3) * (vis->score_p2 / (vis->map_size_x
				* vis->map_size_y)));
	vis->h = 20;
	draw_rectangle((WIDTH / 2) + 120, (HEIGHT / 2) + 170, vis);
}

void		modif_color(int r, int v, int b, t_vis *vis)
{
	vis->r = r;
	vis->g = v;
	vis->b = b;
}
