/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 20:53:20 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 20:53:20 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/visual.h"

void		draw_square(int start_x, int start_y, int size, t_vis *vis)
{
	int		i;
	int		i2;
	int		tmp;

	i2 = 0;
	tmp = 0;
	while (i2 < (size * 4))
	{
		i = 0;
		while (i < (size * 4))
		{
			vis->ret[i + tmp + ((start_y * WIDTH * 4) +
				(start_x * 4))] = vis->r;
			vis->ret[i + tmp + 1 + ((start_y * WIDTH * 4) +
				(start_x * 4))] = vis->g;
			vis->ret[i + tmp + 2 + ((start_y * WIDTH * 4) +
				(start_x * 4))] = vis->b;
			vis->ret[i + tmp + 3 + ((start_y * WIDTH * 4) +
				(start_x * 4))] = 0;
			i += 4;
		}
		i2 += 4;
		tmp += (WIDTH * 4);
	}
}

void		draw_background(t_vis *vis)
{
	int		i;

	i = 0;
	while (i < (HEIGHT * WIDTH * 4))
	{
		(vis->ret)[i] = 52;
		(vis->ret)[i + 1] = 52;
		(vis->ret)[i + 2] = 52;
		(vis->ret)[i + 3] = 0;
		i += 4;
	}
}

void		draw_title(t_vis *vis)
{
	int		n;
	int		r;

	vis->img2 = mlx_xpm_file_to_image(vis->mlx, TITLE_IMAGE, &n, &r);
	vis->ret2 = mlx_get_data_addr(vis->img, &(vis->bpp),
		&(vis->size_l), &(vis->endian));
}

void		draw_map(t_vis *vis)
{
	int		i;
	int		j;
	int		s;

	s = ((WIDTH / 2) / (vis->map_size_x)) - 2;
	i = -1;
	while (++i < vis->map_size_y)
	{
		j = -1;
		while (++j < vis->map_size_x)
		{
			if (vis->map[i][j] == '.' && is_aly_adj(vis, i, j) == 2)
				modif_color(50, 50, 50, vis);
			else if (vis->map[i][j] == '.' && is_aly_adj(vis, i, j) == 1)
				modif_color(42, 42, 42, vis);
			else if (vis->map[i][j] == '.')
				modif_color(55, 55, 55, vis);
			else if (vis->map[i][j] == 'X' || vis->map[i][j] == 'x')
				modif_color(60, 76, 232, vis);
			else if (vis->map[i][j] == 'O' || vis->map[i][j] == 'o')
				modif_color(211, 151, 53, vis);
			draw_square((j * s) + (j * 2) + 2, (i * s) + (i * 2) +
				((600 - (vis->map_size_y * s) - (vis->map_size_y * 2)) / 2), s, vis);
		}
	}
}

void		draw_menu(t_vis *vis)
{
	vis->r = 35;
	vis->g = 35;
	vis->b = 35;
	draw_square(WIDTH / 2, 0, WIDTH / 2, vis);
	vis->r = 25;
	vis->g = 25;
	vis->b = 25;
	vis->l = 20;
	vis->h = HEIGHT;
	draw_rectangle(WIDTH / 2, 0, vis);
	draw_rectangle(WIDTH - vis->l, 0, vis);
	vis->l = WIDTH;
	vis->h = 20;
	draw_rectangle(0, 0, vis);
	draw_rectangle(0, HEIGHT - vis->h, vis);
	vis->l = WIDTH / 2;
	vis->h = HEIGHT;
	draw_rectangle(0, 0, vis);
}
