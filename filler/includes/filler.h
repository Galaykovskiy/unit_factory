/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 19:50:54 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 19:50:55 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include "../libft/libft.h"

typedef struct	s_map
{
	char		**map;
	int			player;
	int			map_size_x;
	int			map_size_y;
	char		*en;
	int			pos_enemy_x;
	int			pos_enemy_y;
	int			my_pos_x;
	int			my_pos_y;
	int			first_x;
	int			first_y;
	char		*me;
}				t_map;

typedef struct	s_p
{
	int			tmp_x;
	int			tmp_y;
	int			nbr_contact;
	int			contact;
	int			final_x;
	int			final_y;
	char		**piece;
	int			size_x;
	int			size_y;
	int			real_size_x;
	int			real_size_y;
	int			start_x;
	int			end_x;
	int			start_y;
	int			end_y;
}				t_p;

int				al_1(t_map *map, t_p *p);
int				al_1_2(t_map *map, t_p *p);
int				al_2(t_map *map, t_p *p);
int				al_2_2(t_map *map, t_p *p);
int				al_3(t_map *map, t_p *p);
int				al_3_2(int nbr_contact, t_p *p, t_map *map);

int				is_placable(int i, int i2, t_map *map, t_p *p);
int				is_placable_2(t_p *p, int count, int i, int j);

void			get_contact(t_p *p, t_map *map);
int				count_contact(t_map *map, t_p *p, int y, int x);
int				contact(t_map *map, t_p *p);
int				contact_2(t_map *map, t_p *p);

int				last_try(t_map *map, t_p *p);
void			print_result(t_p *p, t_map *map);
void			wich_player(t_map *map);

void			get_real_piece_size(t_p *p);
void			get_piece(t_p *p);
void			get_piece_size(char *line, t_p *p);

int				last_try(t_map *map, t_p *p);
void			wich_player(t_map *map);
void			print_result(t_p *p, t_map *map);
void			get_player(t_map *map);
int				filler(t_map *map, t_p *p);
int				is_number(char c);
int				get_the_ret(t_map *map, t_p *p);

int				big_map(t_map *map, t_p *p);
int				small_map(t_map *map, t_p *p);
#endif
