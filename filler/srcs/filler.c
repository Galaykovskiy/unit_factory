/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 19:53:15 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 19:53:15 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/filler.h"

int		contact(t_map *map, t_p *p)
{
	int i;
	int j;
	int ret;

	i = map->map_size_y - 1;
	p->final_x = 0;
	p->final_y = 0;
	while (i > 0)
	{
		j = map->map_size_x - 1;
		while (j > 0)
		{
			ret = is_placable(i, j, map, p);
			if (ret == 0)
			{
				print_result(p, map);
				return (0);
			}
			j--;
		}
		i--;
	}
	return (1);
}

int		contact_2(t_map *map, t_p *p)
{
	int i;
	int j;
	int ret;

	i = -1;
	p->final_x = 0;
	p->final_y = 0;
	while (++i < map->map_size_y - 1)
	{
		j = -1;
		while (++j < map->map_size_x - 1)
		{
			ret = is_placable(i, j, map, p);
			if (ret == 0)
			{
				print_result(p, map);
				return (0);
			}
		}
	}
	return (1);
}

void	get_contact(t_p *p, t_map *map)
{
	int	i;
	int	j;

	i = 2;
	while (++i < map->map_size_y - 3)
	{
		j = 2;
		while (++j < map->map_size_x - 3)
		{
			if (map->map[i][j] == map->me[0] || map->map[i][j] == map->me[1])
				if (map->map[i][j + 3] == map->en[0] || map->map[i][j - 3]
															== map->en[0]
					|| map->map[i + 3][j] == map->en[0] || map->map[i - 3][j]
															== map->en[0]
					|| map->map[i][j + 3] == map->en[1] || map->map[i][j - 3]
															== map->en[1]
					|| map->map[i + 3][j] == map->en[1] || map->map[i - 3][j]
															== map->en[1])
				{
					p->contact = 1;
					return ;
				}
		}
	}
}

int		count_contact(t_map *map, t_p *p, int y, int x)
{
	int	i;
	int	j;
	int k;

	i = -1;
	p->nbr_contact = 0;
	while (++i < p->size_y)
	{
		j = -1;
		while (++j < p->size_x)
			if (p->piece[i][j] == '*')
			{
				k = 0;
				while (++k < 4)
					if ((j + x + k) < map->map_size_x && (j + x - k) > 0 &&
						(i + y + k) < map->map_size_y && (i + y - k) > 0)
						if (map->map[i + y][j + x + k] == map->en[0] ||
							map->map[i + y][j + x - k] == map->en[0] ||
							map->map[i + y + k][j + x] == map->en[0] ||
							map->map[i + y - k][j + x] == map->en[0])
							p->nbr_contact += (4 - k);
			}
	}
	return (p->nbr_contact);
}

int		filler(t_map *map, t_p *p)
{
	if (map->map_size_y < 20)
		return (small_map(map, p));
	if (map->map_size_y >= 20)
		return (big_map(map, p));
	return (0);
}
