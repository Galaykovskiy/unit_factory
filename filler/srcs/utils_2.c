/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 19:58:01 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 19:58:02 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/filler.h"

int		is_placable_2(t_p *p, int count, int i, int j)
{
	if (count == 1)
	{
		p->final_x = j;
		p->final_y = i;
		return (0);
	}
	return (1);
}

int		al_3_2(int nbr_contact, t_p *p, t_map *map)
{
	if (nbr_contact == -1)
		return (0);
	p->final_x = p->tmp_x;
	p->final_y = p->tmp_y;
	print_result(p, map);
	return (1);
}

int		al_2(t_map *map, t_p *p)
{
	int	i;
	int	j;
	int	ret;

	i = map->map_size_y;
	p->final_x = 0;
	p->final_y = 0;
	while (i > 0)
	{
		j = map->map_size_x;
		while (j > 0)
		{
			ret = is_placable(i, j, map, p);
			if (ret == 0)
			{
				print_result(p, map);
				return (0);
			}
			j--;
		}
		i--;
	}
	return (1);
}

int		al_2_2(t_map *map, t_p *p)
{
	int	i;
	int	j;
	int	ret;

	i = map->map_size_y;
	p->final_x = 0;
	p->final_y = 0;
	while (i > 0)
	{
		j = -1;
		while (++j < map->map_size_x)
		{
			ret = is_placable(i, j, map, p);
			if (ret == 0)
			{
				print_result(p, map);
				return (0);
			}
		}
		i--;
	}
	return (1);
}
