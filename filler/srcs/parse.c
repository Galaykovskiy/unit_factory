/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 19:53:29 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 19:53:30 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/filler.h"

void			get_player(t_map *map)
{
	char		*line;

	get_next_line(0, &line);
	if (ft_strncmp(line, "$$$", 2) == 0 && map->player == 0)
	{
		if (ft_strstr(line, "p2"))
			map->player = 2;
		if (ft_strstr(line, "p1"))
			map->player = 1;
		wich_player(map);
	}
	ft_strdel(&line);
}

void			get_strat_pos(t_map *map)
{
	int			i;
	static int	j;

	if (j == map->map_size_y)
		return ;
	j = 0;
	while (++j < (map->map_size_y))
	{
		i = 0;
		while (++i < (map->map_size_x))
		{
			if ((map->map[j][i] == map->en[0] ||
				map->map[j][i] == map->en[1]))
			{
				map->pos_enemy_x = i;
				map->pos_enemy_y = j;
			}
			if ((map->map[j][i] == map->me[0] ||
				map->map[j][i] == map->me[1]))
			{
				map->my_pos_x = i;
				map->my_pos_y = j;
			}
		}
	}
}

void			get_map(t_map *map)
{
	int			i;
	char		*line;

	i = -1;
	get_next_line(0, &line);
	ft_strdel(&line);
	map->map = (char **)malloc(sizeof(char *) * (map->map_size_y + 1));
	while (++i <= (map->map_size_y - 1))
	{
		get_next_line(0, &line);
		map->map[i] = ft_strdup((const char *)(&line[4]));
		ft_strdel(&line);
	}
	get_strat_pos(map);
}

void			get_map_size(char *line, t_map *map)
{
	int			i;
	int			tmp;

	i = 0;
	map->map_size_x = 0;
	map->map_size_y = 0;
	while (line[i])
	{
		tmp = 0;
		while (is_number(line[i]) == 0 && line[i])
		{
			tmp += line[i] - 48;
			if (is_number(line[i + 1]) == 0)
				tmp *= 10;
			i++;
		}
		if (map->map_size_y == 0)
			map->map_size_y = tmp;
		else if (map->map_size_x == 0)
			map->map_size_x = tmp;
		i++;
	}
	get_map(map);
}

int				get_the_ret(t_map *map, t_p *p)
{
	char		*line;

	p->piece = NULL;
	while (get_next_line(0, &line) > 0)
	{
		if (ft_strncmp(line, "Plateau", 6) == 0)
		{
			get_map_size(line, map);
			ft_strdel(&line);
		}
		else if (ft_strncmp(line, "Piece", 4) == 0)
		{
			get_piece_size(line, p);
			return (1);
		}
		else
			ft_strdel(&line);
	}
	return (0);
}
