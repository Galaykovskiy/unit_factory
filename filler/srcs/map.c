/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 19:57:33 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 19:57:34 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/filler.h"

int		big_map(t_map *map, t_p *p)
{
	int ret;

	get_contact(p, map);
	if (p->contact == 0)
	{
		if (map->my_pos_y < map->pos_enemy_y)
		{
			if (contact(map, p) == 1)
				return (1);
		}
		else if (map->my_pos_y >= map->pos_enemy_y)
		{
			if (contact_2(map, p) == 1)
				return (1);
		}
	}
	else
	{
		ret = al_3(map, p);
		if (ret == 0)
			return (1);
	}
	return (0);
}

int		small_map(t_map *map, t_p *p)
{
	if (map->my_pos_y < (map->map_size_y / 2))
	{
		if (map->my_pos_x < (map->map_size_x / 2))
		{
			if (al_2(map, p) == 1)
				return (1);
		}
		else if (map->my_pos_x >= (map->map_size_x / 2))
			if (al_2_2(map, p) == 1)
				return (1);
	}
	else if (map->my_pos_y >= (map->map_size_y / 2))
	{
		if (map->my_pos_x < (map->map_size_x / 2))
		{
			if (al_1(map, p) == 1)
				return (1);
		}
		else if (map->my_pos_x >= (map->map_size_x / 2))
			if (al_1_2(map, p) == 1)
				return (1);
	}
	return (0);
}

int		al_3(t_map *map, t_p *p)
{
	int	i;
	int	j;
	int	nbr_contact_tmp;
	int	nbr_contact;

	nbr_contact = -1;
	i = map->map_size_y - (p->size_y - p->end_y);
	p->tmp_x = 0;
	p->tmp_y = 0;
	while (--i >= 0)
	{
		j = map->map_size_x - (p->size_x - p->end_x);
		while (--j >= 0)
			if (is_placable(i, j, map, p) == 0)
			{
				nbr_contact_tmp = count_contact(map, p, i, j);
				if (nbr_contact_tmp > nbr_contact)
				{
					nbr_contact = nbr_contact_tmp;
					p->tmp_x = j;
					p->tmp_y = i;
				}
			}
	}
	return (al_3_2(nbr_contact, p, map));
}

int		al_1(t_map *map, t_p *p)
{
	int	i;
	int	j;
	int	ret;

	i = -1;
	p->final_x = 0;
	p->final_y = 0;
	while (++i < map->map_size_y)
	{
		j = map->map_size_x;
		while (--j > 0)
		{
			ret = is_placable(i, j, map, p);
			if (ret == 0)
			{
				print_result(p, map);
				return (0);
			}
		}
	}
	return (1);
}

int		al_1_2(t_map *map, t_p *p)
{
	int	i;
	int	j;
	int	ret;

	i = -1;
	p->final_x = 0;
	p->final_y = 0;
	while (++i < map->map_size_y)
	{
		j = -1;
		while (++j < map->map_size_x)
		{
			ret = is_placable(i, j, map, p);
			if (ret == 0)
			{
				print_result(p, map);
				return (0);
			}
		}
	}
	return (1);
}
