/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/02 19:57:57 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/02 19:57:58 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/filler.h"

int		is_number(char c)
{
	if (c >= 48 && c <= 57)
		return (0);
	return (1);
}

void	wich_player(t_map *map)
{
	if (map->player == 1)
	{
		map->en = "Xx";
		map->me = "Oo";
	}
	if (map->player == 2)
	{
		map->en = "Oo";
		map->me = "Xx";
	}
}

int		is_placable(int i, int j, t_map *map, t_p *p)
{
	int k;
	int k2;
	int count;

	k = -1;
	count = 0;
	if (i + p->size_y > map->map_size_y || j + p->size_x > map->map_size_x)
		return (1);
	while (++k <= (p->size_y - 1))
	{
		k2 = -1;
		while (++k2 <= (p->size_x - 1))
		{
			if (p->piece[k][k2] == '*' && (map->map[i + k][j + k2] ==
				map->en[0] || map->map[i + k][j + k2] ==
				map->en[0] - 32))
				return (1);
			if (p->piece[k][k2] == '*' && (map->map[i + k][j + k2] ==
				map->me[0] || map->map[i + k][j + k2] == map->me[0] - 32))
				count++;
		}
	}
	if (is_placable_2(p, count, i, j) == 0)
		return (0);
	return (1);
}

void	print_result(t_p *p, t_map *map)
{
	ft_putnbr(p->final_y);
	ft_putchar(' ');
	ft_putnbr(p->final_x);
	ft_putchar('\n');
	map->my_pos_x = p->final_x;
	map->my_pos_y = p->final_y;
}

int		last_try(t_map *map, t_p *p)
{
	int	i;
	int	j;
	int	ret;

	i = -1;
	p->final_x = 0;
	p->final_y = 0;
	while (++i < map->map_size_y - 1)
	{
		j = -1;
		while (++j < map->map_size_x - 1)
		{
			ret = is_placable(i, j, map, p);
			if (ret == 0)
			{
				print_result(p, map);
				return (0);
			}
		}
	}
	return (1);
}
