/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 16:55:45 by agalayko          #+#    #+#             */
/*   Updated: 2018/06/22 16:55:46 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static t_cell	**parse_line(char **line, t_fdf *fdf, int height)
{
	char	**mas;
	int		i;
	int		len;
	t_cell	**cell;

	i = 0;
	len = 0;
	mas = ft_strsplit(*line, ' ');
	while (mas[len])
		len++;
	cell = (t_cell**)malloc(sizeof(*cell) * len);
	while (mas[i])
	{
		cell[i] = parse_cell(mas[i], fdf);
		cell[i]->y = height;
		cell[i]->x = i;
		i++;
	}
	ft_free_mas(mas);
	free(*line);
	return (cell);
}

void			read_file(char *file, t_fdf *fdf, t_cell ***cell)
{
	int		fd;
	int		i;
	char	*line;

	i = 0;
	fd = open(file, O_RDONLY);
	while (get_next_line(fd, &line) == 1)
	{
		if (line[0] == '\n')
			continue ;
		cell[i] = parse_line(&line, fdf, i);
		i++;
	}
	close(fd);
}

static int		get_width(char *line, int check)
{
	int		i;
	int		width;

	i = 0;
	width = 0;
	while (line[i])
	{
		if (check)
		{
			width++;
			while (line[i] != ' ' && line[i] != '\0')
				i++;
			check = 0;
		}
		else
		{
			check = 1;
			while (line[i] == ' ' && line[i] != '\0')
				i++;
		}
		if (width > 1000)
			break ;
	}
	return (width);
}

static int		ft_read(int fd, t_fdf *fdf, int ret)
{
	int		width;
	int		height;
	char	*line;

	width = 0;
	height = 0;
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		width = get_width(line, 0);
		if (!(fdf->width))
			fdf->width = width;
		if (line[0] == '\0')
			continue ;
		if (fdf->width != width || fdf->width > 1000 || height > 1000)
		{
			fdf->error = 1;
			fdf->message = "ERROR! Wrong width or width || height > 1000";
			free(line);
			break ;
		}
		free(line);
		height++;
	}
	fdf->height = height;
	return (ret);
}

void			first_read(char *file, t_fdf *fdf)
{
	int		fd;
	int		ret;

	fd = open(file, O_RDONLY);
	ret = ft_read(fd, fdf, 0);
	close(fd);
	if (ret == -1)
	{
		fdf->error = 1;
		fdf->message = "ERROR! File is closed.";
	}
}
