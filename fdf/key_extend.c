/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_extend.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 17:56:25 by agalayko          #+#    #+#             */
/*   Updated: 2018/06/29 17:56:28 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	change_color(t_cell ***cell, t_fdf *fdf, int k)
{
	int		i;
	int		j;

	i = 0;
	while (i < fdf->height)
	{
		j = 0;
		while (j < fdf->width)
		{
			if (cell[i][j]->color + k > 1000000)
				cell[i][j]->color += k;
			j++;
		}
		i++;
	}
}

void	peak_color(t_cell ***cell, t_fdf *fdf)
{
	int		i;
	int		j;

	i = 0;
	while (i < fdf->height)
	{
		j = 0;
		while (j < fdf->width)
		{
			cell[i][j]->color = COLOR_WHITE + cell[i][j]->z * 1000000;
			j++;
		}
		i++;
	}
}

void	cpc(t_cell ***cell, t_fdf *fdf, int k)
{
	int		i;
	int		j;

	i = 0;
	while (i < fdf->height)
	{
		j = 0;
		while (j < fdf->width)
		{
			if (cell[i][j]->z != 0)
			{
				if (cell[i][j]->z < 0)
					cell[i][j]->color -= 1000000 * k;
				else
					cell[i][j]->color += 1000000 * k;
			}
			j++;
		}
		i++;
	}
}
