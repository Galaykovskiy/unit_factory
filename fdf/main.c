/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 16:26:41 by agalayko          #+#    #+#             */
/*   Updated: 2018/06/22 16:26:45 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	check_error(t_fdf *fdf)
{
	if (fdf->error)
	{
		ft_putendl(fdf->message);
		free(fdf);
		return (1);
	}
	return (0);
}

static void	initial(char *file, t_fdf *fdf)
{
	t_cell	***cell;

	first_read(file, fdf);
	if (check_error(fdf))
		exit(1);
	cell = (t_cell***)malloc(sizeof(**cell) * fdf->height);
	read_file(file, fdf, cell);
	if (check_error(fdf))
		exit(1);
	window(cell, fdf);
}

int			main(int argc, char **argv)
{
	t_fdf	*fdf;

	if (argc != 2)
	{
		ft_putendl("ERROR! Usage file hasn't found..");
		return (0);
	}
	fdf = (t_fdf*)malloc(sizeof(*fdf));
	fdf->error = 0;
	fdf->width = 0;
	fdf->rx = -0.3;
	fdf->ry = -0.3;
	fdf->rz = 0;
	initial(argv[1], fdf);
	return (0);
}
