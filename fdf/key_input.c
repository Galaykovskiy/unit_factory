/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_input.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/23 19:12:25 by agalayko          #+#    #+#             */
/*   Updated: 2018/06/23 19:12:26 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	change_height(t_cell ***cell, t_fdf *fdf, int k)
{
	int i;
	int j;

	i = 0;
	while (i < fdf->height)
	{
		j = 0;
		while (j < fdf->width)
		{
			if (cell[i][j]->z + k != k && cell[i][j]->z + k != -k
				&& cell[i][j]->z != 0)
				cell[i][j]->z += k;
			j++;
		}
		i++;
	}
}

static void	key_cell_fdf(int key, t_cell ***cell, t_fdf *fdf)
{
	if (key == 6)
		change_height(cell, fdf, -1);
	else if (key == 7)
		change_height(cell, fdf, 1);
	else if (key == 116)
		change_color(cell, fdf, 100000);
	else if (key == 121)
		change_color(cell, fdf, -100000);
	else if (key == 8)
		peak_color(cell, fdf);
	else if (key == 115)
		cpc(cell, fdf, 1);
	else if (key == 119)
		cpc(cell, fdf, -1);
}

static void	control(int key, t_fdf *fdf)
{
	if (key == 126)
		fdf->sy -= fdf->scale / 3;
	else if (key == 125)
		fdf->sy += fdf->scale / 3;
	else if (key == 124)
		fdf->sx += fdf->scale / 3;
	else if (key == 123)
		fdf->sx -= fdf->scale / 3;
	else if (key == 69)
	{
		if ((fdf->scale + fdf->scale / 50) < fdf->conts * 5)
		{
			fdf->scale += fdf->scale / 50;
			fdf->sx -= fdf->scale / fdf->width;
			fdf->sy -= fdf->scale / fdf->height;
		}
	}
	else if (key == 78)
		if ((fdf->scale - fdf->scale / 50) > fdf->conts / 5)
		{
			fdf->scale -= fdf->scale / 50;
			fdf->sx += fdf->scale / fdf->width;
			fdf->sy += fdf->scale / fdf->height;
		}
}

static void	rotate(int key, t_fdf *fdf)
{
	if (key == 12)
	{
		if (fdf->rz == -1)
			fdf->rz = 1;
		fdf->rz -= 0.05;
	}
	else if (key == 14)
	{
		if (fdf->rz == 1)
			fdf->rz = -1;
		fdf->rz += 0.05;
	}
	else if (key == 13)
	{
		if (fdf->rx == 1)
			fdf->rx = -1;
		fdf->rx -= 0.05;
	}
	else if (key == 1)
	{
		if (fdf->rx == -1)
			fdf->rx = 1;
		fdf->rx += 0.05;
	}
}

int			key_handler(int key, void *param)
{
	mlx_clear_window(((t_draw*)param)->fdf->mlx, ((t_draw*)param)->fdf->win);
	if (key == 53)
	{
		exit(1);
		return (1);
	}
	else if (key == 0)
	{
		if (((t_draw*)param)->fdf->ry == 1)
			((t_draw*)param)->fdf->ry = -1;
		((t_draw*)param)->fdf->ry += 0.05;
	}
	else if (key == 2)
	{
		if (((t_draw*)param)->fdf->ry == -1)
			((t_draw*)param)->fdf->ry = 1;
		((t_draw*)param)->fdf->ry -= 0.05;
	}
	key_cell_fdf(key, ((t_draw*)param)->cell, ((t_draw*)param)->fdf);
	control(key, ((t_draw*)param)->fdf);
	rotate(key, ((t_draw*)param)->fdf);
	draw_grid(((t_draw*)param)->cell, ((t_draw*)param)->fdf);
	return (0);
}
