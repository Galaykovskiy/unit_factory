/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 16:57:43 by agalayko          #+#    #+#             */
/*   Updated: 2018/06/22 16:57:44 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static t_cell	xyz(t_cell cell, t_fdf *fdf, t_cell c)
{
	t_cell p0;
	t_cell p1;
	t_cell p2;

	p0.x = cell.x;
	p0.y = (cell.y - c.y) * cos(fdf->rx)
						+ (cell.z - c.z) / 5 * sin(fdf->rx) + c.y;
	p0.z = (cell.z - c.z) / 5 * cos(fdf->rx)
						- (cell.y - c.y) * sin(fdf->rx) + c.z;
	p1.x = (p0.x - c.x) * cos(fdf->ry) - (p0.z - c.z) * sin(fdf->ry) + c.x;
	p1.y = p0.y;
	p1.z = (p0.z - c.z) * cos(fdf->ry) + (p0.x - c.x) * sin(fdf->ry) + c.z;
	p2.x = (p1.x - c.x) * cos(fdf->rz) + (p1.y - c.y) * sin(fdf->rz) + c.x;
	p2.y = (p1.y - c.y) * cos(fdf->rz) - (p1.x - c.x) * sin(fdf->rz) + c.y;
	p2.color = cell.color;
	return (p2);
}

static void		draw_line(t_cell s, t_cell e, t_fdf *fdf)
{
	double	x;
	double	y;

	x = s.x;
	y = s.y;
	s.color = e.color;
	if (abs(s.x - e.x) > abs(s.y - e.y))
		while ((s.x > e.x ? x > e.x : x < e.x) && (x > 0 || x < fdf->win_w))
		{
			x > e.x ? x-- : x++;
			y = (int)(((x - s.x) / (e.x - s.x)) * (e.y - s.y) + s.y);
			mlx_pixel_put(fdf->mlx, fdf->win, x, y, s.color);
		}
	else
		while ((s.y > e.y ? y > e.y : y < e.y) && (y > 0 || y < fdf->win_h))
		{
			y > e.y ? y-- : y++;
			x = (int)(((y - s.y) * (e.x - s.x) / (e.y - s.y)) + s.x);
			mlx_pixel_put(fdf->mlx, fdf->win, x, y, s.color);
		}
}

static void		draw_grid_ext(t_fdf *fdf, t_cell s, t_cell ***cell, int *ij)
{
	t_cell	end;
	t_cell	c;

	c.x = cell[fdf->height / 2][fdf->width / 2]->x * fdf->scale + fdf->sx;
	c.y = cell[fdf->height / 2][fdf->width / 2]->y * fdf->scale + fdf->sy;
	c.z = cell[fdf->height / 2][fdf->width / 2]->z * fdf->scale;
	if (ij[0] + 1 < fdf->height)
	{
		end.x = s.x;
		end.y = s.y + fdf->scale;
		end.z = cell[ij[0] + 1][ij[1]]->z * fdf->scale;
		end.color = cell[ij[0] + 1][ij[1]]->color;
		draw_line(xyz(s, fdf, c), xyz(end, fdf, c), fdf);
	}
	s.y = cell[ij[0]][ij[1]]->y * fdf->scale + fdf->sy;
	if (ij[1] + 1 < fdf->width)
	{
		end.x = s.x + fdf->scale;
		end.y = s.y;
		end.z = cell[ij[0]][ij[1] + 1]->z * fdf->scale;
		end.color = cell[ij[0]][ij[1] + 1]->color;
		draw_line(xyz(s, fdf, c), xyz(end, fdf, c), fdf);
	}
}

void			draw_grid(t_cell ***cell, t_fdf *fdf)
{
	int		*ij;
	t_cell	start;

	ij = (int*)malloc(sizeof(ij) * 2);
	ij[0] = 0;
	while (ij[0] < fdf->height)
	{
		ij[1] = 0;
		while (ij[1] < fdf->width)
		{
			start.color = cell[ij[0]][ij[1]]->color;
			start.x = cell[ij[0]][ij[1]]->x * fdf->scale + fdf->sx;
			start.y = cell[ij[0]][ij[1]]->y * fdf->scale + fdf->sy;
			start.z = cell[ij[0]][ij[1]]->z * fdf->scale;
			draw_grid_ext(fdf, start, cell, ij);
			ij[1]++;
		}
		ij[0]++;
	}
	free(ij);
}

void			window(t_cell ***cell, t_fdf *fdf)
{
	t_draw	*draw;

	fdf->win_w = 1280;
	fdf->win_h = 720;
	draw = (t_draw*)malloc(sizeof(draw));
	draw->cell = cell;
	draw->fdf = fdf;
	if (fdf->width > fdf->height)
		fdf->scale = fdf->win_w / (float)fdf->width;
	else
		fdf->scale = fdf->win_h / (float)fdf->height;
	fdf->conts = fdf->scale;
	fdf->sx = (fdf->win_w - (fdf->width - 1) * fdf->scale) / 2;
	fdf->sy = (fdf->win_h - (fdf->height - 1) * fdf->scale) / 2;
	fdf->mlx = mlx_init();
	fdf->win = mlx_new_window(fdf->mlx, fdf->win_w, fdf->win_h, "FDF");
	mlx_hook(fdf->win, 2, 1L << 0, key_handler, draw);
	draw_grid(draw->cell, draw->fdf);
	mlx_loop(fdf->mlx);
}
