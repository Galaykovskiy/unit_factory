/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:54:53 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:55:12 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int	ft_len(int n)
{
	int len;

	len = 1;
	if (n < 0)
		len++;
	while (n < -9 || n > 9)
	{
		n /= 10;
		len++;
	}
	return (len);
}

static	int	ft_pow(int p)
{
	int a;

	a = 1;
	while (--p > 0)
		a *= 10;
	return (a);
}

char		*ft_itoa(int n)
{
	char	*a;
	int		len;
	int		i;
	int		k;

	k = 1;
	if (n < 0)
		k = -1;
	len = ft_len(n);
	a = (char*)malloc(sizeof(*a) * (len + 1));
	if (a == NULL)
		return (NULL);
	i = 0;
	if (n < 0)
	{
		a[i++] = '-';
		len--;
	}
	while (len > 0)
	{
		a[i++] = '0' + n / ft_pow(len) * k;
		n %= ft_pow(len--);
	}
	a[i] = '\0';
	return (a);
}
