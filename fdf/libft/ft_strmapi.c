/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:33:13 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:33:16 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int		i;
	int		l;
	char	*a;

	i = 0;
	l = 0;
	if (!s || !f)
		return (NULL);
	while (s[l] != '\0')
		l++;
	a = (char*)malloc(sizeof(*a) * (l + 1));
	if (!a)
		return (NULL);
	while (i < l)
	{
		a[i] = f(i, s[i]);
		i++;
	}
	a[i] = '\0';
	return (a);
}
