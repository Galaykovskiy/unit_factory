/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_cell.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/23 13:46:10 by agalayko          #+#    #+#             */
/*   Updated: 2018/06/23 13:46:11 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_color(char *hex)
{
	int		ret;
	char	byte;

	ret = 0;
	while (*hex)
	{
		byte = *hex++;
		if (byte >= '0' && byte <= '9')
			byte -= '0';
		else if (byte >= 'a' && byte <= 'f')
			byte -= 'a' + 10;
		else if (byte >= 'A' && byte <= 'F')
			byte -= 'A' + 10;
		ret = (ret << 4) | (byte & 0xF);
	}
	return (ret);
}

static int	parse_color(char *col)
{
	int		i;
	char	*tmp;

	i = 0;
	if (ft_strlen(col) > 1 && col[i] == 'x')
	{
		i = 1;
		while (i < 8)
		{
			if ((col[i] < 'a' && col[i] > 'f') && (col[i] < 'A' &&
				col[i] > 'F') && (col[i] < 0 && col[i] > 0))
				return (COLOR_WHITE);
			i++;
		}
		i = 1;
		tmp = ft_strsub(col, 1, 8);
		i = ft_color(tmp);
		free(tmp);
		return (i);
	}
	else if (col[i] >= '1' && col[i] <= '9')
		return (ft_atoi(col));
	return (COLOR_WHITE);
}

t_cell		*parse_cell(char *str, t_fdf *fdf)
{
	t_cell	*cell;
	int		len;
	int		i;

	len = 0;
	i = 0;
	cell = (t_cell*)malloc(sizeof(cell));
	if ((str[i] < '0' || str[i] > '9') && str[i] != '-' && str[i] != '+')
	{
		fdf->error = 1;
		fdf->message = "ERROR! Wrong height of point";
	}
	cell->z = ft_atoi(str);
	while ((str[i] >= '0' && str[i] <= '9') || str[i] == ',')
		i++;
	cell->color = parse_color(&str[i]);
	return (cell);
}
