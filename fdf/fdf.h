/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 17:58:53 by agalayko          #+#    #+#             */
/*   Updated: 2018/06/20 17:58:56 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# define COLOR_WHITE 0xFFFFFF
# include "libft/libft.h"
# include <mlx.h>
# include <math.h>

typedef struct	s_fdf
{
	int			width;
	int			height;
	double		rx;
	double		ry;
	double		rz;
	int			error;
	int			sx;
	int			sy;
	int			win_w;
	int			win_h;
	float		scale;
	float		conts;
	char		*message;
	void		*mlx;
	void		*win;
}				t_fdf;

typedef struct	s_cell
{
	int			x;
	int			y;
	int			z;
	int			color;
}				t_cell;

typedef struct	s_draw
{
	t_fdf		*fdf;
	t_cell		***cell;
}				t_draw;

void			first_read(char *file, t_fdf *fdf);
void			read_file(char *file, t_fdf *fdf, t_cell ***cell);
t_cell			*parse_cell(char *str, t_fdf *fdf);
int				key_handler(int key, void *param);
void			window(t_cell ***cell, t_fdf *fdf);
void			free_cell_arr(t_cell ***cell, t_fdf *fdf);
void			draw_grid(t_cell ***cell, t_fdf *fdf);
void			change_color(t_cell ***cell, t_fdf *fdf, int k);
void			peak_color(t_cell ***cell, t_fdf *fdf);
void			cpc(t_cell ***cell, t_fdf *fdf, int k);

#endif
