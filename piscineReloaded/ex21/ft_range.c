/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/19 18:53:01 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/19 18:53:04 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int		*a;
	int		i;
	int		j;

	if (min >= max)
		return (0);
	i = 0;
	j = min;
	while (j++ < max)
		i++;
	a = (int*)malloc(sizeof(*a) * i);
	j = 0;
	while (j < i)
		a[j++] = min++;
	return (a);
}
