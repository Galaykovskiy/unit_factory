/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:35:23 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:35:27 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(const char *haystack, const char *needle)
{
	int		i;
	int		j;
	char	*a;

	i = 0;
	a = (char*)haystack;
	if (*needle == '\0')
		return (a);
	while (a[i] != '\0')
	{
		j = 0;
		while (needle[j] != '\0')
		{
			if (a[i + j] != needle[j])
				break ;
			j++;
		}
		if (needle[j] == '\0')
			return (a + i);
		i++;
	}
	return (0);
}
