/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:24:36 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:24:40 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char	*a;
	char	*b;

	a = (char*)dst;
	b = (char*)src;
	if (dst < src)
		ft_memcpy(dst, src, len);
	else
		while ((int)--len >= 0)
			a[len] = b[len];
	return (dst);
}
