/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 20:41:35 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/25 20:41:37 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdelone(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*list;

	if (!alst || !(*alst) || !del)
		*alst = NULL;
	list = *alst;
	if (list->content)
		del(list->content, list->content_size);
	free(list);
	*alst = NULL;
}
