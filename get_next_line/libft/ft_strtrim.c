/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:35:58 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:36:01 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strtrim(char const *s)
{
	int		a;
	int		b;
	int		i;
	char	*t;

	if (!s)
		return (NULL);
	a = 0;
	b = 0;
	i = 0;
	while ((s[a] == ' ' || s[a] == '\n' || s[a] == '\t') && s[a] != '\0')
		a++;
	if (s[a] == '\0')
		return (ft_strdup(""));
	while (s[b] != '\0')
		b++;
	while ((s[b] == ' ' || s[b] == '\n' || s[b] == '\t' || !s[b]) && b > a)
		b--;
	t = (char*)malloc(sizeof(*t) * (b - a + 2));
	if (!t)
		return (NULL);
	while (a <= b)
		t[i++] = s[a++];
	t[i] = '\0';
	return (t);
}
