/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/28 18:50:24 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/28 18:50:27 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_free_mas(char **r)
{
	int		i;

	i = 0;
	if (!(*r) || !(r))
		return ;
	while (r[i])
		free(r[i++]);
	free(r);
}
