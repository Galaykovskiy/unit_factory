#include "get_next_line.h"
#include <fcntl.h>
#include <stdio.h>

// int  main(void)
// {
// 	char  *line = NULL;
// 	int   fd = open("./gnl10.txt", O_RDONLY);
// 	get_next_line(fd, &line);
// 	free(line);
// 	close(fd);
// 	system("leaks a.out");
// 	return (0);
// }

int		main(int argc, char **argv)
{
	int		fd;
	char	*line;

	if (argc == 1)
		fd = 0;
	else if (argc == 2)
		fd = open(argv[1], O_RDONLY);
	else
		return (2);
		// int i = 0;
	while (get_next_line(fd, &line) == 1)
	{
		//ft_putendl(line);
		// if (!ft_strcmp(buf, "asd\n1234\n"))
		// if (i)
		// 	system("leaks a.out");
		free(line);
		// i++;
	}
	if (argc == 2)
		close(fd);
	system("leaks a.out");
}

// int	main(void)
// {
// 	char *line;
// 	int fd;
// 	int fd2;
// 	int fd3;
// 	int	diff_file_size;

//     system("mkdir -p sandbox");
// 	system("openssl rand -base64 $((30 * 1000 * 3/4)) | tr -d '\n' | tr -d '\r' > sandbox/one_big_fat_line.txt");
// 	system("echo '\n' >> sandbox/one_big_fat_line.txt");

// 	fd = open("sandbox/one_big_fat_line.txt", O_RDONLY);
// 	fd2 = open("sandbox/one_big_fat_line.txt.mine", O_CREAT | O_RDWR | O_TRUNC, 0755);

// 	while (get_next_line(fd, &line) == 1)
// 	{
// 	    write(fd2, line, strlen(line));
// 	    write(fd2, "\n", 1);
// 	}
// 	if (line)
// 		write(fd2, line, strlen(line));
// 	close(fd);
// 	close(fd2);

// 	system("diff sandbox/one_big_fat_line.txt sandbox/one_big_fat_line.txt.mine > sandbox/one_big_fat_line.diff");
// 	fd3 = open("sandbox/one_big_fat_line.diff", O_RDONLY);
// 	diff_file_size = read(fd3, NULL, 10);
// 	close(fd3);

// 	if (diff_file_size == 0)
// 		printf("Success");
// 	return (0);
// }

// gnl7_3.txt
// int				main(void)
// {
// 	char		*line;
// 	int			fd;
// 	int			ret;
// 	int			count_lines;
// 	char		*filename;
// 	int			errors;

// 	filename = "gnl7_3.txt";
// 	fd = open(filename, O_RDONLY);
// 	if (fd > 2)
// 	{
// 		count_lines = 0;
// 		errors = 0;
// 		line = NULL;
// 		while ((ret = get_next_line(fd, &line)) > 0)
// 		{
// 			if (count_lines == 0 && strcmp(line, "1234567") != 0)
// 				errors++;
// 			if (count_lines == 1 && strcmp(line, "abcdefg") != 0)
// 				errors++;
// 			if (count_lines == 2 && strcmp(line, "4567890") != 0)
// 				errors++;
// 			if (count_lines == 3 && strcmp(line, "defghijk") != 0)
// 				errors++;
// 			count_lines++;
// 			if (count_lines > 50)
// 				break ;
// 		}
// 		close(fd);
// 		if (count_lines != 4)
// 			printf("-> should have returned '1' four times instead of %d time(s)\n", count_lines);
// 		if (errors > 0)
// 			printf("-> should have read \"1234567\", \"abcdefg\", \"4567890\" and \"defghijk\"\n");
// 		if (count_lines == 4 && errors == 0)
// 			printf("OK\n");
// 	}
// 	else
// 		printf("An error occured while opening file %s\n", filename);
// 	return (0);
// }

// // gnl1_1.txt
// int				main(void)
// {
// 	char		*line;
// 	int			fd;
// 	int			ret;
// 	int			count_lines;
// 	char		*filename;
// 	int			errors;

// 	filename = "gnl1_1.txt";
// 	fd = open(filename, O_RDONLY);
// 	if (fd > 2)
// 	{
// 		count_lines = 0;
// 		errors = 0;
// 		line = NULL;
// 		while ((ret = get_next_line(fd, &line)) > 0)
// 		{
// 			if (count_lines == 0 && strcmp(line, "1234567") != 0)
// 				errors++;
// 			count_lines++;
// 			if (count_lines > 50)
// 				break;
// 		}
// 		close(fd);
// 		if (count_lines != 1)
// 			printf("-> must have returned '1' once instead of %d time(s)\n", count_lines);
// 		if (errors > 0)
// 			printf("-> must have read \"1234567\" instead of \"%s\"\n", line);
// 		if (count_lines == 1 && errors == 0)
// 			printf("OK\n");
// 	}
// 	else
// 		printf("An error occured while opening file %s\n", filename);
// 	return (0);
// }

// gnl3_3.txt

// int				main(void)
// {
// 	char		*line;
// 	int			fd;
// 	int			ret;
// 	int			count_lines;
// 	char		*filename;
// 	int			errors;

// 	filename = "gnl3_3.txt";
// 	fd = open(filename, O_RDONLY);
// 	if (fd > 2)
// 	{
// 		count_lines = 0;
// 		errors = 0;
// 		line = NULL;
// 		while ((ret = get_next_line(fd, &line)) > 0)
// 		{
// 			if (count_lines == 0 && strcmp(line, "1234567890abcde") != 0)
// 				errors++;
// 			if (count_lines == 1 && strcmp(line, "fghijklmnopqrst") != 0)
// 				errors++;
// 			if (count_lines == 2 && strcmp(line, "edcba0987654321") != 0)
// 				errors++;
// 			if (count_lines == 3 && strcmp(line, "tsrqponmlkjihgf") != 0)
// 				errors++;
// 			count_lines++;
// 			if (count_lines > 50)
// 				break ;
// 		}
// 		close(fd);
// 		if (count_lines != 4)
// 			printf("-> must have returned '1' four times instead of %d time(s)\n", count_lines);
// 		if (errors > 0)
// 			printf("-> must have read \"1234567890abcde\", \"fghijklmnopqrst\", \"edcba0987654321\" and \"tsrqponmlkjihgf\"\n");
// 		if (count_lines == 4 && errors == 0)
// 			printf("OK\n");
// 	}
// 	else
// 		printf("An error occured while opening file %s\n", filename);
// 	return (0);
// }
