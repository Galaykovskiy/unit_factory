#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abulakh <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/09/06 12:22:36 by abulakh           #+#    #+#              #
#    Updated: 2018/09/06 12:22:37 by abulakh          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME		=	asm

CC			=	gcc
FLAGS		=	-Wall -Wextra -Werror -O2

DELTA		=	$$(echo "$$(tput cols)-47"|bc)

LIBFT_DIR	=	libft/
LIBFT_LIB	=	$(LIBFT_DIR)libft.a
LIBFT_INC	=	$(LIBFT_DIR)includes/

LIBS		=	-ltermcap

SRC_DIR		=	src/
INC_DIR		=	includes/
OBJ_DIR		=	objects/

SRC_BASE = \
	asm_src/asm_parser.c            \
	asm_src/lexer/lexer.c           \
	asm_src/lexer/tools_lexer_1.c   \
	asm_src/lexer/tools_lexer_2.c   \
	asm_src/lexer/get_line.c        \
	asm_src/lexer/get_token.c       \
	asm_src/lexer/create_token.c    \
	asm_src/lexer/types.c           \
	asm_src/syntax/syntax.c         \
	asm_src/syntax/operation_1.c    \
	asm_src/syntax/operation_2.c    \
	asm_src/syntax/tools_syntax.c   \
	asm_src/codegen/codegen.c       \
	asm_src/codegen/code_get.c      \
	asm_src/codegen/create.c        \
	asm_src/codegen/tools.c         \
	asm_src/itoa_16.c               \
	asm_src/utils.c                 \
	op.c

SRCS		=	$(addprefix $(SRC_DIR), $(SRC_BASE))
OBJS		=	$(addprefix $(OBJ_DIR), $(SRC_BASE:.c=.o))
NB			=	$(words $(SRC_BASE))
INDEX		=	0

all :
	@make -C $(LIBFT_DIR)
	@make -j $(NAME)

$(NAME):		$(LIBFT_LIB) $(OBJ_DIR) $(OBJS)
	@$(CC) $(OBJS) -o $(NAME) \
		-I $(INC_DIR) \
		-I $(LIBFT_INC) \
		$(LIBS) \
		$(LIBFT_LIB) \
		$(FLAGS)
	@strip -x $@
	@printf "\r\033[38;5;112mDONE! %9.10s : %2d%% \033[48;5;112m%*s\033[0m \033[38;5;112m   %*.*s\033[0m\033[K\n" $(NAME) 100 20 "" $(DELTA) $(DELTA) "✔"

$(LIBFT_LIB):
	@make -C $(LIBFT_DIR)

$(OBJ_DIR) :
	@mkdir -p $(OBJ_DIR)
	@mkdir -p $(dir $(OBJS))

$(OBJ_DIR)%.o :	$(SRC_DIR)%.c | $(OBJ_DIR)
	@$(eval DONE=$(shell echo $$(($(INDEX)*20/$(NB)))))
	@$(eval PERCENT=$(shell echo $$(($(INDEX)*100/$(NB)))))
	@$(eval COLOR=$(shell echo $$(($(PERCENT)%35+196))))
	@$(eval TO_DO=$(shell echo $$((20-$(INDEX)*20/$(NB)))))
	@printf "\r\033[38;5;11mMAKE %10.10s : %2d%%  \033[48;5;%dm%*s\033[0m%*s\033[48;5;255m \033[0m \033[38;5;11m %*.*s\033[0m\033[K" $(NAME) $(PERCENT) $(COLOR) $(DONE) "" $(TO_DO) "" $(DELTA) $(DELTA) "$@"
	@$(CC) $(FLAGS) -MMD -c $< -o $@\
		-I $(INC_DIR)\
		-I $(LIBFT_INC)
	@$(eval INDEX=$(shell echo $$(($(INDEX)+1))))

clean:			cleanlib
	@rm -rf $(OBJ_DIR)
	@printf "\r\033[38;5;202m✖ Deleting $(NAME)'s objects\033[0m\033[K\n"

cleanlib:
	@make -C $(LIBFT_DIR) clean

fclean:			clean fcleanlib
	@rm -f $(NAME)
	@printf "\r\033[38;5;196m❌ Deleting $(NAME)'s objects\033[0m\033[K\n"

fcleanlib:		cleanlib
	@make -C $(LIBFT_DIR) fclean

re:				fclean all

relib:			fcleanlib $(LIBFT_LIB)

.PHONY :		fclean clean re relib cleanlib fcleanlib

-include $(OBJS:.o=.d)

