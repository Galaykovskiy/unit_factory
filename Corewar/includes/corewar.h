/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/27 15:10:23 by agalayko          #+#    #+#             */
/*   Updated: 2019/03/27 15:10:29 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COREWAR_H
# define COREWAR_H
# include "libft.h"
# include "op.h"

int is_number(char *name);
char *join_with_free(char *str1, char *str2);

typedef struct		s_carr
{
	int				id;
	int				carry;
	int				color;
	char			*cur_op;
	int				cyc_last_live;
	int				cyc_wait;
	int				pos;
	int				next_pos;
	char			regs[REG_NUMBER][REG_SIZE * 2];
	struct		s_carr *next;
}					t_carr;

typedef struct		s_player
{
	int				id;
	char			*name;
	char			*comment;
	int				size;
	char			*code;
	struct s_player	*next;
}					t_player;

typedef struct		s_core
{
	char			*arena;
	int				num_pl;
	int				dump;
	int				dump_numbr;
	int				n[MAX_PLAYERS];
	int				last_say_live;
	int				n_cycles;
	int				num_lives;
	int				cycles_to_die;
	int				n_check;
	t_op			op_tab[17];
	t_player		*players;
	t_carr			*carrs;
}					t_core;

void get_functions_core(t_core *core);




#endif
