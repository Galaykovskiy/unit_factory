/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abulakh <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 13:58:02 by abulakh           #+#    #+#             */
/*   Updated: 2017/10/26 13:58:03 by abulakh          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char	*a;
	char	*b;
	size_t	i;

	b = (char *)s1;
	i = 0;
	a = malloc(sizeof(char) * (ft_strlen(b) + 1));
	if (!a)
		return (NULL);
	a[ft_strlen(b)] = '\0';
	while (b[i])
	{
		a[i] = b[i];
		i++;
	}
	return (a);
}
