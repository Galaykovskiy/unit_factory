/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abulakh <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 20:43:26 by abulakh           #+#    #+#             */
/*   Updated: 2017/10/25 20:43:27 by abulakh          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# define BUFF_SIZE 42
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <stdarg.h>
# include <wchar.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

typedef struct	s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}				t_list;

typedef struct	s_print
{
	int				first[5];
	int				wth[3];
	int				hhhllljz[6];
}				t_print;

int				g_p;
int				get_next_line(const int fd, char **line);
void			*ft_memset(void *tab, int c, size_t b);
void			ft_bzero(void *s, size_t n);
void			*ft_memcpy(void *dst, const void *src, size_t n);
void			*ft_memccpy(void *dst, const void *src, int c, size_t n);
void			*ft_memmove(void *dst, const void *src, size_t len);
void			*ft_memchr(const void *s, int c, size_t n);
int				ft_memcmp(const void *s1, const void *s2, size_t n);
size_t			ft_strlen(const char *s);
char			*ft_strdup(const char *s1);
char			*ft_strcpy(char *dst, const char *src);
char			*ft_strncpy(char *dst, const char *src, size_t len);
char			*ft_strcat(char *s1, const char *s2);
char			*ft_strncat(char *s1, const char *s2, size_t n);
size_t			ft_strlcat(char *dst, const char *src, size_t n);
char			*ft_strchr(const char *s, int c);
char			*ft_strrchr(const char *s, int c);
char			*ft_strstr(const char *haystack, const char *needle);
char			*ft_strnstr(const char *hs, const char *nl, size_t len);
int				ft_strcmp(const char *s1, const char *s2);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
int				ft_atoi(const char *str);
int				ft_isalpha(int c);
int				ft_isdigit(int c);
int				ft_isalnum(int c);
int				ft_isascii(int c);
int				ft_isprint(int c);
int				ft_toupper(int c);
int				ft_tolower(int c);
void			*ft_memalloc(size_t size);
void			ft_memdel(void **ap);
char			*ft_strnew(size_t size);
void			ft_strdel(char **as);
void			ft_strclr(char *s);
void			ft_striter(char *s, void (*f)(char *));
void			ft_striteri(char *s, void (*f)(unsigned int, char *));
char			*ft_strmap(char const *s, char (*f)(char));
char			*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int				ft_strequ(char const *s1, char const *s2);
int				ft_strnequ(char const *s1, char const *s2, size_t n);
char			*ft_strsub(char const *s, unsigned int start, size_t len);
char			*ft_strjoin(char const *s1, char const *s2);
char			*ft_strtrim(char const *s);
char			**ft_strsplit(char const *s, char c);
char			*ft_itoa(int n);
void			ft_putchar(char c);
void			ft_putstr(char const *s);
void			ft_putendl(char const *s);
int				ft_putnbr(intmax_t n);
void			ft_putchar_fd(char c, int fd);
void			ft_putstr_fd(char const *s, int fd);
void			ft_putendl_fd(char const *s, int fd);
void			ft_putnbr_fd(int n, int fd);
t_list			*ft_lstnew(void const *content, size_t content_size);
void			ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void			ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void			ft_lstadd(t_list **alst, t_list *new);
void			ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list			*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
void			ft_guz(int n);
void			ft_sobaka(int n);
void			ft_free_split(char **k);
long			ft_sqrt(long n);
long			ft_pow(long n, size_t i);
int				ft_listsize(t_list *klist);
int				*ft_strfind(char **k, char chr);
char			*ft_itoa_base(uintmax_t num, int base, int xx);
int				ft_strlenwc2(wchar_t *a, t_print stp);
char			*ft_itoa_base(uintmax_t num, int base, int xx);
void			ft_type_field(t_print stp, va_list ap, char **format);
void			ft_length_mod(t_print stp, va_list ap, char **format);
void			ft_wth_func(t_print stp, va_list ap, char **format);
void			ft_start_va(char **format, va_list ap, t_print stp);
int				ft_printf(const char *format, ...);
void			ft_put_str(char **str);
void			ft_baby_zero(t_print *stp);
int				ft_modul(int nbr, t_print *stp, char **format);
void			ft_int_plus(int *m, char c, int flag);
void			ft_int_minus(int *m, char c, int flag);
void			ft_put_di_m(int m, intmax_t val, t_print stp);
void			ft_put_di_wd2(int m, intmax_t val, t_print stp);
void			ft_put_di_wd(int m, intmax_t val, t_print stp);
void			ft_put_di2(intmax_t val, t_print stp);
void			ft_put_di(t_print stp, va_list ap, char **format);
void			ft_put_dubl(t_print stp, va_list ap, char **format);
void			ft_put_obl(t_print stp, va_list ap, char **format);
void			ft_put_ouxxbl_m(int m, char *a, t_print stp, int base);
void			ft_put_ouxxb2_wd3(int base, t_print *stp, char *a, int *m);
void			ft_put_ouxxbl_wd2(int m, char *a, t_print stp, int base);
void			ft_put_ouxxbl_wd(int m, char *a, t_print stp, int base);
void			ft_put_ouxxbl2(uintmax_t val, t_print stp, int base);
void			ft_put_ouxxbl(t_print stp, va_list ap, char **format);
void			ft_put_p_m(int m, char *a, t_print stp, int base);
void			ft_put_p_wd2(int m, char *a, t_print stp, int base);
void			ft_put_p_wd(int m, char *a, t_print stp, int base);
void			ft_put_p2(uintmax_t val, t_print stp, int base);
void			ft_put_p(t_print stp, va_list ap, char **format);
void			ft_put_s2ch(char *a, t_print stp);
void			ft_put_s2ch2(char *a, t_print stp, char ag, int m);
void			ft_put_s2ch3(char *a, t_print stp, char ag, int m);
void			ft_put_s2wc2(wchar_t *a, t_print stp, char ag, int m);
void			ft_put_s2wc3(wchar_t *a, t_print stp, char ag, int m);
void			ft_put_s2wc(wchar_t *a, t_print stp);
void			ft_put_s(t_print stp, va_list ap, char **format);
void			ft_put_cbl(t_print stp, va_list ap, char **format);
void			ft_put_c2wc(wchar_t a, t_print stp);
void			ft_put_c2ch(char a, t_print stp);
void			ft_else(t_print stp, char **format);
void			ft_put_c(t_print stp, va_list ap, char **format);
void			ft_putwchar_fd(wchar_t chr, int fd);
void			ft_put_sbl(t_print stp, va_list ap, char **format);
void			ft_put_chtoto(t_print stp, char **format);
int				ft_lenwchar(wchar_t a);
int				ft_strlenwc(wchar_t *a);
void			ft_putstrwchar(wchar_t *a);

#endif
