/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_checker.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psaprono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/01 13:18:28 by psaprono          #+#    #+#             */
/*   Updated: 2018/04/01 13:18:32 by psaprono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "fillit.h"

int	ft_block_check(char *str, int len)
{
	if (!ft_strlen(str) || !ft_corr_col_chars(str) || !ft_corr_block(str)
		|| len < 20)
	{
		ft_putstr("error\n");
		return (0);
	}
	return (1);
}

int	ft_block_count(t_list *list)
{
	if (ft_lstsize(list) > 25)
	{
		ft_putendl("error");
		return (0);
	}
	return (1);
}

int	ft_corr_col_chars(char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (str[i] != '#' && str[i] != '.' && str[i] != '\n')
			return (0);
		i++;
	}
	i = 4;
	while (i < 21)
	{
		if (str[i] != '\n')
			return (0);
		i += 5;
	}
	return (1);
}

int	ft_corr_block(char *str)
{
	int		i;
	int		hash;
	int		dots;
	int		neigb;

	hash = 0;
	i = 0;
	dots = 0;
	neigb = 0;
	while (str[i])
	{
		if (str[i] == '#')
		{
			hash++;
			neigb += ft_neighbor_hash(str, i);
		}
		if (str[i] == '.')
			dots++;
		i++;
	}
	if (hash != 4 || dots != 12 || neigb < 6)
		return (0);
	return (1);
}

int	ft_neighbor_hash(char *str, int position)
{
	int		n;

	n = 0;
	if (str[position - 1] == '#')
		n++;
	if (str[position + 1] == '#')
		n++;
	if (str[position - 5] == '#')
		n++;
	if (str[position + 5] == '#')
		n++;
	return (n);
}
