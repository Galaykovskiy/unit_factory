/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 21:38:28 by agalayko          #+#    #+#             */
/*   Updated: 2018/04/05 21:38:31 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "fillit.h"

void	ft_lstaddend(t_list **lst, char *str)
{
	t_list	*curr;
	char	*strcp;

	strcp = ft_strnew(20);
	strcp = ft_strncpy(strcp, str, 20);
	if (*lst == NULL)
		*lst = ft_lstnew(str, ft_strlen(strcp));
	else
	{
		curr = *lst;
		while (curr->next)
			curr = curr->next;
		curr->next = ft_lstnew(str, ft_strlen(strcp));
	}
}

t_list	*free_list(t_list *list)
{
	t_list	*next;

	while (list)
	{
		next = list->next;
		ft_strdel((char **)&(list->content));
		ft_memdel((void **)&list);
		list = next;
	}
	return (NULL);
}

int		ft_lstsize(t_list *lst)
{
	t_list	*curr;
	int		size;

	curr = lst;
	size = 0;
	while (curr)
	{
		size++;
		curr = curr->next;
	}
	return (size);
}
