/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psaprono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 10:25:09 by psaprono          #+#    #+#             */
/*   Updated: 2018/04/06 12:19:21 by psaprono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# include <fcntl.h>

int		ft_reader(int fd);
int		ft_block_check(char *str, int len);
int		ft_block_count(t_list *list);
int		ft_corr_col_chars(char *str);
int		ft_corr_block(char *str);
int		ft_neighbor_hash(char *str, int position);
t_list	*free_list(t_list *list);
int		ft_lstsize(t_list *lst);
void	ft_lstaddend(t_list **lst, char *str);
int		ft_min_height(char *str);
int		ft_min_weight(char *str);
void	ft_shift(char *str, char c);
int		ft_solve(t_list *lst);
char	*ft_create_field(int size);
int		ft_islocate(char *block, char *map, int size, int p);
void	ft_set(char *block, char *map, int size, int p);
void	ft_del(char *block, char *map, int size, int p);
int		ft_func(char *map, t_list *lst, int size);
int		ft_check_count(int len);

#endif
