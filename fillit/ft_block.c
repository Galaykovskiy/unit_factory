/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_block.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 21:38:12 by agalayko          #+#    #+#             */
/*   Updated: 2018/04/06 17:32:51 by psaprono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "fillit.h"

void	ft_shift(char *str, char c)
{
	int		min_height;
	int		min_weight;
	int		i;

	min_height = ft_min_height(str);
	min_weight = ft_min_weight(str);
	i = 0;
	while (str[i])
	{
		if (str[i] == '#')
		{
			str[i] = '.';
			str[i - (min_weight + 5 * min_height)] = c;
		}
		i++;
	}
}

int		ft_min_weight(char *str)
{
	int		i;
	int		min;

	i = 0;
	while (str[i] != '#')
		i++;
	min = i % 5;
	while (i < 20)
	{
		if (str[i] == '#')
			if (min > i % 5)
				min = i % 5;
		i++;
	}
	return (min);
}

int		ft_min_height(char *str)
{
	int		i;
	int		min;

	i = 0;
	while (str[i] != '#')
		i++;
	min = i / 5;
	while (i < 20)
	{
		if (str[i] == '#')
			if (min > i / 5)
				min = i / 5;
		i++;
	}
	return (min);
}
