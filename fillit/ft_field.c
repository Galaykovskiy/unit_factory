/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_field.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 21:37:58 by agalayko          #+#    #+#             */
/*   Updated: 2018/04/05 21:38:02 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "fillit.h"

int		ft_func(char *map, t_list *lst, int size)
{
	int		i;

	if (!lst)
		return (1);
	i = 0;
	while (i < size * size + size)
	{
		if (ft_islocate((char *)(lst->content), map, size, i))
		{
			ft_set((char *)(lst->content), map, size, i);
			if (ft_func(map, lst->next, size))
				return (1);
			else
				ft_del((char *)(lst->content), map, size, i);
		}
		i++;
	}
	return (0);
}

int		ft_islocate(char *block, char *map, int size, int p)
{
	int		i;

	i = 0;
	while (block[i])
	{
		if (block[i] != '.' && block[i] != '\n')
			if (map[i / 5 * size + i % 5 + p + i / 5] != '.')
				return (0);
		i++;
	}
	return (1);
}

void	ft_set(char *block, char *map, int size, int p)
{
	int		i;

	i = 0;
	while (block[i])
	{
		if (block[i] != '.' && block[i] != '\n')
			map[i / 5 * size + i % 5 + p + i / 5] = block[i];
		i++;
	}
}

void	ft_del(char *block, char *map, int size, int p)
{
	int		i;

	i = 0;
	while (block[i])
	{
		if (block[i] != '.' && block[i] != '\n')
			map[i / 5 * size + i % 5 + p + i / 5] = '.';
		i++;
	}
}

char	*ft_create_field(int size)
{
	char	*str;
	int		i;

	if ((str = (char *)ft_memalloc(size * size + size)) == NULL)
		return (NULL);
	ft_memset(str, '.', size * size + size);
	i = size;
	while (str[i])
	{
		str[i] = '\n';
		i += size + 1;
	}
	return (str);
}
