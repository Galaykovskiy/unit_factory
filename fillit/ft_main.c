/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_main.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psaprono <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/05 10:33:12 by psaprono          #+#    #+#             */
/*   Updated: 2018/04/06 17:51:12 by psaprono         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "fillit.h"

int	ft_solve(t_list *lst)
{
	t_list	*curr;
	int		size;
	char	*map;
	char	c;

	curr = lst;
	c = 'A';
	while (curr)
	{
		ft_shift((char *)(curr->content), c++);
		curr = curr->next;
	}
	size = 2;
	while (size * size < ft_lstsize(lst) * 4)
		size++;
	while (!(ft_func((map = ft_create_field(size)), lst, size)))
	{
		ft_strdel(&map);
		size++;
	}
	ft_putstr(map);
	ft_strdel(&map);
	return (1);
}

int	ft_reader(int fd)
{
	char	*buf;
	t_list	*lst;
	int		len;
	int		count;

	buf = ft_strnew(21);
	lst = NULL;
	while ((len = read(fd, buf, 21)))
	{
		count = len;
		if (!ft_block_check(buf, len) || !ft_block_count(lst))
		{
			free_list(lst);
			ft_memdel((void **)&buf);
			return (0);
		}
		ft_lstaddend(&lst, buf);
	}
	ft_memdel((void **)&buf);
	if (ft_check_count(count) == 1)
		ft_solve(lst);
	lst = free_list(lst);
	return (1);
}

int	ft_check_count(int len)
{
	if (len != 20)
	{
		ft_putstr("error\n");
		return (0);
	}
	return (1);
}

int	main(int argc, char **argv)
{
	int		fd;

	if (argc != 2)
		ft_putstr("usage: fillit [input_file]\n");
	else
	{
		if ((fd = open(argv[1], O_RDONLY)) == -1)
		{
			ft_putstr("error\n");
			return (1);
		}
		ft_reader(fd);
		if (close(fd) == -1)
		{
			ft_putstr("error\n");
			return (1);
		}
	}
	return (0);
}
