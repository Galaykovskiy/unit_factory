/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit_lst.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 15:16:16 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 15:16:16 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static t_list	*new_elem(const char *s, char c, t_list *lst)
{
	t_list		*new;
	size_t		wordlen;
	static int	first = 1;

	wordlen = ft_countseg(s, c);
	if (!(new = ft_lstnew(NULL, 0)))
		return (NULL);
	if (!(new->content = ft_strsub(s, 0, wordlen)))
		return (NULL);
	new->content_size = wordlen + 1;
	if (first)
	{
		lst = new;
		first = 0;
	}
	else
		ft_lstappend(lst, new);
	return (lst);
}

t_list			*ft_strsplit_lst(char const *s, char c)
{
	t_list		*lst;
	int			flag;

	flag = 0;
	if (!s)
		return (NULL);
	if (!(ft_wordcount(s, c)))
		return (NULL);
	while (*s)
	{
		if (*s != c && !flag)
		{
			flag = 1;
			if (!(lst = new_elem(s, c, lst)))
				return (NULL);
		}
		else if (*s == c && flag)
			flag = 0;
		++s;
	}
	return (lst);
}
