/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 15:16:08 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 15:16:09 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrev(char *s)
{
	int		len;
	int		i;
	char	temp;

	i = 0;
	if (!s || !*s)
		return (NULL);
	len = (int)ft_strlen(s);
	while (i < len - 1)
	{
		temp = s[i];
		s[i] = s[len - 1];
		s[len - 1] = temp;
		++i;
		--len;
	}
	return (s);
}
