/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ptoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 15:15:59 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 15:16:00 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	places(unsigned long long n)
{
	int count;

	count = 0;
	if (n == 0)
		return (1);
	while (n != 0)
	{
		++count;
		n /= 16;
	}
	return (count);
}

char		*ft_ptoa(void *p, char c)
{
	char				*s;
	unsigned long long	n;
	int					i;

	n = (unsigned long long)p;
	i = places(n);
	if (!(s = (char*)ft_memalloc(i + 1)))
		return (NULL);
	if (p == NULL)
		s[0] = '0';
	while (n)
	{
		s[--i] = (n % 16 > 9) ? (n % 16 - 10 + c) : (n % 16 + '0');
		n /= 16;
	}
	return (s);
}
