/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_newstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/05 20:56:02 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/05 20:56:02 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_newstr(const char *text)
{
	char	*ret;

	ret = ft_strnew(ft_strlen(text));
	ret = ft_strcpy(ret, text);
	return (ret);
}
