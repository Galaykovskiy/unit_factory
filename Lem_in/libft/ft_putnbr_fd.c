/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:29:19 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:29:23 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	long_putter(long l, int fd)
{
	if (l < 10)
		ft_putchar_fd(l + '0', fd);
	else
	{
		long_putter(l / 10, fd);
		long_putter(l % 10, fd);
	}
}

void		ft_putnbr_fd(int n, int fd)
{
	long	l;

	l = n;
	if (l < 0)
	{
		ft_putchar_fd('-', fd);
		l = -l;
	}
	long_putter(l, fd);
}
