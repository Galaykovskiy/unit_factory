
#ifndef LEM_IN_H

# define LEM_IN_H
# define LEM_IN_USAGE			"Usage: ./lem-in < MAP_FILE"
# define ER_READING				"ERROR! Reading error"
# define ER_LINE_INIT			"ERROR! Can\'t initialize line"
# define ER_LEM_IN_INIT			"ERROR! Can\'t initialize Lem-in"
# define ER_ANTS_NBR_PARSE		"ERROR! Number of ants is incorrent"
# define ER_RM_INIT				"ERROR! Can\'t initialize room"
# define ER_RM_PARSING			"ERROR! Can\'t parse room"
# define ER_RM_NAME_DUP			"ERROR! Input has room name duplicate"
# define ER_RM_COORDS_DUP		"ERROR! Input has room coordinates duplicate"
# define ER_START_END_RM		"ERROR! Input has no start or end room"
# define ER_LINK_INIT			"ERROR! Can\'t initialize link"
# define ER_LINK_PARSING		"ERROR! Can\'t parse link"
# define ER_LINK_DUP			"ERROR! Input has link duplicate"
# define ER_NO_LINKS			"ERROR! Input has no links"
# define ER_LOC_INIT			"ERROR! Can\'t initialize location"
# define ER_LOC_PARSING			"ERROR! Location parsing error"
# define ER_TURN_INIT			"ERROR! Can\'t initialize turn"
# define ER_NO_TURNS			"ERROR! Input has no turns"
# define ER_QUEUE_INIT			"ERROR! Can\'t initialize queue"
# define ER_PATH_INIT			"ERROR! Can\'t initialize path"
# define ER_NO_PATH				"ERROR! Input has no path from start to end"

# include "../libft/libft.h"
# include <stdio.h>
# include <errno.h>

typedef struct			s_l
{
	char				*cont;
	struct s_l			*next;
}						t_l;

typedef enum
{
	START,
	MIDDLE,
	END
}	t_type;

typedef struct			s_rm
{
	char				*name;
	int					x;
	int					y;
	t_type				type;
	int					bfs_lvl;
	int					ip_ln;
	int					op_lk;
	int					ant_nbr;
	struct s_rm			*next;
}						t_rm;

typedef struct			s_lk
{
	t_rm				*start;
	t_rm				*end;
	struct s_lk		*next;
	struct s_lk		*prev;
}						t_lk;

typedef struct			s_path
{
	t_lk				*head;
	int					len;
	struct s_path		*next;
}						t_path;

typedef struct			s_loc
{
	int					ant_number;
	t_rm				*rm;
	struct s_loc		*next;
}						t_loc;

typedef struct			s_turn
{
	t_loc				*loc;
	struct s_turn		*next;
}						t_turn;

typedef struct			s_ant
{
	int					i;
	t_rm				*st;
	t_rm				*end;
	struct s_ant		*next;
}						t_ant;

typedef struct			s_lem
{
	int					ants_st;
	int					ants_e;
	int					ant_nbr;
	int 				rm_num;
	int 				lk_num;
	t_rm				*rms;
	t_rm				*st;
	t_rm				*end;
	t_lk				*lk;
	int					bfs_lvl;
	t_path				*paths;
	t_loc				*loc;
	t_turn				*turns;
	t_ant				*ants;
}						t_lem;

typedef struct			s_q
{
	t_rm				*rm;
	struct s_q			*next;
}						t_q;

t_l						*read_next_line(t_l **l);
t_lem					*parse(t_l **l, t_bool vs);
void					parse_ants(t_lem *lem, t_l **cur, t_l **l);
void					parse_rooms(t_lem *lem, t_l **cur, t_l **l);
void					parse_links(t_lem *lem, t_l **cur, t_l **l, t_bool vs);
t_loc					*parse_loc(t_lem *lem, char *str);
void					parse_turns(t_lem *lem, t_l **cur, t_l **l);
void					validate_room(t_lem *lem, t_rm *rm);
void					validate_link(t_lem *lem, t_lk *link);
t_bool					ft_is_command(char *str);
t_bool					ft_is_comment(char *str);
t_bool					ft_is_rm(char *str);
t_bool					ft_is_loc(t_lem *lem, char *str);
void					bfs(t_lem *lem);
void					align_lk(t_lem *lem);
void					count_io_lk(t_lem *lem);
t_rm					*find_rm(t_lem *lem, char *name);
t_lk					*find_lk(t_lem *lem, t_rm *st, t_rm *e);
void					del_useless_lk(t_lem *lem);
void					del_dead_ends(t_lem *lem);
void					del_lk(t_lem *lem, t_lk *lk);
t_lk					*rem_lk(t_lem *lem, t_lk *lk);
void					del_in_forks(t_lem *lem);
void					del_out_forks(t_lem *lem);
void					form_paths(t_lem *lem);
t_loc					*turn_create_location(int ant_nbr, t_rm *rm);
void					turn_add_location(t_lem *lem, t_loc *loc);
void					perform_turns(t_lem *lem, t_bool ext);
int						calculate_diff(t_lem *lem, t_path *path);
void					l_free(t_l **l);
void					free_lem_in(t_lem **lem);
void					free_ants(t_ant **cur);
void					free_turns(t_turn **cur);
void					free_loc(t_loc **cur);
void					free_paths(t_path **cur);
void					free_lk(t_lk **cur);
void					free_rm(t_rm **cur);
void					terminate(char *s);
void					print_lines(t_l *l);
void					print_paths(t_lem *lem);
void					print_locations(t_lem *lem);

#endif
