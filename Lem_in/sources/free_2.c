
#include "../includes/lem_in.h"

void	free_turns(t_turn **cur)
{
	t_turn	*del;

	if (cur)
		while ((*cur))
		{
			del = (*cur);
			(*cur) = (*cur)->next;
			free_loc(&(del->loc));
			free(del);
		}
}

void	free_loc(t_loc **cur)
{
	t_loc	*delete;

	if (cur)
		while ((*cur))
		{
			delete = (*cur);
			(*cur) = (*cur)->next;
			free(delete);
		}
}

void	free_paths(t_path **cur)
{
	t_path	*del;

	if (cur)
		while ((*cur))
		{
			del = (*cur);
			(*cur) = (*cur)->next;
			free_lk(&(del->head));
			free(del);
		}
}

void	free_lk(t_lk **cur)
{
	t_lk	*del;

	if (cur)
		while ((*cur))
		{
			del = (*cur);
			(*cur) = (*cur)->next;
			free(del);
		}
}

void	free_rm(t_rm **cur)
{
	t_rm	*del;

	if (cur)
		while ((*cur))
		{
			del = (*cur);
			(*cur) = (*cur)->next;
			free(del->name);
			free(del);
		}
}
