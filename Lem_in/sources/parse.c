
#include "../includes/lem_in.h"

static t_loc	*create_location(t_lem *lem_in, char *str)
{
	t_loc		*location;
	char		*dash;
	char		*ant_number;
	char		*room_name;

	if (!(location = (t_loc *)ft_memalloc(sizeof(t_loc))))
		terminate(ER_LOC_INIT);
	dash = ft_strchr(str, '-');
	if (!(ant_number = ft_strsub(str, 1, dash - str - 1)))
		terminate(ER_LOC_INIT);
	if (!(room_name = ft_strsub(dash + 1, 0, ft_strlen(dash + 1))))
		terminate(ER_LOC_INIT);
	location->ant_number = ft_atoi(ant_number);
	location->rm = find_rm(lem_in, room_name);
	location->next = NULL;
	free(ant_number);
	free(room_name);
	return (location);
}

static void			add_location(t_loc **locations, t_loc *location)
{
	t_loc	*current;

	current = (*locations);
	if (current)
	{
		while (current->next)
			current = current->next;
		current->next = location;
	}
	else
		(*locations) = location;
}

t_loc			*parse_loc(t_lem *lem, char *str)
{
	t_loc	*locations;
	size_t		i;
	char		**strsplit;

	i = 0;
	locations = NULL;
	if (!(strsplit = ft_strsplit(str, ' ')))
		terminate(ER_LOC_PARSING);
	while (strsplit[i])
	{
		if (!ft_is_loc(lem, strsplit[i]))
			terminate(ER_LOC_PARSING);
		add_location(&locations, create_location(lem, strsplit[i]));
		i++;
	}
	ft_strsplit_free(&strsplit);
	return (locations);
}

static t_lem	*init_lem_in(void)
{
	t_lem *lem_in;

	if (!(lem_in = (t_lem *)ft_memalloc(sizeof(t_lem))))
		terminate(ER_LEM_IN_INIT);
	lem_in->ants_st = 0;
	lem_in->ants_e = 0;
	lem_in->ant_nbr = 0;
	lem_in->rm_num = 0;
	lem_in->lk_num = 0;
	lem_in->rms = NULL;
	lem_in->st = NULL;
	lem_in->end = NULL;
	lem_in->lk = NULL;
	lem_in->bfs_lvl = 0;
	lem_in->paths = NULL;
	lem_in->loc = NULL;
	lem_in->turns = NULL;
	lem_in->ants = NULL;
	return (lem_in);
}

t_lem		*parse(t_l **l, t_bool vs)
{
	t_lem	*lem_in;
	t_l		*current;

	current = NULL;
	lem_in = init_lem_in();
	parse_ants(lem_in, &current, l);
	parse_rooms(lem_in, &current, l);
	if (!lem_in->st || !lem_in->end)
		terminate(ER_START_END_RM);
	parse_links(lem_in, &current, l, vs);
	if (!lem_in->lk)
		terminate(ER_NO_LINKS);
	if (vs)
	{
		current = NULL;
		parse_turns(lem_in, &current, l);
		if (!lem_in->turns)
			terminate(ER_NO_TURNS);
	}
	return (lem_in);
}
