
#include "../includes/lem_in.h"

static t_l	*create_line(char *content)
{
	t_l *line;

	while (!(line = (t_l *)ft_memalloc(sizeof(t_l))))
		terminate(ER_LINE_INIT);
	line->cont = content;
	line->next = NULL;
	return (line);
}

static void		add_line(t_l **lines, t_l *line)
{
	t_l *current;

	if (lines && *lines)
	{
		current = *lines;
		while (current->next)
			current = current->next;
		current->next = line;
	}
	else if (lines)
		*lines = line;
}

t_l			*read_next_line(t_l **l)
{
	t_l	*line;
	char	*content;
	ssize_t	size;

	line = NULL;
	if ((size = get_next_line(0, &content)) > 0)
		add_line(l, (line = create_line(content)));
	if (size == -1)
		terminate(ER_READING);
	return (line);
}
