
#include "../includes/lem_in.h"

t_bool			ft_is_command(char *str)
{
	if (str)
	{
		if (ft_strlen(str) >= 2
			&& !ft_strncmp(str, "##", 2))
			return (true);
	}
	return (false);
}

t_bool			ft_is_comment(char *str)
{
	if (str)
	{
		if (ft_strlen(str)
			&& !ft_is_command(str)
			&& !ft_strncmp(str, "#", 1))
			return (true);
	}
	return (false);
}

static t_bool	ft_is_rm_name(char *str)
{
	if (str && ft_strlen(str))
	{
		if (str[0] != 'L' && str[0] != '#')
			return (true);
	}
	return (false);
}

t_bool			ft_is_rm(char *str)
{
	t_bool	res;
	char	**mas;

	res = false;
	if (!(mas = ft_strsplit(str, ' ')))
		terminate(ER_RM_PARSING);
	if (ft_strsplit_len(mas) == 3)
	{
		if (ft_is_rm_name(mas[0])
			&& ft_isint(mas[1], true)
			&& ft_isint(mas[2], true))
			res = true;
	}
	ft_strsplit_free(&mas);
	return (res);
}

t_bool			ft_is_loc(t_lem *lem, char *str)
{
	t_bool	res;
	char	*dash;
	char	*ant_nbr;
	char	*rm_name;

	res = false;
	if (ft_strlen(str) < 1 && str[0] != 'L')
		terminate(ER_LOC_PARSING);
	if (!(dash = ft_strchr(str, '-')))
		terminate(ER_LOC_PARSING);
	if (!(ant_nbr = ft_strsub(str, 1, dash - str - 1)))
		terminate(ER_LOC_PARSING);
	if (!(rm_name = ft_strsub(dash + 1, 0, ft_strlen(dash + 1))))
		terminate(ER_LOC_PARSING);
	if (find_rm(lem, rm_name)
		&& ft_isint(ant_nbr, true)
		&& ft_atoi(ant_nbr) >= 1
		&& ft_atoi(ant_nbr) <= lem->ants_st)
		res = true;
	free(ant_nbr);
	free(rm_name);
	return (res);
}
