
#include "../includes/lem_in.h"

void			parse_ants(t_lem *lem, t_l **cur, t_l **l)
{
	if (*cur || ((*cur) = read_next_line(l)))
	{
		if (ft_isint((*cur)->cont, true))
		{
			lem->ants_st = ft_atoi((*cur)->cont);
			(*cur) = NULL;
			if (lem->ants_st <= 0)
				ft_exit(ER_ANTS_NBR_PARSE);
		}
		else
			ft_exit(ER_ANTS_NBR_PARSE);
	}
}

static void		add_link(t_lem *lem, t_lk *lk)
{
	t_lk	*cur;

	lem->lk_num++;
	cur = lem->lk;
	if (cur)
	{
		while (cur->next)
			cur = cur->next;
		cur->next = lk;
	}
	else
		lem->lk = lk;
}

static t_lk	*init_lk(t_rm *st, t_rm *e)
{
	t_lk	*lk;

	if (!(lk = (t_lk *)ft_memalloc(sizeof(t_lk))))
		ft_exit(ER_LINK_INIT);
	lk->start = st;
	lk->end = e;
	lk->next = NULL;
	lk->prev = NULL;
	return (lk);
}

static t_lk	*create_link(t_lem *lem, char *str)
{
	char	*dash;
	char	*st_name;
	char	*e_name;
	t_rm	*st_rm;
	t_rm	*e_rm;

	dash = str;
	while ((dash = ft_strchr(dash + 1, '-')))
	{
		if (!(st_name = ft_strsub(str, 0, dash - str)))
			ft_exit(ER_LINK_INIT);
		if (!(e_name = ft_strsub(dash + 1, 0, ft_strlen(dash + 1))))
			ft_exit(ER_LINK_INIT);
		st_rm = find_rm(lem, st_name);
		e_rm = find_rm(lem, e_name);
		free(st_name);
		free(e_name);
		if (st_rm && e_rm)
			return (init_lk(st_rm, e_rm));
	}
	return (NULL);
}

void			parse_links(t_lem *lem,
					t_l **cur,
					t_l **l,
					t_bool vs)
{
	t_lk	*link;

	while ((*cur) || ((*cur) = read_next_line(l)))
	{
		if (!ft_is_comment((*cur)->cont)
			&& !ft_is_command((*cur)->cont))
		{
			if (vs && !ft_strlen((*cur)->cont))
				break ;
			if (!(link = create_link(lem, (*cur)->cont)))
				ft_exit(ER_LINK_PARSING);
			add_link(lem, link);
			valid_lk(lem, link);
		}
		(*cur) = NULL;
	}
}
