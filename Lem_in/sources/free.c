
#include "../includes/lem_in.h"

void	l_free(t_l **l)
{
	t_l	*delete;
	t_l	*current;

	current = (*l);
	while (current)
	{
		delete = current;
		current = current->next;
		free(delete->cont);
		free(delete);
	}
	(*l) = NULL;
}

void	free_lem_in(t_lem **lem)
{
	if (lem && *lem)
	{
		free_ants(&((*lem)->ants));
		free_turns(&((*lem)->turns));
		free_loc(&((*lem)->loc));
		free_paths(&((*lem)->paths));
		free_lk(&((*lem)->lk));
		free_rm(&((*lem)->rms));
		free((*lem));
		(*lem) = NULL;
	}
}

void	free_ants(t_ant **cur)
{
	t_ant	*del;

	if (cur)
		while ((*cur))
		{
			del = (*cur);
			(*cur) = (*cur)->next;
			free(del);
		}
}
