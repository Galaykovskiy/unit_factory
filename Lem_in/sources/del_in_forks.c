
#include "../includes/lem_in.h"

static t_bool	path_has_out_fork(t_lem *lem, t_lk *lk)
{
	if (lk->start == lem->st)
		return (false);
	if (lk->start->op_lk > 1)
		return (true);
	return (path_has_out_fork(lem, find_lk(lem, NULL, lk->start)));
}

static void		del_other_in_lk(t_lem *lem, t_lk *lk)
{
	t_rm	*end;
	t_lk	*current;
	t_lk	*delete;

	end = lk->end;
	current = lem->lk;
	while (current)
	{
		delete = current;
		current = current->next;
		if (delete->end == end && delete != lk)
			del_lk(lem, delete);
	}
}

static void		del_in_fork(t_lem *lem, t_rm *rm)
{
	t_lk	*cur;
	t_lk	*del;

	cur = lem->lk;
	while (cur && rm->ip_ln > 1)
	{
		del = cur;
		cur = cur->next;
		if (del->end == rm)
		{
			if (!path_has_out_fork(lem, del))
				del_other_in_lk(lem, del);
			else
				del_lk(lem, del);
			del_dead_ends(lem);
		}
	}
}

void			del_in_forks(t_lem *lem)
{
	int		bfs_lvl;
	t_rm	*cur;

	bfs_lvl = 1;
	while (bfs_lvl <= lem->bfs_lvl)
	{
		cur = lem->rms;
		while (cur)
		{
			if (cur->bfs_lvl == bfs_lvl
				&& cur->ip_ln > 1)
				del_in_fork(lem, cur);
			cur = cur->next;
		}
		bfs_lvl++;
	}
}
