
#include "../includes/lem_in.h"

void	lem_in(t_bool ext)
{
	t_l			*lines;
	t_lem		*lem;

	lines = NULL;
	lem = parse(&lines, false);
	bfs(lem);
	if (lem->end->bfs_lvl == -1)
		terminate(ER_NO_PATH);
	del_useless_lk(lem);
	align_lk(lem);
	count_io_lk(lem);
	del_dead_ends(lem);
	del_in_forks(lem);
	del_out_forks(lem);
	print_lines(lines);
	ft_putendl(NULL);
	form_paths(lem);
	if (ext)
	{
		print_paths(lem);
		ft_putendl(NULL);
	}
	perform_turns(lem, ext);
	l_free(&lines);
	free_lem_in(&lem);
}

int		main(int argc, char **argv)
{
	t_bool	ext;

	errno = 0;
	ext = false;
	while (argc >= 2
		&& (!ft_strcmp(argv[1], "-e")))
	{
		if (!ft_strcmp(argv[1], "-e"))
			ext = true;
		argv++;
		argc--;
	}
	if (argc == 1)
		lem_in(ext);
	else
		ft_putendl(LEM_IN_USAGE);
	return (0);
}
