
#include "../includes/lem_in.h"

void	print_lines(t_l *l)
{
	t_l		*current;

	current = l;
	while (current)
	{
		ft_putendl(current->cont);
		current = current->next;
	}
}

static void		print_info(t_lem *l)
{
	ft_putendl("\033[94m---------- Info ----------\n\033[0m");
	ft_putstr("\033[092mNumber of ants: \033[91m");
	ft_putnbr(l->ants_st);
	ft_putstr("\n\033[092mNumber of rooms: \033[91m");
	ft_putnbr(l->rm_num);
	ft_putstr("\n\033[092mNumber of links: \033[91m");
	ft_putnbr(l->lk_num);
	ft_putstr("\n\033[092mStart room: \033[91m");
	ft_putstr(l->st->name);
	ft_putstr("\n\033[092mEnd room: \033[91m");
	ft_putstr(l->end->name);
	ft_putendl("\n");
	ft_putendl("\033[94m---------- Selected Paths ----------\n\033[0m");
}

void	print_paths(t_lem *lem)
{
	t_path	*current_path;
	t_lk	*current_link;

	print_info(lem);
	current_path = lem->paths;
	while (current_path)
	{
		current_link = current_path->head;
		ft_putstr("\033[93m");
		ft_putstr(current_link->start->name);
		while (current_link)
		{
			ft_putstr("\033[90m -> \033[0m");
			ft_putstr("\033[93m");
			ft_putstr(current_link->end->name);
			current_link = current_link->next;
		}
		ft_putendl(NULL);
		current_path = current_path->next;
	}
	ft_putendl("\n\033[94m---------- Result ----------\033[0m");
}

void	print_locations(t_lem *lem)
{
	t_loc	*current;

	current = lem->loc;
	while (current)
	{
		if (current != lem->loc)
			ft_putchar(' ');
		ft_putchar('L');
		ft_putnbr(current->ant_number);
		ft_putchar('-');
		ft_putstr(current->rm->name);
		current = current->next;
	}
	ft_putendl(NULL);
}
