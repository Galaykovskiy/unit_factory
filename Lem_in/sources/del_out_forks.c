
#include "../includes/lem_in.h"

static t_bool	path_len(t_lem *lem, t_lk *lk, int len)
{
	if (lk->end == lem->end)
		return (len + 1);
	if (lk->end->ip_ln > 1)
		return (len + 1);
	return (path_len(lem, find_lk(lem, lk->end, NULL), len + 1));
}

static void		del_other_out_lk(t_lem *lem, t_lk *lk)
{
	t_rm	*st;
	t_lk	*cur;
	t_lk	*del;

	st = lk->start;
	cur = lem->lk;
	while (cur)
	{
		del = cur;
		cur = cur->next;
		if (del->start == st && del != lk)
			del_lk(lem, del);
	}
}

static void		del_out_fork(t_lem *lem, t_rm *rm)
{
	t_lk	*cur;
	t_lk	*min_path;
	int		min_path_len;
	int		cur_path_len;

	min_path_len = FT_INT_MAX;
	min_path = NULL;
	cur = lem->lk;
	while (cur && rm->op_lk > 1)
	{
		if (cur->start == rm)
		{
			if (min_path_len > (cur_path_len = path_len(lem, cur, 0)))
			{
				min_path_len = cur_path_len;
				min_path = cur;
			}
		}
		cur = cur->next;
	}
	del_other_out_lk(lem, min_path);
	del_dead_ends(lem);
}

void			del_out_forks(t_lem *lem)
{
	int		bfs_lvl;
	t_rm	*cur;

	bfs_lvl = lem->bfs_lvl;
	while (bfs_lvl > 0)
	{
		cur = lem->rms;
		while (cur)
		{
			if (cur->bfs_lvl == bfs_lvl
				&& cur->op_lk > 1)
				del_out_fork(lem, cur);
			cur = cur->next;
		}
		bfs_lvl--;
	}
}
