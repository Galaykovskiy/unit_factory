
#include "../includes/lem_in.h"

static t_type	get_type(char *str)
{
	if (!ft_strcmp(str, "##start"))
		return (START);
	else if (!ft_strcmp(str, "##end"))
		return (END);
	else
		return (MIDDLE);
}

static t_rm		*create_rm(char *str, t_type type)
{
	char	**mas;
	t_rm	*rm;

	if (!(mas = ft_strsplit(str, ' ')))
		ft_exit(ER_RM_INIT);
	if (!(rm = (t_rm *)ft_memalloc(sizeof(t_rm))))
		ft_exit(ER_RM_INIT);
	if (!(rm->name = ft_strdup(mas[0])))
		ft_exit(ER_RM_INIT);
	rm->x = ft_atoi(mas[1]);
	rm->y = ft_atoi(mas[2]);
	rm->type = type;
	rm->bfs_lvl = -1;
	rm->ip_ln = 0;
	rm->op_lk = 0;
	rm->ant_nbr = -1;
	rm->next = NULL;
	ft_strsplit_free(&mas);
	return (rm);
}

static void		add_rm(t_lem *lem, t_rm *rm)
{
	t_rm	*cur;

	lem->rm_num++;
	if ((cur = lem->rms))
	{
		while (cur->next)
			cur = cur->next;
		cur->next = rm;
	}
	else
		lem->rms = rm;
	if (rm->type == START)
		lem->st = rm;
	else if (rm->type == END)
		lem->end = rm;
}

void			parse_rm(t_lem *lem, t_l **cur, t_l **l)
{
	t_type	type;
	t_rm	*rm;

	type = MIDDLE;
	while ((*cur || ((*cur) = read_next_line(l)))
		&& (ft_is_command((*cur)->cont)
		|| ft_is_comment((*cur)->cont)
		|| ft_is_rm((*cur)->cont)))
	{
		if (ft_is_command((*cur)->cont))
			type = get_type((*cur)->cont);
		else if (ft_is_rm((*cur)->cont))
		{
			add_rm(lem, (rm = create_rm((*cur)->cont, type)));
			valid_rm(lem, rm);
			type = MIDDLE;
		}
		else
			type = MIDDLE;
		if ((type == START && lem->st)
			|| (type == END && lem->end))
			ft_exit(ER_RM_PARSING);
		(*cur) = NULL;
	}
}
