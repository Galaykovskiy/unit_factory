
#include "../includes/lem_in.h"

t_lk	*find_lk(t_lem *lem, t_rm *st, t_rm *e)
{
	t_lk	*cur;

	cur = lem->lk;
	while (cur)
	{
		if (st && cur->start == st)
			return (cur);
		if (e && cur->end == e)
			return (cur);
		cur = cur->next;
	}
	return (NULL);
}

t_rm	*find_rm(t_lem *lem, char *name)
{
	t_rm	*cur;

	cur = lem->rms;
	while (cur)
	{
		if (!ft_strcmp(cur->name, name))
			return (cur);
		cur = cur->next;
	}
	return (NULL);
}

