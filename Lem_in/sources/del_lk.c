
#include "../includes/lem_in.h"

void	del_lk(t_lem *lem, t_lk *lk)
{
	t_lk	*previous;
	t_lk	*current;

	previous = NULL;
	current = lem->lk;
	while (current && current != lk)
	{
		previous = current;
		current = current->next;
	}
	if (!previous && current)
		lem->lk = current->next;
	else if (current)
		previous->next = current->next;
	if (lk->start->op_lk > 0)
		lk->start->op_lk--;
	if (lk->end->ip_ln > 0)
		lk->end->ip_ln--;
	free(lk);
}

t_lk	*rem_lk(t_lem *lem, t_lk *lk)
{
	t_lk	*prev;
	t_lk	*cur;

	prev = NULL;
	cur = lem->lk;
	while (cur && cur != lk)
	{
		prev = cur;
		cur = cur->next;
	}
	if (!prev && cur)
		lem->lk = cur->next;
	else if (cur)
		prev->next = cur->next;
	lk->next = NULL;
	return (lk);
}

void	del_useless_lk(t_lem *lem)
{
	t_lk	*cur;
	t_lk	*del;

	cur = lem->lk;
	while (cur)
	{
		del = cur;
		cur = cur->next;
		if (del->start->bfs_lvl == -1 || del->end->bfs_lvl == -1
			|| del->start->bfs_lvl == del->end->bfs_lvl)
			del_lk(lem, del);
	}
}

void	del_dead_ends(t_lem *lem)
{
	t_bool	dead;
	t_lk	*cur;
	t_lk	*del;

	dead = true;
	while (dead)
	{
		dead = false;
		cur = lem->lk;
		while (cur)
		{
			del = cur;
			cur = cur->next;
			if ((del->start != lem->st && del->start->ip_ln == 0
				&& del->start->op_lk > 0) || (del->end != lem->end
				&& del->end->ip_ln > 0 && del->end->op_lk == 0))
			{
				del_lk(lem, del);
				dead = true;
			}
		}
	}
}
