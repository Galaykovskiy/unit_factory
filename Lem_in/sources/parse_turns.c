
#include "../includes/lem_in.h"

static t_turn	*create_turn(void)
{
	t_turn	*turn;

	if (!(turn = (t_turn *)ft_memalloc(sizeof(t_turn))))
		terminate(ER_TURN_INIT);
	turn->loc = NULL;
	turn->next = NULL;
	return (turn);
}

static void		add_turn(t_lem *lem_in, t_turn *turn)
{
	t_turn	*current;

	current = lem_in->turns;
	if (current)
	{
		while (current->next)
			current = current->next;
		current->next = turn;
	}
	else
		lem_in->turns = turn;
}

void			parse_turns(t_lem *lem, t_l **cur, t_l **l)
{
	t_turn	*turn;

	while (*cur || ((*cur) = read_next_line(l)))
	{
		turn = create_turn();
		turn->loc = parse_loc(lem, (*cur)->cont);
		add_turn(lem, turn);
		(*cur) = NULL;
	}
}
