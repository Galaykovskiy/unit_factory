
#include "../includes/lem_in.h"

static void		move_ants_along_path(t_lem *lem_in, t_path *path)
{
	t_lk	*current;

	current = path->head;
	while (current->next)
		current = current->next;
	while (current)
	{
		if (current->start->ant_nbr != -1 && current->end == lem_in->end)
		{
			turn_add_location(lem_in,
				turn_create_location(current->start->ant_nbr, current->end));
			current->start->ant_nbr = -1;
			lem_in->ants_e++;
		}
		else if (current->start->ant_nbr != -1)
		{
			turn_add_location(lem_in,
				turn_create_location(current->start->ant_nbr, current->end));
			current->end->ant_nbr = current->start->ant_nbr;
			current->start->ant_nbr = -1;
		}
		current = current->prev;
	}
}

static void		move_ants_along_paths(t_lem *lem_in)
{
	t_path	*current;

	current = lem_in->paths;
	while (current)
	{
		move_ants_along_path(lem_in, current);
		current = current->next;
	}
}

static void		move_ant_from_start(t_lem *lem_in, t_path *path)
{
	if (path->head->end == lem_in->end)
	{
		lem_in->ants_st--;
		lem_in->ants_e++;
		turn_add_location(lem_in,
			turn_create_location(++(lem_in->ant_nbr), path->head->end));
	}
	else
	{
		lem_in->ants_st--;
		path->head->end->ant_nbr = ++(lem_in->ant_nbr);
		turn_add_location(lem_in,
			turn_create_location(path->head->end->ant_nbr, path->head->end));
	}
}

static void		move_ants_from_start(t_lem *lem_in)
{
	t_path	*current;

	current = lem_in->paths;
	while (current && lem_in->ants_st)
	{
		if (lem_in->ants_st > calculate_diff(lem_in, current))
			move_ant_from_start(lem_in, current);
		current = current->next;
	}
}

void			perform_turns(t_lem *lem, t_bool ext)
{
	int 	i;

	i = 0;
	while (lem->ants_st || lem->ant_nbr != lem->ants_e)
	{
		move_ants_along_paths(lem);
		move_ants_from_start(lem);
		print_locations(lem);
		free_loc(&(lem->loc));
		lem->loc = NULL;
		i++;
	}
	if (ext)
	{
		ft_putstr("\n\033[92mNumber of steps: \033[94m");
		ft_putnbr(i);
		ft_putendl("\033[0m");
	}
}
