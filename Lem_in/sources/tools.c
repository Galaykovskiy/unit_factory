
#include "../includes/lem_in.h"

void		valid_rm(t_lem *lem, t_rm *rm)
{
	t_rm	*i;

	i = lem->rms;
	while (i)
	{
		if (rm != i && !ft_strcmp(rm->name, i->name))
			ft_exit(ER_RM_NAME_DUP);
		if (rm != i && rm->x == i->x && rm->y == i->y)
			ft_exit(ER_RM_COORDS_DUP);
		i = i->next;
	}
}

void		valid_lk(t_lem *lem, t_lk *lk)
{
	t_lk	*i;

	i = lem->lk;
	while (i)
	{
		if (lk != i && ((!ft_strcmp(lk->start->name, i->start->name)
			  && !ft_strcmp(lk->end->name, i->end->name)) ||
			 (!ft_strcmp(lk->start->name, i->end->name)
			  && !ft_strcmp(lk->end->name, i->start->name))))
			ft_exit(ER_LINK_DUP);
		i = i->next;
	}
}

void	ft_exit(char *s)
{
	ft_putstr("\033[91m");
	if (errno == 0)
		ft_putendl_fd(s, 2);
	else
		perror(s);
	ft_putstr("\033[0m");
	exit(1);
}
