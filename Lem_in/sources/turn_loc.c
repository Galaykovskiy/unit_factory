
#include "../includes/lem_in.h"

t_loc	*turn_create_loc(int ant_nbr, t_rm *rm)
{
	t_loc	*loc;

	if (!(loc = (t_loc *)ft_memalloc(sizeof(t_loc))))
		ft_exit(ER_LOC_INIT);
	loc->ant_nbr = ant_nbr;
	loc->rm = rm;
	loc->next = NULL;
	return (loc);
}

void	turn_add_loc(t_lem *lem, t_loc *loc)
{
	t_loc	*prev;
	t_loc	*cur;

	prev = NULL;
	cur = lem->loc;
	if (cur)
	{
		while (cur && loc->ant_nbr > cur->ant_nbr)
		{
			prev = cur;
			cur = cur->next;
		}
		if (!prev)
			lem->loc = loc;
		else
			prev->next = loc;
		loc->next = cur;
	}
	else
		lem->loc = loc;
}
