
#include "../includes/lem_in.h"

static t_q	*q_cr_elem(t_rm *rm)
{
	t_q	*q;

	if (!(q = (t_q *)ft_memalloc(sizeof(t_q))))
		terminate(ER_QUEUE_INIT);
	q->rm = rm;
	q->next = NULL;
	return (q);
}

static t_q	*q_get_head(t_q **q)
{
	t_q		*elem;

	elem = NULL;
	if (q && *q)
	{
		elem = (*q);
		(*q) = (*q)->next;
	}
	return (elem);
}

static void		q_add_elem(t_q **q, t_q *elem)
{
	t_q	*cur;

	if (q && *q)
	{
		cur = (*q);
		while (cur->next)
			cur = cur->next;
		cur->next = elem;
	}
	else if (q)
		(*q) = elem;
}

static void		q_add_lk_elems(t_lem *lem, t_q **q, t_rm *rm)
{
	t_lk	*cur;

	cur = lem->lk;
	while (cur)
	{
		if (cur->start == rm)
		{
			if (cur->end->bfs_lvl == -1)
			{
				cur->end->bfs_lvl = rm->bfs_lvl + 1;
				q_add_elem(q, q_cr_elem(cur->end));
			}
		}
		else if (cur->end == rm)
		{
			if (cur->start->bfs_lvl == -1)
			{
				cur->start->bfs_lvl = rm->bfs_lvl + 1;
				q_add_elem(q, q_cr_elem(cur->start));
			}
		}
		cur = cur->next;
	}
}

void			bfs(t_lem *lem)
{
	t_q		*q;
	t_q		*cur;

	q = NULL;
	lem->st->bfs_lvl = 0;
	q_add_elem(&q, q_cr_elem(lem->st));
	while (q != NULL)
	{
		cur = q_get_head(&q);
		if (cur->rm != lem->end)
		{
			q_add_lk_elems(lem, &q, cur->rm);
			lem->bfs_lvl = cur->rm->bfs_lvl;
		}
		else
			lem->end->bfs_lvl = FT_INT_MAX;
		free(cur);
	}
}
