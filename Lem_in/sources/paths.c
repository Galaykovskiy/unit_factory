
#include "../includes/lem_in.h"

int		calculate_diff(t_lem *lem, t_path *path)
{
	int		diff;
	t_path	*current;

	diff = 0;
	current = lem->paths;
	while (current != path)
	{
		diff += path->len - current->len;
		current = current->next;
	}
	return (diff);
}

static t_path	*create_path(void)
{
	t_path	*path;

	if (!(path = (t_path *)ft_memalloc(sizeof(t_path))))
		terminate(ER_PATH_INIT);
	path->head = NULL;
	path->len = 0;
	path->next = NULL;
	return (path);
}

static void		path_add_link(t_path *path, t_lk *link)
{
	t_lk	*previous;
	t_lk	*current;

	previous = NULL;
	current = path->head;
	if (current)
	{
		while (current)
		{
			previous = current;
			current = current->next;
		}
		link->prev = previous;
		previous->next = link;
	}
	else
		path->head = link;
	path->len++;
}

static void		add_path(t_lem *lem_in, t_path *path)
{
	t_path	*previous;
	t_path	*current;

	previous = NULL;
	current = lem_in->paths;
	if (current)
	{
		while (current && path->len > current->len)
		{
			previous = current;
			current = current->next;
		}
		if (!previous)
			lem_in->paths = path;
		else
			previous->next = path;
		path->next = current;
	}
	else
		lem_in->paths = path;
}

void			form_paths(t_lem *lem)
{
	t_path	*path;
	t_lk	*link;

	while (lem->lk)
	{
		path = create_path();
		link = find_lk(lem, lem->st, NULL);
		path_add_link(path, rem_lk(lem, link));
		while (link->end != lem->end)
		{
			link = find_lk(lem, link->end, NULL);
			path_add_link(path, rem_lk(lem, link));
		}
		add_path(lem, path);
	}
}
