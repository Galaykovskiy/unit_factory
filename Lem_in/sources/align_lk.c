
#include "../includes/lem_in.h"

static void	swap_rm(t_rm **st, t_rm **e)
{
	t_rm	*tmp;

	tmp = *st;
	*st = *e;
	*e = tmp;
}

void		align_lk(t_lem *lem)
{
	t_lk	*cur;

	cur = lem->lk;
	while (cur)
	{
		if (cur->start->bfs_lvl > cur->end->bfs_lvl)
			swap_rm(&cur->start, &cur->end);
		cur = cur->next;
	}
}

void	count_io_lk(t_lem *lem)
{
	t_lk *cur;

	cur = lem->lk;
	while (cur)
	{
		cur->start->op_lk++;
		cur->end->ip_ln++;
		cur = cur->next;
	}
}

