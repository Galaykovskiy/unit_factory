/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:21:19 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:21:22 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int	ft_check(char *str, int negnbr)
{
	int		i;

	i = 0;
	while (*str >= '0' && *str++ <= '9')
		i++;
	if (i > 18 && negnbr)
		i = 0;
	else if (i > 18 && !negnbr)
		i = -1;
	return (i);
}

int			ft_atoi(char *str)
{
	int i;
	int result;
	int negnbr;

	result = 0;
	negnbr = 0;
	i = 0;
	while (str[i] == '\n' || str[i] == '\t' || str[i] == '\v' ||
		str[i] == ' ' || str[i] == '\f' || str[i] == '\r')
		i++;
	if (str[i] == '-')
		negnbr = 1;
	if (str[i] == '+' || str[i] == '-')
		i++;
	if (ft_check(&str[i], negnbr) < 1)
		return (ft_check(&str[i], negnbr));
	while (str[i] >= '0' && str[i] <= '9')
	{
		result *= 10;
		result += (int)str[i++] - '0';
	}
	if (negnbr == 1)
		return (-result);
	else
		return (result);
}
