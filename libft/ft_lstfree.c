/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 19:31:56 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/30 19:31:57 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstfree(t_list *lst)
{
	t_list *list;

	list = lst;
	while (list)
	{
		if (list->content)
			free(list->content);
		free(list);
		list = list->next;
	}
	lst = NULL;
}
