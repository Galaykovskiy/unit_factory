/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:33:07 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:33:10 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		i;
	char	*a;

	i = 0;
	if (!s || !f)
		return (NULL);
	while (s[i] != '\0')
		i++;
	a = (char*)malloc(sizeof(*a) * (i + 1));
	if (!a)
		return (NULL);
	i = 0;
	while (*s != '\0')
		a[i++] = f(*s++);
	a[i] = '\0';
	return (a);
}
