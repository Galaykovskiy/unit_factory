/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_input.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/05 20:58:50 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/05 20:58:51 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		set_settings(t_fr *fract)
{
	fract->get_help = 0;
	fract->wow_mode = 0;
	fract->get_color = 0;
	fract->get_fract = 0;
}

static int	choose_fractol(int keycode, t_fr *fract)
{
	if (keycode == 18)
		fract->fractol = 0;
	else if (keycode == 19)
		fract->fractol = 1;
	else if (keycode == 20)
		fract->fractol = 2;
	else if (keycode == 21)
		fract->fractol = 3;
	else if (keycode == 22)
		fract->fractol = 4;
	else if (keycode == 23)
		fract->fractol = 5;
	if (keycode > 17 && keycode < 24)
		fract_init(fract);
	return (0);
}

static int	key_color(int keycode, t_fr *fract)
{
	if (keycode == 82)
		fract->color = 1677216;
	else if (keycode == 83)
		fract->color = 2050;
	else if (keycode == 84)
		fract->color = 265;
	else if (keycode == 85)
		fract->color = 1048576;
	else if (keycode == 48)
	{
		if (fract->wow_mode)
			fract->wow_mode = 0;
		else
			fract->wow_mode = 1;
	}
	else if (keycode == 8)
	{
		if (fract->get_color)
			fract->get_color = 0;
		else
			fract->get_color = 1;
	}
	choose_fractol(keycode, fract);
	return (0);
}

static int	key_hook2(int keycode, t_fr *fract)
{
	if (keycode == 49)
	{
		if (fract->mouse_julia == 1)
			fract->mouse_julia = 0;
		else
			fract->mouse_julia = 1;
	}
	else if (keycode == 4)
	{
		if (fract->get_help)
			fract->get_help = 0;
		else
			fract->get_help = 1;
	}
	else if (keycode == 3)
	{
		if (fract->get_fract)
			fract->get_fract = 0;
		else
			fract->get_fract = 1;
	}
	key_color(keycode, fract);
	return (0);
}

int			key_hook(int keycode, t_fr *fract)
{
	if (keycode == 53)
		exit(1);
	else if (keycode == 69)
		fract->max_i++;
	else if (keycode == 78)
		fract->max_i--;
	else if (keycode == 123)
		fract->x1 -= 30 / fract->zoom;
	else if (keycode == 124)
		fract->x1 += 30 / fract->zoom;
	else if (keycode == 125)
		fract->y1 += 30 / fract->zoom;
	else if (keycode == 126)
		fract->y1 -= 30 / fract->zoom;
	else if (keycode == 15)
		fract_init(fract);
	key_hook2(keycode, fract);
	fract_calc(fract);
	return (0);
}
