/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/10 17:52:47 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/10 17:52:48 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		get_colors(t_fr *fract)
{
	char	*txt;

	txt = ft_newstr("~Numpad~");
	mlx_string_put(fract->mlx, fract->win, 10, 470, WHITE, txt);
	ft_strdel(&txt);
	txt = ft_newstr("L");
	mlx_string_put(fract->mlx, fract->win, 10, 490, 1677216, txt);
	ft_strdel(&txt);
	txt = ft_newstr("S");
	mlx_string_put(fract->mlx, fract->win, 20, 490, 0xF7DC6F, txt);
	ft_strdel(&txt);
	txt = ft_newstr("D....0");
	mlx_string_put(fract->mlx, fract->win, 30, 490, 0xFF5B7B1, txt);
	ft_strdel(&txt);
	txt = ft_newstr("Green..1");
	mlx_string_put(fract->mlx, fract->win, 10, 510, 0x229954, txt);
	ft_strdel(&txt);
	txt = ft_newstr("Blue...2");
	mlx_string_put(fract->mlx, fract->win, 10, 530, 0x2E86C1, txt);
	ft_strdel(&txt);
	txt = ft_newstr("Red....3");
	mlx_string_put(fract->mlx, fract->win, 10, 550, 0xCB4335, txt);
	ft_strdel(&txt);
}

static int	text_color(t_fr *fract)
{
	if (fract->color == 265)
		return (ft_hextoint("AED6F1"));
	else if (fract->color == 2050)
		return (ft_hextoint("A9DFBF"));
	else if (fract->color == 1048576)
		return (ft_hextoint("E6B0AA"));
	else
		return (ft_hextoint("F7DC6F"));
}

void		get_fractols(t_fr *f)
{
	char	*t;
	int		c;

	c = text_color(f);
	t = ft_newstr("Mandelbrot...1");
	mlx_string_put(f->mlx, f->win, 10, 670, (f->fractol == 0) ? c : WHITE, t);
	ft_strdel(&t);
	t = ft_newstr("Julia........2");
	mlx_string_put(f->mlx, f->win, 10, 690, (f->fractol == 1) ? c : WHITE, t);
	ft_strdel(&t);
	t = ft_newstr("Burningship..3");
	mlx_string_put(f->mlx, f->win, 10, 710, (f->fractol == 2) ? c : WHITE, t);
	ft_strdel(&t);
	t = ft_newstr("Mushroom.....4");
	mlx_string_put(f->mlx, f->win, 10, 730, (f->fractol == 3) ? c : WHITE, t);
	ft_strdel(&t);
	t = ft_newstr("Pudding......5");
	mlx_string_put(f->mlx, f->win, 10, 750, (f->fractol == 5) ? c : WHITE, t);
	ft_strdel(&t);
	t = ft_newstr("Gangurru.....6");
	mlx_string_put(f->mlx, f->win, 10, 770, (f->fractol == 4) ? c : WHITE, t);
	ft_strdel(&t);
}

void		get_help(t_fr *fract)
{
	char	*txt;

	txt = ft_newstr("Fractals: F");
	mlx_string_put(fract->mlx, fract->win, 10, 70, WHITE, txt);
	ft_strdel(&txt);
	txt = ft_newstr("Move: Arrows");
	mlx_string_put(fract->mlx, fract->win, 10, 90, WHITE, txt);
	ft_strdel(&txt);
	txt = ft_newstr("Mouse fix: Space");
	mlx_string_put(fract->mlx, fract->win, 10, 110, WHITE, txt);
	ft_strdel(&txt);
	txt = ft_newstr("Special mode: Tab");
	mlx_string_put(fract->mlx, fract->win, 10, 130, WHITE, txt);
	ft_strdel(&txt);
	txt = ft_newstr("Reload fractal: R");
	mlx_string_put(fract->mlx, fract->win, 10, 150, WHITE, txt);
	ft_strdel(&txt);
	txt = ft_newstr("Increase iterations: +");
	mlx_string_put(fract->mlx, fract->win, 10, 170, WHITE, txt);
	ft_strdel(&txt);
	txt = ft_newstr("Decrease iterations: -");
	mlx_string_put(fract->mlx, fract->win, 10, 190, WHITE, txt);
	ft_strdel(&txt);
}

void		right_fractol(void)
{
	ft_putendl("Usage /fractol [name1] <name2>\n\t➤ mandelbrot");
	ft_putendl("\t➤ julia\n\t➤ burningship\n\t➤ mushroom");
	ft_putendl("\t➤ pudding\n\t➤ gangurru");
}
