/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/10 22:02:49 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/10 22:02:50 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	ft_zoom(int x, int y, t_fr *fract)
{
	fract->x2 = x;
	fract->y2 = y;
	fract->x1 = (x / fract->zoom + fract->x1) - ((fract->zoom * 1.3) / 2);
	fract->x1 += ((fract->zoom * 1.3) / 2) - (x / (fract->zoom * 1.3));
	fract->y1 = (y / fract->zoom + fract->y1) - ((fract->zoom * 1.3) / 2);
	fract->y1 += ((fract->zoom * 1.3) / 2) - (y / (fract->zoom * 1.3));
	fract->zoom *= 1.3;
	fract->max_i++;
}

static void	ft_dezoom(t_fr *fract)
{
	fract->x1 = (fract->x2 / fract->zoom + fract->x1) - ((fract->zoom / 1.3)
		/ 2);
	fract->x1 += ((fract->zoom / 1.3) / 2) - (fract->x2 / (fract->zoom / 1.3));
	fract->y1 = (fract->y2 / fract->zoom + fract->y1) - ((fract->zoom / 1.3)
		/ 2);
	fract->y1 += ((fract->zoom / 1.3) / 2) - (fract->y2 / (fract->zoom / 1.3));
	fract->zoom /= 1.3;
	fract->max_i--;
}

int			mouse_hook(int mousecode, int x, int y, t_fr *fract)
{
	if ((mousecode == 4 || mousecode == 1))
		ft_zoom(x, y, fract);
	else if (mousecode == 5 || mousecode == 2)
		ft_dezoom(fract);
	fract_calc(fract);
	return (0);
}

void		mouse_mlx(t_fr *fract)
{
	mlx_hook(fract->win, 6, 1L < 6, mouse_julia, fract);
	mlx_hook(fract->win, 17, 0L, ft_close, fract);
	mlx_hook(fract->win, 2, 1L << 0, key_hook, fract);
	mlx_mouse_hook(fract->win, mouse_hook, fract);
}
