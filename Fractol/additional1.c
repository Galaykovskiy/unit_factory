/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   additional1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 19:24:45 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/09 19:24:46 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		additional1_init(t_fr *fract)
{
	fract->max_i = 50;
	fract->zoom = 225;
	fract->x1 = -2.6;
	fract->y1 = -1.75;
	fract->color = 265;
}

void		additional1_calc(t_fr *fract)
{
	fract->n_r = fract->x / fract->zoom + fract->x1;
	fract->n_i = fract->y / fract->zoom + fract->y1;
	fract->o_r = 0;
	fract->o_i = 0;
	fract->i = 0;
	while (fract->o_r * fract->o_r + fract->o_i *
			fract->o_i < 4 && fract->i < fract->max_i)
	{
		fract->tmp = fract->o_r;
		fract->o_r = fabs(fract->o_r * fract->o_r -
			fract->o_i * fract->o_i) + fract->n_r;
		fract->o_i = 2 * fract->o_i * fract->tmp + fract->n_i;
		fract->i++;
	}
	if (fract->i == fract->max_i)
		put_pxl_to_img(fract, fract->x, fract->y, 0x000000);
	else
	{
		if (fract->wow_mode)
			put_pxl(fract, fract->x, fract->y);
		else
			put_pxl_to_img(fract, fract->x, fract->y, (fract->color
				* fract->i));
	}
}

void		*additional1(void *tab)
{
	t_fr	*fract;
	double	tmp;

	fract = (t_fr *)tab;
	fract->x = 0;
	tmp = fract->y;
	while (fract->x < WIN_S)
	{
		fract->y = tmp;
		while (fract->y < fract->y_max)
		{
			additional1_calc(fract);
			fract->y++;
		}
		fract->x++;
	}
	return (tab);
}

void		additional1_pthread(t_fr *fract)
{
	t_fr		tab[8];
	pthread_t	t[8];
	int			i;

	i = 0;
	while (i < 8)
	{
		ft_memcpy((void*)&tab[i], (void*)fract, sizeof(t_fr));
		tab[i].y = 100 * i;
		tab[i].y_max = 100 * (i + 1);
		i++;
	}
	i = 0;
	while (++i <= 8)
		pthread_create(&t[i - 1], NULL, additional1, &tab[i - 1]);
	while (i--)
		pthread_join(t[i], NULL);
	mlx_put_image_to_window(fract->mlx, fract->win, fract->img, 0, 0);
}
