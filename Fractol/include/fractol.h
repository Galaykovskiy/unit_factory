/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/10 22:04:25 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/10 22:04:25 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# define WIN_S 800
# define WHITE 0xFFFFFF
# include "libft.h"
# include <mlx.h>
# include <math.h>
# include <pthread.h>

typedef struct	s_fr
{
	void		*mlx;
	void		*win;
	void		*img;
	void		*img_ptr;
	int			bpp;
	int			size_l;
	int			endian;
	int			mouse_julia;
	int			color;
	int			fractol;
	int			get_help;
	int			get_fract;
	int			get_color;
	int			wow_mode;
	double		zoom;
	double		max_i;
	double		y;
	double		x;
	double		x1;
	double		y1;
	double		x2;
	double		y2;
	double		y_max;
	double		i;
	double		n_r;
	double		n_i;
	double		o_r;
	double		o_i;
	double		tmp;
}				t_fr;

void			fract_init(t_fr *fract);
void			fract_calc(t_fr *fract);
void			mlx_win_init(t_fr *fract, void *mlx, char *name);
void			julia_pthread(t_fr *fract);
void			julia_init(t_fr *fract);
int				mouse_julia(int x, int y, t_fr *fract);
void			mandelbrot_pthread(t_fr *fract);
void			mandelbrot_init(t_fr *fract);
void			burningship_pthread(t_fr *fract);
void			burningship_init(t_fr *fract);
void			additional1_pthread(t_fr *fract);
void			additional1_init(t_fr *fract);
void			additional2_pthread(t_fr *fract);
void			additional2_init(t_fr *fract);
void			additional3_pthread(t_fr *fract);
void			additional3_init(t_fr *fract);
void			put_pxl_to_img(t_fr *fract, int x, int y, int color);
void			put_pxl(t_fr *fract, int x, int y);
void			set_settings(t_fr *fract);
int				wow_blue(int iter);
int				wow_red(int iter);
int				wow_green(int iter);
int				wow_lsd(int iter);
int				key_hook(int keycode, t_fr *fract);
int				mouse_hook(int mousecode, int x, int y, t_fr *fract);
void			mouse_mlx(t_fr *fract);
void			put_text(t_fr *fract);
void			get_help(t_fr *fract);
void			get_fractols(t_fr *fract);
void			get_colors(t_fr *fract);
void			right_fractol();
int				ft_close(void);
#endif
