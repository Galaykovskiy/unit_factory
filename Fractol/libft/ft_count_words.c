/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_words.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/30 15:32:34 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/30 15:32:37 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_count_words(const char *str, char ch)
{
	int i;
	int words;

	i = 0;
	words = 0;
	if (!str || !ch)
		return (0);
	while (str[i])
	{
		while (str[i] == ch)
			i++;
		if (str[i] == '\0')
			break ;
		words++;
		while (str[i] && str[i] != ch)
			i++;
	}
	return (words);
}
