/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:32:35 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:32:38 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*a;
	int		i;
	int		j;

	if (!s1 || !s2)
		return (NULL);
	i = 0;
	j = 0;
	while (s1[j] != '\0')
		j++;
	while (s2[i] != '\0')
		i++;
	a = (char*)malloc(sizeof(*a) * (i + j + 1));
	if (!a)
		return (NULL);
	i = 0;
	while (*s1 != '\0')
		a[i++] = *s1++;
	while (*s2 != '\0')
		a[i++] = *s2++;
	a[i] = '\0';
	return (a);
}
