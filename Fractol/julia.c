/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/10 22:02:41 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/10 22:02:41 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		mouse_julia(int x, int y, t_fr *fract)
{
	if (fract->fractol == 1 && fract->mouse_julia == 1)
	{
		fract->n_r = x * 2;
		fract->n_i = y * 2 - 800;
		fract_calc(fract);
	}
	return (0);
}

void	julia_init(t_fr *fract)
{
	fract->max_i = 50;
	fract->zoom = 200;
	fract->x1 = -2.0;
	fract->y1 = -1.9;
	fract->color = 265;
	fract->n_r = 0.285;
	fract->n_i = 0.01;
	fract->mouse_julia = 1;
}

void	julia_calc(t_fr *fract)
{
	fract->o_r = fract->x / fract->zoom + fract->x1;
	fract->o_i = fract->y / fract->zoom + fract->y1;
	fract->i = 0;
	while (fract->o_r * fract->o_r + fract->o_i
			* fract->o_i < 4 && fract->i < fract->max_i)
	{
		fract->tmp = fract->o_r;
		fract->o_r = fract->o_r * fract->o_r -
			fract->o_i * fract->o_i - 0.8 + (fract->n_r / WIN_S);
		fract->o_i = 2 * fract->o_i * fract->tmp + fract->n_i / WIN_S;
		fract->i++;
	}
	if (fract->i == fract->max_i)
		put_pxl_to_img(fract, fract->x, fract->y, 0x000000);
	else
	{
		if (fract->wow_mode)
			put_pxl(fract, fract->x, fract->y);
		else
			put_pxl_to_img(fract, fract->x, fract->y, (fract->color
				* fract->i));
	}
}

void	*julia(void *tab)
{
	double	tmp;
	t_fr	*fract;

	fract = (t_fr *)tab;
	fract->x = 0;
	tmp = fract->y;
	while (fract->x < WIN_S)
	{
		fract->y = tmp;
		while (fract->y < fract->y_max)
		{
			julia_calc(fract);
			fract->y++;
		}
		fract->x++;
	}
	return (tab);
}

void	julia_pthread(t_fr *fract)
{
	t_fr		tab[8];
	pthread_t	t[8];
	int			i;

	i = 0;
	while (i < 8)
	{
		ft_memcpy((void*)&tab[i], (void*)fract, sizeof(t_fr));
		tab[i].y = 100 * i;
		tab[i].y_max = 100 * (i + 1);
		i++;
	}
	i = 0;
	while (++i <= 8)
		pthread_create(&t[i - 1], NULL, julia, &tab[i - 1]);
	while (i--)
		pthread_join(t[i], NULL);
	mlx_put_image_to_window(fract->mlx, fract->win, fract->img, 0, 0);
}
