/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/10 22:02:56 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/10 22:02:57 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_close(void)
{
	exit(0);
	return (0);
}

void	put_pxl(t_fr *fract, int x, int y)
{
	int		color;

	if (fract->color == 265)
		color = wow_blue(fract->i);
	else if (fract->color == 2050)
		color = wow_green(fract->i);
	else if (fract->color == 1048576)
		color = wow_red(fract->i);
	else
		color = wow_lsd(fract->i);
	put_pxl_to_img(fract, x, y, color);
}

void	put_pxl_to_img(t_fr *fract, int x, int y, int color)
{
	if (fract->x < WIN_S && fract->y < WIN_S)
	{
		color = mlx_get_color_value(fract->mlx, color);
		ft_memcpy(fract->img_ptr + 4 * WIN_S * y + x * 4,
				&color, sizeof(int));
	}
}

void	put_text(t_fr *fract)
{
	char	*text;
	char	*nb;

	nb = ft_itoa(fract->max_i);
	text = ft_strjoin("Iterations: ", nb);
	mlx_string_put(fract->mlx, fract->win, 10, 10, 0xFFFFFF, text);
	ft_strdel(&text);
	ft_strdel(&nb);
	text = ft_newstr("Get help: H");
	mlx_string_put(fract->mlx, fract->win, 10, 30, 0xFFFFFF, text);
	ft_strdel(&text);
	if (fract->get_help)
	{
		text = ft_newstr("Colors: C");
		mlx_string_put(fract->mlx, fract->win, 10, 50, WHITE, text);
		ft_strdel(&text);
		get_help(fract);
	}
	if (fract->get_fract)
		get_fractols(fract);
	if (fract->get_color)
		get_colors(fract);
}

void	mlx_win_init(t_fr *fract, void *mlx, char *name)
{
	fract->win = mlx_new_window(mlx, WIN_S, WIN_S, name);
	fract->img = mlx_new_image(mlx, WIN_S, WIN_S);
	fract->img_ptr = mlx_get_data_addr(fract->img,
			&fract->bpp, &fract->size_l, &fract->endian);
}
