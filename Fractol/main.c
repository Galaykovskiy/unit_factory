/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/10 21:46:09 by agalayko          #+#    #+#             */
/*   Updated: 2018/10/15 17:03:42 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	fract_calc(t_fr *fract)
{
	if (fract->max_i < 0)
		fract->max_i = 0;
	if (fract->fractol == 0)
		mandelbrot_pthread(fract);
	else if (fract->fractol == 1)
		julia_pthread(fract);
	else if (fract->fractol == 2)
		burningship_pthread(fract);
	else if (fract->fractol == 3)
		additional1_pthread(fract);
	else if (fract->fractol == 4)
		additional2_pthread(fract);
	else if (fract->fractol == 5)
		additional3_pthread(fract);
	put_text(fract);
}

void	fract_init(t_fr *fract)
{
	if (fract->fractol == 0)
		mandelbrot_init(fract);
	else if (fract->fractol == 1)
		julia_init(fract);
	else if (fract->fractol == 2)
		burningship_init(fract);
	else if (fract->fractol == 3)
		additional1_init(fract);
	else if (fract->fractol == 4)
		additional2_init(fract);
	else if (fract->fractol == 5)
		additional3_init(fract);
	fract_calc(fract);
}

int		fract_comp(char *argv, t_fr *fract)
{
	ft_strlowcase(argv);
	if (ft_strcmp(argv, "mandelbrot") == 0)
		fract->fractol = 0;
	else if (ft_strcmp(argv, "julia") == 0)
		fract->fractol = 1;
	else if (ft_strcmp(argv, "burningship") == 0)
		fract->fractol = 2;
	else if (ft_strcmp(argv, "mushroom") == 0)
		fract->fractol = 3;
	else if (ft_strcmp(argv, "gangurru") == 0)
		fract->fractol = 4;
	else if (ft_strcmp(argv, "pudding") == 0)
		fract->fractol = 5;
	else
	{
		right_fractol();
		return (0);
	}
	return (1);
}

int		two_window(char **argv, t_fr *fract)
{
	t_fr	*fract2;

	if (!(fract2 = (t_fr*)malloc(sizeof(t_fr))))
		return (0);
	if (fract_comp(argv[1], fract) == 0 || fract_comp(argv[2], fract2) == 0)
		return (0);
	set_settings(fract2);
	fract->mlx = mlx_init();
	mlx_win_init(fract, fract->mlx, "New Fractol_1");
	fract2->mlx = mlx_init();
	mlx_win_init(fract2, fract->mlx, "New Fractol 2");
	fract_init(fract);
	fract_init(fract2);
	mouse_mlx(fract);
	mouse_mlx(fract2);
	mlx_loop(fract->mlx);
	return (0);
}

int		main(int argc, char **argv)
{
	t_fr	*fract;

	if (!(fract = (t_fr *)malloc(sizeof(t_fr))))
		return (0);
	set_settings(fract);
	if (argc == 2)
	{
		if ((fract_comp(argv[1], fract)) == 0)
			return (0);
		fract->mlx = mlx_init();
		mlx_win_init(fract, fract->mlx, "Fractol");
		fract_init(fract);
		mouse_mlx(fract);
		mlx_loop(fract->mlx);
	}
	else if (argc == 3)
	{
		if (!two_window(argv, fract))
			return (0);
	}
	else
		right_fractol();
	return (0);
}
