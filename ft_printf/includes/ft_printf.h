/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:46:56 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:46:57 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include "../libft/libft.h"
# include <stdlib.h>
# include <unistd.h>
# include <stdarg.h>
# define HH 1
# define H 2
# define L 3
# define LL 5
# define ZEROS 0
# define START 1
# define END 2
# define MAX(x, y) x > y ? x : y

typedef struct			s_pr
{
	char				hash_flag;
	char				zero_flag;
	char				minus_flag;
	char				plus_flag;
	char				space_flag;
	char				is_neg;
	char				prepend;
	int					width;
	char				p_flag;
	int					p;
	char				is_nan;
	char				is_inf;
	char				mod;
	int					c;
	char				valid;
}						t_pr;

int						ft_printf(const char *format, ...);
int						call_handler(t_pr *pr, va_list ap);
int						pr_parser(const char *s, t_pr *pr, va_list ap);
int						parse_mod(const char *s, t_pr *pr);
const char				*parse_flag(const char *s, t_pr *pr);
const char				*parse_width(const char *s, t_pr *pr, va_list ap);
int						star_width(t_pr *pr, va_list ap, int star_flag);
const char				*parse_p(const char *s, t_pr *pr, va_list ap);
const char				*parse_c(const char *s, t_pr *pr);
int						type_c(t_pr *pr, va_list ap);
int						type_s(t_pr *pr, va_list ap);
int						type_p(t_pr *pr, va_list ap);
int						type_d(t_pr *pr, va_list ap);
int						handle_u(t_pr *pr, va_list ap);
int						handle_o(t_pr *pr, va_list ap);
int						handle_x(t_pr *pr, va_list ap);
int						type_f(t_pr *pr, va_list ap);
int						handle_percent(t_pr *pr);
int						handle_plain(const char *s);
long long int			signed_handle_mod(t_pr *pr, va_list ap);
unsigned long long int	unsigned_handle_mod(t_pr *pr, va_list ap);
int						write_unsigned(t_pr *pr, unsigned long long n,
										char *s, char *prepend);
long double				float_handle_mod(t_pr *pr, va_list ap);
int						count_big_f(long double f);
long double				pf_atof_helper(char *s);
char					*big_f(long double f);
char					*little_f(long double f, int p, char *big_f);
void					pad_pr_f(int len, t_pr *pr, int seg);
int						pr_len_f(char *big_s, t_pr *pr);
char					set_sign_f(long double f, t_pr *pr);
void					flag_manager_f(long double f, t_pr *pr);
void					flag_helper_f_nan(t_pr *pr);
int						write_f(t_pr *pr, char sign, char *big_s,
								char *little_s);
void					free_helper_f(t_pr *pr, char *big_s,
										char *little_s);
long double				long_pow(int pow);
int						is_even_str(char *str);
long double				ft_round(long double f, t_pr *pr);
int						is_signed_conv(int c);
void					flag_manager(t_pr *pr);
void					pad_pr(t_pr *pr, int len, int b_len, int seg);
int						pr_len(t_pr *pr, int len, char *prepend);
char					set_sign(long long int n, t_pr *pr);
void					write_padding(int len, char c);
char					*ft_dtoa_signed(long long int n);
int						places(unsigned long long n, int base);
char					*ft_dtoa_u(unsigned long long int n, int base, char c);
void					zero_helper(t_pr *pr, int *count);
int						is_nan(long double f);
int						is_inf(long double f);
int						is_not_even_str(char *str);

#endif
