/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:45:38 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:45:39 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

long double	long_pow(int pow)
{
	long double f;

	f = 1;
	while (pow > 0)
	{
		f = f * 10;
		--pow;
	}
	while (pow < 0)
	{
		f = f / 10;
		++pow;
	}
	return (f);
}

int			is_not_even_str(char *str)
{
	return ((str[ft_strlen(str) - 1] - '0') % 2);
}

long double	ft_round(long double f, t_pr *pr)
{
	long double	temp;
	char		*big_s;
	char		*little_s;

	f = f < 0 ? -f : f;
	temp = f * long_pow(pr->p);
	big_s = big_f(temp);
	little_s = little_f(temp, pr->p, big_s);
	temp -= pf_atof_helper(big_s);
	free(big_s);
	if (temp > 0.5 || (temp == 0.5 && is_not_even_str(big_s)))
		f += 0.5 * long_pow(-pr->p);
	return (f);
}

int			is_nan(long double f)
{
	return (!(f == f));
}

int			is_inf(long double f)
{
	if (f == (1.0 / 0.0) || f == -(1.0 / 0.0))
		return (1);
	else
		return (0);
}
