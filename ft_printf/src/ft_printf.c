/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:44:33 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:44:34 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int			pr_parser(const char *s, t_pr *pr, va_list ap)
{
	const char *start;

	start = s;
	s = parse_flag(s, pr);
	s = parse_width(s, pr, ap);
	s = parse_p(s, pr, ap);
	s += parse_mod(s, pr);
	s = parse_c(s, pr);
	return (s - start);
}

int			call_handler(t_pr *pr, va_list ap)
{
	static int	(*handlers[11])(t_pr *pr, va_list ap) = {
		&type_c,
		&type_s,
		&type_p,
		&type_d,
		&handle_o,
		&handle_u,
		&handle_x,
		&handle_x,
		&type_f
	};
	static int	(*handle_percent_ptr)(t_pr *pr) = &handle_percent;

	if (pr->c == 9)
		return (handle_percent_ptr(pr));
	return ((handlers[pr->c](pr, ap)));
}

int			handle_plain(const char *s)
{
	int		len;
	char	*next;

	next = ft_strchr(s, '%');
	len = next ? next - s : ft_strlen(s);
	write(1, s, len);
	return (len);
}

inline void	zero_helper(t_pr *pr, int *count)
{
	ft_bzero(pr, sizeof(*pr));
	*count = 0;
}

int			ft_printf(const char *format, ...)
{
	va_list	ap;
	t_pr	pr;
	int		count;
	int		temp;

	zero_helper(&pr, &count);
	va_start(ap, format);
	while (*format)
	{
		if (*format == '%')
		{
			format += pr_parser(format + 1, &pr, ap) + 1;
			if (pr.valid)
				count += call_handler(&pr, ap);
			ft_bzero(&pr, sizeof(pr));
		}
		else
		{
			temp = handle_plain(format);
			format += temp;
			count += temp;
		}
	}
	va_end(ap);
	return (count);
}
