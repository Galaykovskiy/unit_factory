/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_flags.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:46:00 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:46:00 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

long long int			signed_handle_mod(t_pr *pr, va_list ap)
{
	long long int n;

	if (pr->mod == LL)
		n = va_arg(ap, long long int);
	else if (pr->mod == L)
		n = va_arg(ap, long int);
	else if (pr->mod == H)
		n = (short int)va_arg(ap, int);
	else if (pr->mod == HH)
		n = (char)va_arg(ap, int);
	else
		n = (int)va_arg(ap, int);
	return (n);
}

unsigned long long int	unsigned_handle_mod(t_pr *pr, va_list ap)
{
	unsigned long long int n;

	if (pr->mod == LL)
		n = va_arg(ap, unsigned long long int);
	else if (pr->mod == L)
		n = va_arg(ap, unsigned long int);
	else if (pr->mod == H)
		n = (unsigned short int)va_arg(ap, unsigned int);
	else if (pr->mod == HH)
		n = (unsigned char)va_arg(ap, unsigned int);
	else
		n = (unsigned int)va_arg(ap, unsigned int);
	return (n);
}

char					set_sign(long long int n, t_pr *pr)
{
	char sign;

	sign = pr->space_flag ? ' ' : '+';
	if (n < 0)
	{
		sign = '-';
		pr->is_neg = 1;
	}
	return (sign);
}

void					flag_manager(t_pr *pr)
{
	if (pr->p_flag || pr->minus_flag)
		pr->zero_flag = 0;
	if (pr->plus_flag)
		pr->space_flag = 0;
}

void					pad_pr(t_pr *pr, int len, int b_len, int seg)
{
	if (!pr->zero_flag && !pr->minus_flag && pr->width >
		b_len && seg == START)
		write_padding(pr->width - b_len, ' ');
	else if (!pr->zero_flag && pr->minus_flag && pr->width >
		b_len && seg == END)
		write_padding(pr->width - b_len, ' ');
	else if (seg == ZEROS)
	{
		if (pr->p_flag)
			write_padding(pr->p > len ? pr->p - len : 0, '0');
		else if (pr->zero_flag && pr->width)
			write_padding(pr->width > b_len ? pr->width - b_len : 0, '0');
	}
}
