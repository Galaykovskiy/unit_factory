/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   types_cspd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:45:10 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:45:11 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int	type_c(t_pr *pr, va_list ap)
{
	char c;

	c = va_arg(ap, int);
	if (pr->width && !pr->minus_flag && !pr->zero_flag)
		write_padding(pr->width - 1, ' ');
	if (pr->width && !pr->minus_flag && pr->zero_flag)
		write_padding(pr->width - 1, '0');
	write(1, &c, 1);
	if (pr->width && pr->minus_flag)
		write_padding(pr->width - 1, ' ');
	return (pr->width ? pr->width : 1);
}

int	handle_percent(t_pr *pr)
{
	if (pr->width && !pr->zero_flag && !pr->minus_flag)
		write_padding(pr->width - 1, ' ');
	if (pr->width && pr->zero_flag && !pr->minus_flag)
		write_padding(pr->width - 1, '0');
	write(1, "%", 1);
	if (pr->width && pr->minus_flag)
		write_padding(pr->width - 1, ' ');
	return (pr->width ? pr->width : 1);
}

int	type_s(t_pr *pr, va_list ap)
{
	char	*s;
	int		len;

	s = va_arg(ap, char*);
	len = s ? ft_strlen(s) : 6;
	if (pr->p_flag && pr->p < len)
		len = pr->p;
	if (pr->width && !pr->minus_flag && !pr->zero_flag
		&& pr->width > len)
		write_padding(pr->width - len, ' ');
	if (pr->width && !pr->minus_flag && pr->zero_flag
		&& pr->width > len)
		write_padding(pr->width - len, '0');
	s ? write(1, s, len) : write(1, "(null)", len);
	if (pr->width && pr->minus_flag && pr->width > len)
		write_padding(pr->width - len, ' ');
	return (pr->width > len ? pr->width : len);
}

int	type_p(t_pr *pr, va_list ap)
{
	void	*p;
	char	*s;
	int		len;
	int		b_len;

	p = va_arg(ap, void*);
	s = ft_ptoa(p, 'a')	;
	pr->prepend = 1;
	len = (pr->p_flag && !pr->p && !p ? 0 : ft_strlen(s));
	b_len = pr_len(pr, len, "0x");
	pad_pr(pr, len, b_len, START);
	write(1, "0x", 2);
	pad_pr(pr, len, b_len, ZEROS);
	write(1, s, len);
	pad_pr(pr, len, b_len, END);
	free(s);
	return (MAX(pr->width, b_len));
}

int	type_d(t_pr *pr, va_list ap)
{
	long long int	n;
	char			sign;
	char			*s;
	int				len;
	int				b_len;

	n = signed_handle_mod(pr, ap);
	s = ft_dtoa_signed(n);
	flag_manager(pr);
	sign = set_sign(n, pr);
	len = pr->p_flag && !pr->p && n == 0 ? 0 : ft_strlen(s);
	b_len = pr_len(pr, len, NULL);
	pad_pr(pr, len, b_len, START);
	if (n < 0 || pr->plus_flag || pr->space_flag)
		write(1, &sign, 1);
	pad_pr(pr, len, b_len, ZEROS);
	write(1, s, len);
	pad_pr(pr, len, b_len, END);
	free(s);
	return (pr->width > b_len ? pr->width : b_len);
}
