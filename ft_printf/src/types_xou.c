/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   types_xou.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:45:20 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:45:20 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int			handle_u(t_pr *pr, va_list ap)
{
	unsigned long long int	n;
	char					*s;

	n = unsigned_handle_mod(pr, ap);
	s = ft_dtoa_u(n, 10, 0);
	return (write_unsigned(pr, n, s, NULL));
}

int			handle_o(t_pr *pr, va_list ap)
{
	unsigned long long int	n;
	char					*s;

	n = unsigned_handle_mod(pr, ap);
	s = ft_dtoa_u(n, 8, 0);
	if (pr->hash_flag)
	{
		pr->p_flag = 1;
		if (!n)
			pr->p = MAX(pr->p, 1);
		else
			pr->p = MAX(pr->p, (int)ft_strlen(s) + 1);
	}
	return (write_unsigned(pr, n, s, NULL));
}

int			handle_x(t_pr *pr, va_list ap)
{
	unsigned long long int	n;
	char					*s;
	char					*prepend;
	char					hex_char;

	n = unsigned_handle_mod(pr, ap);
	hex_char = pr->c == 6 ? 'a' : 'A';
	s = ft_dtoa_u(n, 16, hex_char);
	if (pr->hash_flag && n)
	{
		prepend = pr->c == 6 ? "0x" : "0X";
		pr->prepend = 1;
	}
	return (write_unsigned(pr, n, s, (pr->prepend ? prepend : NULL)));
}

inline int	is_signed_conv(int c)
{
	if (c == 3)
		return (1);
	else
		return (0);
}

int			pr_len(t_pr *pr, int len, char *prepend)
{
	int b_len;

	b_len = (pr->p_flag && pr->p > len ? pr->p : len);
	b_len += (is_signed_conv(pr->c) && (pr->space_flag ||
		pr->plus_flag || pr->is_neg) ? 1 : 0);
	if (pr->prepend)
		b_len += ft_strlen(prepend);
	return (b_len);
}
