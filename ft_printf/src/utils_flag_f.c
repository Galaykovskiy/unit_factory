/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_flag_f.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:50:54 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:50:55 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	flag_helper_f_nan(t_pr *pr)
{
	pr->p = 0;
	pr->p_flag = 1;
	pr->zero_flag = 0;
	pr->hash_flag = 0;
}

void	flag_manager_f(long double f, t_pr *pr)
{
	if (is_nan(f) || is_inf(f))
	{
		flag_helper_f_nan(pr);
		if (is_nan(f))
		{
			pr->plus_flag = 0;
			pr->is_nan = 1;
		}
		else
		{
			pr->is_inf = 1;
			if (f == -(1.0 / 0))
				pr->is_neg = 1;
		}
	}
	if (pr->minus_flag)
		pr->zero_flag = 0;
	if (pr->plus_flag)
		pr->space_flag = 0;
	if (!pr->p_flag)
	{
		pr->p_flag = 1;
		pr->p = 6;
	}
}

void	free_helper_f(t_pr *pr, char *big_s, char *little_s)
{
	if (!pr->is_inf && !pr->is_nan)
	{
		free(big_s);
		free(little_s);
	}
}
