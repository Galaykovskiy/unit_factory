/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type_f.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:44:58 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:44:59 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	pad_pr_f(int len, t_pr *pr, int seg)
{
	if (!pr->zero_flag && !pr->minus_flag && pr->width >
		len && seg == START)
		write_padding(pr->width - len, ' ');
	else if (!pr->zero_flag && pr->minus_flag && pr->width >
		len && seg == END)
		write_padding(pr->width - len, ' ');
	else if (seg == ZEROS)
	{
		if (pr->zero_flag && pr->width)
			write_padding(pr->width > len ? pr->width - len : 0, '0');
	}
}

int		pr_len_f(char *big_s, t_pr *pr)
{
	int len;

	if (pr->is_inf || pr->is_nan)
		len = 3;
	else
		len = ft_strlen(big_s) + pr->p;
	if (pr->p > 0 || pr->hash_flag)
		len += 1;
	if (pr->space_flag || pr->plus_flag)
		len += 1;
	return (len);
}

char	set_sign_f(long double f, t_pr *pr)
{
	char sign;

	sign = pr->space_flag ? ' ' : '+';
	if (f < 0)
	{
		sign = '-';
		pr->is_neg = 1;
	}
	return (sign);
}

int		write_f(t_pr *pr, char sign, char *big_s, char *little_s)
{
	int len;

	len = pr_len_f(big_s, pr);
	pad_pr_f(len, pr, START);
	if (pr->space_flag || pr->plus_flag || pr->is_neg)
		write(1, &sign, 1);
	pad_pr_f(len, pr, ZEROS);
	write(1, big_s, ft_strlen(big_s));
	if (pr->p || pr->hash_flag)
		write(1, ".", 1);
	write(1, little_s, ft_strlen(little_s));
	pad_pr_f(len, pr, END);
	return (len);
}

int		type_f(t_pr *pr, va_list ap)
{
	long double	f;
	char		*little_s;
	char		*big_s;
	char		sign;
	int			len;

	f = float_handle_mod(pr, ap);
	flag_manager_f(f, pr);
	sign = set_sign_f(f, pr);
	if (!pr->is_inf && !pr->is_nan)
	{
		f = ft_round(f, pr);
		big_s = big_f(f);
		little_s = little_f(f, pr->p, big_s);
	}
	else
	{
		big_s = pr->is_inf ? "inf" : "nan";
		little_s = "";
	}
	len = write_f(pr, sign, big_s, little_s);
	free_helper_f(pr, big_s, little_s);
	return (MAX(pr->width, len));
}
