/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:44:50 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:44:52 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int			parse_mod(const char *s, t_pr *pr)
{
	char		*ptr;
	static char	key[] = "hlL";

	if (ft_strnstr(s, "hh", 2))
	{
		pr->mod = 1;
		return (2);
	}
	else if (ft_strnstr(s, "ll", 2))
	{
		pr->mod = 5;
		return (2);
	}
	else if ((ptr = ft_strchr(key, *s)) && *s)
	{
		pr->mod = ptr - key + 2;
		return (1);
	}
	return (0);
}

const char	*parse_flag(const char *s, t_pr *pr)
{
	while (ft_strchr("#0-+ ", *s) && *s)
	{
		if (*s == '#')
			pr->hash_flag = 1;
		else if (*s == '0')
			pr->zero_flag = 1;
		else if (*s == '-')
			pr->minus_flag = 1;
		else if (*s == '+')
			pr->plus_flag = 1;
		else if (*s == ' ')
			pr->space_flag = 1;
		++s;
	}
	return (s);
}

const char	*parse_width(const char *s, t_pr *pr, va_list ap)
{
	int star_flag;

	star_flag = 0;
	if (*s == '*')
	{
		star_flag = star_width(pr, ap, star_flag);
		++s;
	}
	while (*s >= '0' && *s <= '9')
	{
		if (!(pr->width) || star_flag)
			pr->width = ft_atoi(s);
		++s;
	}
	if (*s == '*')
	{
		star_flag = star_width(pr, ap, star_flag);
		++s;
	}
	return (s);
}

const char	*parse_p(const char *s, t_pr *pr, va_list ap)
{
	int tmp;

	if (*s == '.')
	{
		pr->p_flag = 1;
		++s;
	}
	while (*s >= '0' && *s <= '9' && pr->p_flag)
	{
		if (!(pr->p))
			pr->p = ft_atoi(s);
		++s;
	}
	if (*s == '*' && pr->p_flag)
	{
		if (!pr->p)
			pr->p = va_arg(ap, int);
		else
			tmp = va_arg(ap, int);
		if (pr->p < 0)
			pr->p_flag = 0;
		++s;
	}
	return (s);
}

const char	*parse_c(const char *s, t_pr *pr)
{
	static char key[] = "cspdouxXf%";

	if (*s && (ft_strchr(key, *s) || *s == 'i'))
	{
		pr->c = ft_strchr(key, *s) - key;
		if (*s == 'i')
			pr->c = 3;
		pr->valid = 1;
		++s;
	}
	return (s);
}
