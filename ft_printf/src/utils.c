/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:45:28 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:45:29 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int		places(unsigned long long n, int base)
{
	int count;

	count = 0;
	if (n == 0)
		return (1);
	while (n != 0)
	{
		++count;
		n /= base;
	}
	return (count);
}

char	*ft_dtoa_signed(long long int n)
{
	char				*s;
	int					i;

	i = places(n < 0 ? -n : n, 10);
	if (!(s = (char*)ft_memalloc(i + 1)))
		return (NULL);
	if (n == 0)
		s[0] = '0';
	while (n)
	{
		s[--i] = ((n > 0) ? (n % 10) : -(n % 10)) + '0';
		n /= 10;
	}
	return (s);
}

char	*ft_dtoa_u(unsigned long long int n, int base, char c)
{
	char				*s;
	int					i;

	i = places(n, base);
	if (!(s = (char*)ft_memalloc(i + 1)))
		return (NULL);
	if (n == 0)
		s[0] = '0';
	while (n)
	{
		s[--i] = (n % base > 9) ? (n % base - 10 + c) : (n % base + '0');
		n /= base;
	}
	return (s);
}

void	write_padding(int len, char c)
{
	while (len)
	{
		write(1, &c, 1);
		--len;
	}
}

int		write_unsigned(t_pr *pr, unsigned long long n,
	char *s, char *prepend)
{
	int len;
	int b_len;

	flag_manager(pr);
	if (pr->c == 'o' && pr->hash_flag)
	{
		pr->p = (pr->p_flag && pr->p > (int)ft_strlen(s) + 1)
			? pr->p : ft_strlen(s) + 1;
		pr->p_flag = 1;
	}
	len = pr->p_flag && !pr->p && n == 0 ? 0 : ft_strlen(s);
	b_len = pr_len(pr, len, prepend);
	pad_pr(pr, len, b_len, START);
	if (pr->prepend)
		write(1, prepend, ft_strlen(prepend));
	pad_pr(pr, len, b_len, ZEROS);
	write(1, s, len);
	pad_pr(pr, len, b_len, END);
	free(s);
	return (pr->width > b_len ? pr->width : b_len);
}
