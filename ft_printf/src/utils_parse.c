/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_parse.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 16:46:20 by agalayko          #+#    #+#             */
/*   Updated: 2019/02/01 16:46:20 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int	star_width(t_pr *pr, va_list ap, int star_flag)
{
	int tmp;

	if (!pr->width || !star_flag)
	{
		pr->width = va_arg(ap, int);
		if (pr->width < 0)
		{
			pr->width = -pr->width;
			pr->minus_flag = 1;
		}
		return (1);
	}
	else
	{
		tmp = va_arg(ap, int);
		return (star_flag);
	}
}
