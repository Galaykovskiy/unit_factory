/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 20:41:35 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/25 20:41:37 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new_lst;
	t_list	*elem;

	if (!lst)
		return (NULL);
	if (!(new_lst = f(lst)))
		return (NULL);
	elem = new_lst;
	lst = lst->next;
	while (lst)
	{
		if (!(elem->next = f(lst)))
			return (NULL);
		elem = elem->next;
		lst = lst->next;
	}
	elem->next = NULL;
	return (new_lst);
}
