/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnewbychar.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 18:07:47 by agalayko          #+#    #+#             */
/*   Updated: 2018/12/13 18:07:47 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnewbychar(char *src, size_t len, size_t start, char c)
{
	char	*str;
	size_t	i;

	i = 0;
	str = (char *)malloc(sizeof(char) * (len + 1));
	while (i < len)
		str[i++] = c;
	str[i] = '\0';
	i = 0;
	while (src[i])
		str[start++] = src[i++];
	return (str);
}
