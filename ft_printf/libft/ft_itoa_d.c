/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_d.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <agalayko@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 14:06:01 by agalayko          #+#    #+#             */
/*   Updated: 2019/01/31 14:06:02 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_digitsize_d(intmax_t n)
{
	int			i;

	i = n < 0 ? 1 : 0;
	if (n == 0)
		return (1);
	while (n != 0)
	{
		n = n / 10;
		i++;
	}
	return (i);
}

char	*ft_itoa_d(intmax_t n)
{
	long long int	nb;
	int				len;
	char			*new;
	int				f;

	f = 0;
	nb = n;
	len = ft_digitsize_d(n);
	if (!(new = (char *)malloc(sizeof(char) * (len + 1))))
		return (0);
	new[len--] = '\0';
	if (nb < 0 && (new[0] = '-'))
		nb = -nb;
	if (nb < -9223372036854775807 && (f = 1))
		nb = 223372036854775808;
	while (nb > 9)
	{
		new[len--] = (nb % 10 + 48);
		nb = nb / 10;
	}
	new[len] = (nb + 48);
	f ? new[--len] = 57 : 0;
	return (new);
}
