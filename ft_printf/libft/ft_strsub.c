/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:35:47 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:35:50 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(const char *s, unsigned int start, size_t len)
{
	char *d;

	if (!s)
		return (NULL);
	if (!(d = (char*)malloc(len + 1)))
		return (NULL);
	ft_strncpy(d, s + start, len);
	d[len] = '\0';
	return (d);
}
