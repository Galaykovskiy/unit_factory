/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:33:07 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:33:10 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*str;
	int		i;

	i = 0;
	str = NULL;
	if (s && f)
	{
		if (!(str = (char*)malloc(ft_strlen(s) + 1)))
			return (NULL);
		while (*s)
			str[i++] = f(*s++);
		str[i] = '\0';
	}
	return (str);
}
