/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 20:41:35 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/25 20:41:37 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*temp;

	if (!alst || !*alst)
		return ;
	temp = *alst;
	while (temp)
	{
		del(temp->content, temp->content_size);
		*alst = temp->next;
		free(temp);
		temp = *alst;
	}
}
