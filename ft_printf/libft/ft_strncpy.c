/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agalayko <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 17:33:56 by agalayko          #+#    #+#             */
/*   Updated: 2018/03/21 17:33:59 by agalayko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	int			i;
	int			count;
	const char	*s;

	s = src;
	count = 1;
	while (*s++)
		count++;
	i = 0;
	while (len)
	{
		if (i < count)
			dst[i] = src[i];
		else
			dst[i] = '\0';
		++i;
		--len;
	}
	return (dst);
}
